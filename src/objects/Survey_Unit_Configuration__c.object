<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Huron - join table to link Unit Config to Survey</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Available_Current_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled Current Number of Units Available</inlineHelpText>
        <label># Available - Current CC</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Available_New_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled New Number of Units Available</inlineHelpText>
        <label># Available - New CC</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Average_Rent_Current_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled Current Average Rent Amount</inlineHelpText>
        <label>Average Rent - Current CC</label>
        <precision>8</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Average_Rent_New_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled New Average Rent Amount</inlineHelpText>
        <label>Average Rent - New CC</label>
        <precision>8</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>High_Rent_Current_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled Current High Rent Amount</inlineHelpText>
        <label>High Rent - Current CC</label>
        <precision>8</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>High_Rent_New_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled New High Rent Amount</inlineHelpText>
        <label>High Rent - New CC</label>
        <precision>8</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Low_Rent_Current_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled Current Low Rent Amount</inlineHelpText>
        <label>Low Rent - Current CC</label>
        <precision>8</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Low_Rent_New_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled New Low Rent Amount</inlineHelpText>
        <label>Low Rent - New CC</label>
        <precision>8</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Survey__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Survey</label>
        <referenceTo>Property_Survey__c</referenceTo>
        <relationshipName>Survey_Floor_Plans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Unit_Type__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Unit Type</label>
        <referenceTo>Unit_Configuration__c</referenceTo>
        <relationshipName>Survey_Floor_Plans</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Units_Current_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled Current Number of Units</inlineHelpText>
        <label># Units - Current CC</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Units_New_CC__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Climate Controlled New Number of Units</inlineHelpText>
        <label># Units - New CC</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Survey Unit Configuration</label>
    <nameField>
        <displayFormat>UC-{0000}</displayFormat>
        <label>Unit Config Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Survey Unit Configurations</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
