/*
	@Name: Shift_Opportunity_Trigger
	@Description: Trigger for the Opportunity object
	@Dependancies: 
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/

trigger Shift_Opportunity_Trigger on Opportunity (after insert) {
	
	if (trigger.isInsert) {
		if (trigger.isAfter) { 
			Shift_OppTeamMember_Handler.createTeamMembers(trigger.new);  
		}	 
	} 	

}