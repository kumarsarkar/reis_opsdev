trigger SurveyCareType on Survey_Care_Types__c (After Insert) {
    System.Debug('<<<<TriggerFired>>>>');
     if (trigger.isInsert){
        if(trigger.isAfter){
           SurveyCareTypeTriggerHandlerClass.HandleAfterInsert(Trigger.NewMap);
        }
    }
    
}