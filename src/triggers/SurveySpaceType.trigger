trigger SurveySpaceType on Survey_Space_Type__c (After Insert) {
    System.Debug('<<<<TriggerFired>>>>');
     if (trigger.isInsert){
        if(trigger.isAfter){
           SurveySpaceTypeTriggerHandlerClass.HandleAfterInsert(Trigger.NewMap);
        }
    }
    
}