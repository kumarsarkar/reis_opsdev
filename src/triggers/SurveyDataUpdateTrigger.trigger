trigger SurveyDataUpdateTrigger on Property_Survey__c (After update,Before Update, After Insert) {
    System.Debug('<<<<TriggerFired>>>>');
    if(trigger.isUpdate){
        if(trigger.isAfter){
            Property_Survey_TriggerHandler.HandleAfterUpdate(Trigger.NewMap, Trigger.OldMap);    
        }else if(trigger.isBefore){
            Property_Survey_TriggerHandler.HandleBeforeUpdate(Trigger.NewMap, Trigger.OldMap);        
        }      
        
    }
    
    
}