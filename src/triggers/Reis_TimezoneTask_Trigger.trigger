/*
    @Name: Reis_TimezoneTask_Trigger
    @Description: Trigger for the Task object
    @Dependancies: 
    @Version: 1.0.0 
    
    ===VERSION HISTORY === 
    | Version Number | Author      | Description
    | 1.0.0          | Barney Holmes |  Initial
*/

trigger Reis_TimezoneTask_Trigger on Task (before insert, before update, after insert) {
    if (trigger.isUpdate || trigger.isInsert) {
        if (trigger.isBefore) { 
            reis_SetTimeZoneActivity_Handler.processTasks(trigger.new);  
        } 
        if (trigger.isAfter && trigger.isInsert) {
            System.Debug('<<<<TriggerFired>>>>');
            TaskTriggerHandlerClass.handleAfterInsert(Trigger.NewMap);
        } 
    } 
}