<!--
    @Name: Shift_NEFEmail_Component
    @Description: Component to generate the NEF Email
    @Dependancies: 
    @Version: 1.0.0 
    
    === VERSION HISTORY === 
    | Version Number | Author      | Description
    | 1.0.0          | Ryan Morden |  Initial 
-->  

<apex:component controller="Shift_NEFEmailTemplate_Ext" access="global">

    
    <apex:attribute name="OpportunityId" type="String" description="the object Id" assignTo="{!theOppId}" access="global"/>

    <apex:outputText rendered="{!ActiveOpportunity.Amount==0.00}" style="color:red">
       <font color="red"> This Opportunity has a <b>$0.00</b> Amount </font>
    </apex:outputText> 
    
    <html style="color: #444; font-family: Verdana, Geneva, sans-serif; font-size: 9px;">
        <body style="color: #444; font-family: Verdana, Geneva, sans-serif; font-size: 9px;">
            <style type="text/css">
                a:hover { color: #2a6496 !important; text-decoration: underline !important; }
            </style>
            
            <table style="width: 100%;">
                <tbody>
                   <tr>
                       <td>
                           <ul style="margin: 0; padding: 0;">
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;font-size: 12px;">
                                    <b>Client Name:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.AccountId}" />   
                                </li>                           
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0; font-size: 10px">
                                    <b>Opportunity:</b> &nbsp;
                                    <apex:outputlink value="{!LEFT($Api.Partner_Server_URL_140,FIND('.com',$Api.Partner_Server_URL_140)+4)+ActiveOpportunity.Id}">{!ActiveOpportunity.Name}</apex:outputlink>   
                                </li>
   
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Accounting ID:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.Reis_Accounting_ID__c}" />   
                                </li>
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Account Status:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.Account_Type__c}" />   
                                </li>
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Saleslogix ID:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.SLXID__c}" />   
                                </li>                                
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Salesforce Id:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.Account_ID__c}" />   
                                </li>  
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Website Id:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.Reis_Website_ID__c}" />   
                                </li>                                                                     
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Account Rep:</b> &nbsp;<apex:outputField value="{!ActiveOpportunity.Owner.Name}" />   
                                </li>       
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Contract Number:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Contract_Number__c}" />   
                                </li>  
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Contract Start Date:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Contract_Start_Date__c}" />   
                                </li> 
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Contract End Date:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Contract_End_Date__c}" />   
                                </li> 
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Close Date:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.CloseDate}" />   
                                </li>  
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Client Billing Address:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.BillingStreet}" /><apex:outputText value="," rendered="{!ActiveOpportunity.Account.BillingStreet!=NULL}" />
                                     &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.BillingCity}" /><apex:outputText value="," rendered="{!ActiveOpportunity.Account.BillingCity!=NULL}" />
                                     &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.BillingState}" /><apex:outputText value="," rendered="{!ActiveOpportunity.Account.BillingState!=NULL}" />
                                     &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.BillingCountry}" /><apex:outputText value="," rendered="{!ActiveOpportunity.Account.BillingCountry!=NULL}" />
                                     &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.BillingPostalCode}" />
                                </li> 
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Industry:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Account.Industry}" />   
                                </li> 
                               
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Amount:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Amount}" />   
                                </li>
                                
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Primary Contact:</b> &nbsp;<apex:outputfield value="{!PrimaryContact.Name}" />   
                                </li>   
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Primary Contact Address:</b> 
                                     &nbsp;<apex:outputfield value="{!PrimaryContact.MailingStreet}" /><apex:outputText value="," rendered="{!PrimaryContact.MailingStreet!=NULL}" />
                                     &nbsp;<apex:outputfield value="{!PrimaryContact.MailingCity}" /><apex:outputText value="," rendered="{!PrimaryContact.MailingCity!=NULL}" />
                                     &nbsp;<apex:outputfield value="{!PrimaryContact.MailingState}" /><apex:outputText value="," rendered="{!PrimaryContact.MailingState!=NULL}" />
                                     &nbsp;<apex:outputfield value="{!PrimaryContact.MailingCountry}" /><apex:outputText value="," rendered="{!PrimaryContact.MailingCountry!=NULL}" />
                                     &nbsp;<apex:outputfield value="{!PrimaryContact.MailingPostalCode}" />
                                </li> 
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Primary Contact Phone:</b> &nbsp;<apex:outputfield value="{!PrimaryContact.Phone}" />   
                                </li> 
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Target Price:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Target_Price__c}" />   
                                </li>
                                                                                                                                                                                                                                                                                                                                                                                                                 
                           </ul>
                       </td>
                   </tr> 
                </tbody>
            </table>
            
            <table style="width: 100%; border-spacing: 0px; margin-top: 15px; border: 1px solid #ddd;border-bottom-width:0px;">
                <thead>
                                      <tr>
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Product        
                        </th>
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Revenue Type
                        </th>
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Capped Amount
                        </th>
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Metro Codes
                        </th>
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Report Codes
                        </th>
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Analytics Reports
                        </th>
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Sectors
                        </th>
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Frequency
                        </th>
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Price
                        </th>  
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Site Access Date
                        </th>   
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Site Access End
                        </th>   
                        <th style="border-bottom-color: #ccc; border-bottom-style: solid; border-bottom-width: 2px; font-size:  8.5px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; background-color: #f2f2f2; padding: 3px 4px;" align="left" bgcolor="#f2f2f2">
                            Notes
                        </th>                                                                                                                                                                                                                                                
                    </tr>
                </thead>
                <tbody>
                    <apex:repeat value="{!OpportunityLineItems}" var="oli">
                        <tr>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px;" align="left" ><apex:outputfield value="{!oli.Product_Type__c}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px;" align="left" ><apex:outputfield value="{!oli.Product_Family__c}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px;" align="left" ><apex:outputfield value="{!oli.Capped_Amount__c}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 0px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px; font-size: 8.5px;" align="left"><apex:outputfield value="{!oli.MSA__c}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 0px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px; font-size: 8.5px;" align="left"><apex:outputfield value="{!oli.Reports__c}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 0px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px;" align="left"><apex:outputfield value="{!oli.Analytic__c}" /></td>                            
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 0px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px; font-size: 8.5px;" align="left"><apex:outputfield value="{!oli.Sector__c}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px;" align="left" ><apex:outputfield value="{!oli.Frequency__c}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 1px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px;" align="left" ><apex:outputfield value="{!oli.UnitPrice}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 0px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px;" align="left"><apex:outputfield value="{!oli.ServiceDate}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 0px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px;" align="left"><apex:outputfield value="{!oli.Site_End_Date__c}" /></td>
                            <td style="border-bottom-color: #ddd; border-bottom-style: solid; border-bottom-width: 1px; text-align: left; border-right-width: 0px; border-right-color: #ddd; border-right-style: solid; padding: 3px 4px; font-size: 8.5px;" align="left"><apex:outputfield value="{!oli.Notes__c}" /></td>                            
                        </tr>
                    </apex:repeat>                    
                </tbody>
            </table>
            
            <br/>
            
            <table style="width: 100%;">
                <tbody>
                   <tr>
                       <td>
                           <ul style="margin: 0; padding: 0;">
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Opportunity Comment:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Sales_Comments__c}" />   
                                </li>
                                <br/><br/>                                 
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Does the client know our property level criteria? (40+ units, etc.):</b> &nbsp;
                                    <apex:outputText value="Yes" rendered="{!ActiveOpportunity.Client_Knows_Prop_Level_Criteria__c}" /> 
                                    <apex:outputText value="No" rendered="{!ActiveOpportunity.Client_Knows_Prop_Level_Criteria__c==false}" />   
                                </li>
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Reports most critical in client’s decision to subscript?</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Most_Critical_Reports__c}" />   
                                </li>   
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Which reports should not be shown during training?:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Reports_Not_Shown__c}" />   
                                </li>   
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Does the client know the limitations of SPV (Industrial, Gas Stations, etc.)?:</b> &nbsp;
                                    <apex:outputText value="Yes" rendered="{!ActiveOpportunity.Client_KNows_SPV_Limitation__c}" /> 
                                    <apex:outputText value="No" rendered="{!ActiveOpportunity.Client_KNows_SPV_Limitation__c==false}" />                                         
                                </li>       
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Competitor beaten?</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Competition__c}" />   
                                </li>  
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Who was beat?</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Competitors_Beaten__c}" />   
                                </li> 
                                <li style="width: 100%; display: inline-block; margin: 0; padding: 1px 0;">
                                    <b>Client Uses:</b> &nbsp;<apex:outputfield value="{!ActiveOpportunity.Client_Uses__c}" />   
                                </li>                                                                                                                                                                                                                                                                                                                                                                                       
                           </ul>
                       </td>
                   </tr> 
                </tbody>
            </table>            
            
        </body>
    </html>



</apex:component>