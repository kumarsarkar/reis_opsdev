/*
	@Name: Shift_OppTeamMember_Handler
	@Description: On creation of an opp, if the lead generator field is populated
					Validate that the userId is from an active user and create a Opp team member of role "Lead Generator"
	@Dependancies: 
	@Version: 1.0.0 
	
	===VERSION HISTORY === 
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/


public class Shift_OppTeamMember_Handler {
	
	public static final string MEMBER_ROLE = 'Lead Generator';
		
	public static void createTeamMembers(List<Opportunity> insertedOpportunities) {
		
		map <Id, Opportunity> oppMap = new map <Id, Opportunity>();
		set <String> userIdSet = new set <String>();
		set <String> activeUserIdSet = new set <String>();
		List<OpportunityTeamMember> lstOppTeams = new List<OpportunityTeamMember>();
		
		 
		//Loop through inserted to opps to find ones that have the lead generator field populated
		for (Opportunity opp :insertedOpportunities) {
			if (opp.Lead_GeneratorId__c != NULL) {
				oppMap.put(opp.Id, opp);
				userIdSet.add(opp.Lead_GeneratorId__c);
			}
		}
		
		//Loop through users to validate that the users are active
		for (User u :[SELECT Id, isActive FROM User Where Id IN :userIdSet]) {
			if (u.isActive) {
				activeUserIdSet.add(u.Id);
			}
		}
		
		//Loop through opps with lead generators, and if the user is active
		//Create them as an opp team member
		for (Id oppId :oppMap.KeySet()) {
			if (activeUserIdSet.contains(oppMap.get(oppId).Lead_GeneratorId__c)) {
				OpportunityTeamMember otm = new OpportunityTeamMember(
					TeamMemberRole = MEMBER_ROLE,
					OpportunityId = oppId,
					UserId = oppMap.get(oppId).Lead_GeneratorId__c 
				);
				lstOppTeams.add(otm);
			}
		}
		
		//Insert the Opp team members
		try {
			insert lstOppTeams;
		} catch (dmlException e) {
			system.debug('Error: ' + e);
		}

	}
}