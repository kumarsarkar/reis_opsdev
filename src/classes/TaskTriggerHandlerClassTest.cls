@isTest
public class TaskTriggerHandlerClassTest {
	static TestMethod void taskTriggerTest(){
         Queue_Configuration__c qconfig = new Queue_Configuration__c(Name = 'Apartment Queue Configuration', Quarter_Market_Coverage__c = 40, Quarter_SubMarket_Coverage__c = 25, Month_1_Market_Coverage__c = 10,Month_2_Market_Coverage__c = 20,
                            Month_1_of_Surveys_per_Submarket__c = 1,Sector__c = 'Apt',  Month_2_of_Surveys_per_Submarket__c= 1, Month_1_Submarket_Coverage__c = 1, Month_2_Submarket_Coverage__c = 16, Aged_Property_Frequency__c = 50, Aged_Timeframe__c = 24, Survey_Records_With_Data__c = 200 );
        insert qconfig;
        Survey_Sector__c surSector = new Survey_Sector__c(Name = 'Apt', Description__c = 'Apartments');
        insert surSector;
        Queue__c que = new Queue__c(Call_Status__c = 'Proposed Type Change', Survey_Sector__c = surSector.Id, Queue_Configuration__c = qconfig.Id,Time_Zone__c= '1 - EST', Priority__c = '4-Low',Status__c = 'Pending Creation',Property_Type__c = 'Market Rentals', Contact_Id__c = '68B28C38-4337-4C9C-8148-958AB3G120Z1', Batch_Id__c = '707Q000000ztpT3AIA'  );
        insert que;
        Property_Survey__c psurvey = new Property_Survey__c(RecordTypeId = Schema.SObjectType.Property_Survey__c.getRecordTypeInfosByName().get('Apartments').getRecordTypeId(), Queue_Record__c = que.Id, Survey_Sector__c = surSector.Id, Survey_Data_Needed__c = True, Call_Status__c = 'Survey Completed');
        insert psurvey;
        Company__c comp = new Company__c(Salesforce_Created__c = true, Oid__c = '7653452765', Name = 'Test', Main_Phone__c = '654243', Company_Website__c = 'www.reis.com', Do_Not_Call__c = true);
        insert comp;
        Reis_Contact__c rcon = new Reis_Contact__c(REIS_Company__c = comp.Id, Salesforce_Created__c=true, Do_Not_Call__c = true, First_Name__c = 'Test', Last_Name__c = 'Reis');
        insert rcon;
        Survey_Contact__c sc = new Survey_Contact__c(Property_Survey__c = psurvey.Id, Most_Recent_Contact__c = true, Contact__c = rcon.Id, Contact_Oid__c = '65412674');
        insert sc;
        Task tsk = new Task();
        tsk.WhatId = sc.Id;
        tsk.CallType = 'Outbound';
        tsk.Status = 'Completed';
        tsk.ActivityDate = Date.Today();
        tsk.CallDurationInSeconds = 28;
        tsk.CreatedLocationVerified__c = false;
        tsk.Task_Type__c = 'Voicemail';
        tsk.Description = 'Test';
        tsk.Priority = 'Normal';
        tsk.IsRecurrence = False;
        tsk.IsReminderSet = False;
        tsk.key_activity__c = False;
        tsk.Meaningful_Activity__c = False;
        tsk.Task_Subject__c = 'Phone Call';
        tsk.Subject = 'Call';
        tsk.Task_Subject__c = 'Phone Call';
        tsk.Task_Type__c = 'Attempted';
        tsk.Subtype__c = 'Task';
        tsk.Type = 'Call'; 
        insert tsk;
    }
}