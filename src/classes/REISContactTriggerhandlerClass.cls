public class REISContactTriggerhandlerClass {
    
    public static void HandleBeforeInsert(List<REIS_Contact__c> NewContacts){
        concatenateFirstNameLastName(NewContacts);
    }
    public static void HandleBeforeUpdate(List<REIS_Contact__c> NewContacts, Map<Id,REIS_Contact__c> OldContacts){
        concatenateFirstNameLastNameBeforeUpdate(NewContacts, OldContacts);
    }
    
    public static void HandleAfterInsert(Map<Id,REIS_Contact__c> NewMap){
        
        populateUniqueString(NewMap);
    }
    
    
    //populate 36characters long string
    public static void populateUniqueString(Map<Id,REIS_Contact__c> NewMap){
        List<REIS_Contact__c> ContactList=new List<REIS_Contact__c>();
        for(String ContactId: NewMap.keyset()){
            if(NewMap.get(ContactId).Id !=null && (NewMap.get(ContactId).Oid__c== '' || NewMap.get(ContactId).Oid__c== null)){
                Blob b = Crypto.GenerateAESKey(128);
                String h = EncodingUtil.ConvertTohex(b);
                String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
                system.debug(guid);
                REIS_Contact__c ContactRec =new REIS_Contact__c(Id=NewMap.get(ContactId).Id ,Oid__c=guid.touppercase(),Salesforce_Created__c=true);
                ContactList.add(ContactRec);
            }
            
        }
        update ContactList;
    }
    //Concatenate FirstName and LastName
    public static void concatenateFirstNameLastName(List<REIS_Contact__c> NewContacts){
        for(REIS_Contact__c reisContact : NewContacts){
            //reisContact.Name = reisContact.First_Name__c==null?'':reisContact.First_Name__c;
             String FirstName=reisContact.First_Name__c==null?'':reisContact.First_Name__c;
              String LastName=reisContact.Last_Name__c==null?'':reisContact.Last_Name__c;
            if(FirstName=='' && LastName==''){
                reisContact.Name='No First Name or Last Name';
            }else{
                reisContact.Name=FirstName+' '+LastName;   
            } 
             
            
            /*if(reisContact.Last_Name__c!=null && reisContact.Last_Name__c!=''){
                reisContact.Name = reisContact.First_Name__c + ' ' + reisContact.Last_Name__c;
            }*/
        }
    }
    //Concatenate FirstName and LastName Update Case
    public static void concatenateFirstNameLastNameBeforeUpdate(List<REIS_Contact__c> NewContacts, Map<Id,REIS_Contact__c> OldContactsMap){
        for(REIS_Contact__c reisContact : NewContacts){
         //   if(reisContact.First_Name__c  != OldContactsMap.get(reisContact.Id).First_Name__c || reisContact.Last_Name__c  != OldContactsMap.get(reisContact.Id).Last_Name__c){
           //     reisContact.Name = reisContact.First_Name__c + ' ' + reisContact.Last_Name__c;
           // }
           // Anantha 18/08/2017
              String FirstName=reisContact.First_Name__c==null?'':reisContact.First_Name__c;
              String LastName=reisContact.Last_Name__c==null?'':reisContact.Last_Name__c;
              reisContact.Name=FirstName+' '+LastName;
            if(FirstName=='' && LastName==''){
                reisContact.Name='No First Name or Last Name';
            }else{
                reisContact.Name=FirstName+' '+LastName;   
            } 
        }
    }
}