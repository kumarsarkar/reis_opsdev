global class REISBatchSchedulerClass implements Schedulable {
    //Command to run : REISBatchSchedulerClass.schedule('0 0 21 * * ?',15,'6-18',60,'6,20',60,'0-23');
    global String ProcessName;
    global REISBatchSchedulerClass(String ProcessName){
        this.ProcessName=ProcessName;
    }
    
    global void execute(SchedulableContext ctx) {
    	system.debug('ProcessName'+ProcessName+' Id=>'+ctx.getTriggerId());
        if(ProcessName=='BuildQueue'){
           BuildQueueScheduleClass.BuildQueue();
        }else if(ProcessName=='QueueAssignment'){
            QueueAssignment_Batch QueueAssignmentBatch=new QueueAssignment_Batch();
            Database.executeBatch(QueueAssignmentBatch,Integer.valueOf(BatchSize_Setting__c.getValues('QueueAssignmentBatch').Size__c));
        }else if(ProcessName=='PurgePropertySurvey'){
            DeletePropertySurveyBatchClass PurgePropSurveyBatch=new DeletePropertySurveyBatchClass();
            Database.executeBatch(PurgePropSurveyBatch,Integer.valueOf(BatchSize_Setting__c.getValues('DeletePropertyPurgeBatch').Size__c));
        }else if(ProcessName=='InboundJSONProcessor'){
            InboundJSONProcessingBatch AptBat=new InboundJSONProcessingBatch('Apt');
			Database.executeBatch(AptBat,Integer.valueOf(BatchSize_Setting__c.getValues('InboundJSONProcessor-Apt').Size__c));
            InboundJSONProcessingBatch AffBat=new InboundJSONProcessingBatch('Aff');
			Database.executeBatch(AffBat,Integer.valueOf(BatchSize_Setting__c.getValues('InboundJSONProcessor-Aff').Size__c));
            InboundJSONProcessingBatch OffBat=new InboundJSONProcessingBatch('Off');
			Database.executeBatch(OffBat,Integer.valueOf(BatchSize_Setting__c.getValues('InboundJSONProcessor-Off').Size__c));
            InboundJSONProcessingBatch IndBat=new InboundJSONProcessingBatch('Ind');
			Database.executeBatch(IndBat,Integer.valueOf(BatchSize_Setting__c.getValues('InboundJSONProcessor-Ind').Size__c));
            InboundJSONProcessingBatch RetBat=new InboundJSONProcessingBatch('Ret');
			Database.executeBatch(RetBat,Integer.valueOf(BatchSize_Setting__c.getValues('InboundJSONProcessor-Ret').Size__c));
            InboundJSONProcessingBatch StoBat=new InboundJSONProcessingBatch('Sto');
			Database.executeBatch(StoBat,Integer.valueOf(BatchSize_Setting__c.getValues('InboundJSONProcessor-Sto').Size__c));
            InboundJSONProcessingBatch SthBat=new InboundJSONProcessingBatch('Sth');
			Database.executeBatch(SthBat,Integer.valueOf(BatchSize_Setting__c.getValues('InboundJSONProcessor-Sth').Size__c));
            InboundJSONProcessingBatch SnrBat=new InboundJSONProcessingBatch('Snr');
			Database.executeBatch(SnrBat,Integer.valueOf(BatchSize_Setting__c.getValues('InboundJSONProcessor-Snr').Size__c));
        }else if(ProcessName=='InboundJSONProcessorSmallBatch'){
            InboundJSONProcessingBatch AptBat=new InboundJSONProcessingBatch('Apt');
			Database.executeBatch(AptBat,1);
            InboundJSONProcessingBatch AffBat=new InboundJSONProcessingBatch('Aff');
			Database.executeBatch(AffBat,1);
            InboundJSONProcessingBatch OffBat=new InboundJSONProcessingBatch('Off');
			Database.executeBatch(OffBat,1);
            InboundJSONProcessingBatch IndBat=new InboundJSONProcessingBatch('Ind');
			Database.executeBatch(IndBat,1);
            InboundJSONProcessingBatch RetBat=new InboundJSONProcessingBatch('Ret');
			Database.executeBatch(RetBat,1);
            InboundJSONProcessingBatch StoBat=new InboundJSONProcessingBatch('Sto');
			Database.executeBatch(StoBat,1);
            InboundJSONProcessingBatch SthBat=new InboundJSONProcessingBatch('Sth');
			Database.executeBatch(SthBat,1);
            InboundJSONProcessingBatch SnrBat=new InboundJSONProcessingBatch('Snr');
			Database.executeBatch(SnrBat,1);
        }
    }
    
 //   global static void schedule(String BuildQueueSchedule,String AssignmentSchedule,String AssignmentSchedule1,String AssignmentSchedule2,String AssignmentSchedule3,String PurgePropSurveySchedule){
    global static void schedule(String BuildQueueSchedule,Integer AssignmentMinutesSchedule,String AssignmentHoursSchedule,Integer PurgePropSurveyMinuteSchedule,String PurgePropSurveyHourSchedule,Integer InboundJSONProcessorMinuteSchedule,String InboundJSONProcessorHourSchedule/*,Integer DeleteQueueMinuteSchedule,String DeleteQueueHourSchedule*/){   
        REISBatchSchedulerClass BuildQueueBatch=new REISBatchSchedulerClass('BuildQueue');
        system.schedule('BuildQueueBatch',BuildQueueSchedule , BuildQueueBatch);//'0 0 21 * * ?'
        AssignementBatchSchedule(AssignmentMinutesSchedule,AssignmentHoursSchedule);
        PurgePropertyBatchSchedule(PurgePropSurveyMinuteSchedule,PurgePropSurveyHourSchedule);
        InboundJSONProcessorBatchSchedule(InboundJSONProcessorMinuteSchedule,InboundJSONProcessorHourSchedule);
      //  DeleteQueueBatchSchedule(DeleteQueueMinuteSchedule,DeleteQueueHourSchedule);
    }
    
    global static void AssignementBatchSchedule(Integer AssignmentMinutesSchedule,String AssignmentHoursSchedule){
        for(Integer i=0;i<=59;i=i+AssignmentMinutesSchedule){
            system.debug('i=>'+i);
            REISBatchSchedulerClass QueueAssignmentBatch=new REISBatchSchedulerClass('QueueAssignment');
        	system.schedule('QueueAssignment'+i+'-Min','0 '+i+' '+AssignmentHoursSchedule+' * * ?' , QueueAssignmentBatch);//'0 0 0-23 * * ?'
        }
    }
    global static void PurgePropertyBatchSchedule(Integer PurgePropSurveyMinuteSchedule,String PurgePropSurveyHourSchedule){
        for(Integer i=0;i<=59;i=i+PurgePropSurveyMinuteSchedule){
            system.debug('i=>'+i);
            REISBatchSchedulerClass PurgePropertySurveyBatch=new REISBatchSchedulerClass('PurgePropertySurvey');
       		system.schedule('PurgePropertySurvey'+i+'-Min','0 '+i+' '+PurgePropSurveyHourSchedule+' * * ?' , PurgePropertySurveyBatch);//'0 0 0-23 * * ?'
        }
    }
    
    global static void InboundJSONProcessorBatchSchedule(Integer InboundJSONProcessorMinuteSchedule,String InboundJSONProcessorHourSchedule){
        for(Integer i=0;i<=59;i=i+InboundJSONProcessorMinuteSchedule){
            system.debug('i=>'+i+'InboundJSONProcessorMinuteSchedule=>'+InboundJSONProcessorMinuteSchedule);
            REISBatchSchedulerClass InboundJSONProcessorBatch=new REISBatchSchedulerClass('InboundJSONProcessor');
       		system.schedule('InboundJSONProcessorBatch'+i+'-Min','0 '+i+' '+InboundJSONProcessorHourSchedule+' * * ?' , InboundJSONProcessorBatch);//'0 0 0-23 * * ?'
          //  system.debug('i=>'+i+'InboundJSONProcessorMinuteSchedule=>'+InboundJSONProcessorMinuteSchedule);
        }
    }
    
    global static void DeleteQueueBatchSchedule(Integer DeleteQueueMinuteSchedule,String DeleteQueueHourSchedule){
        for(Integer i=0;i<=59;i=i+DeleteQueueMinuteSchedule){
            system.debug('i=>'+i);
            REISBatchSchedulerClass DeleteQueueBatch=new REISBatchSchedulerClass('DeleteQueue');
       		system.schedule('DeleteQueueBatch'+i+'-Min','0 '+i+' '+DeleteQueueHourSchedule+' * * ?' , DeleteQueueBatch);//'0 0 0-23 * * ?'
        }
    }
}