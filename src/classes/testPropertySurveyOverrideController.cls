@isTest
private class testPropertySurveyOverrideController {

	@testSetup static void dataSetup(){
		Property_Survey__c newSurvey = new Property_Survey__c();
		Reis_Contact__c newCon = new Reis_Contact__c(First_Name__c = 'test', Last_Name__c = 'tester');
		Survey_Contact__c newSCon = new Survey_Contact__c();
		Reis_Space_Type__c newSpaceType = new Reis_Space_Type__c(name = 'test room Bedroom', Sector__c = 'Apt');
		Survey_Space_Type__c newST = new Survey_Space_Type__c();
		Amenity__c newAmen = new Amenity__c(Name = 'test a', Foundation_Id__c = 'tttest', Has_Quantity__c = FALSE);

		newSpaceType.Code__c = '000';

		insert newAmen;
		insert newSurvey;
		insert newCon;
		insert newSpaceType;
		newSCon.Contact__c = newCon.id;
		newSCon.Property_Survey__c = newSurvey.id;
		insert newSCon;
		newST.Apt_Space_Type__c = newSpaceType.id;
		newST.Reis_Space_Type__c = newSpaceType.id;
		newST.Property_Survey__c = newSurvey.id;
		insert newST;
		Survey_Amenities__c  sa = new Survey_Amenities__c (Amenity__c = newAmen.id, Property_Survey__c = newSurvey.id);
		insert sa;
	}

	@isTest static void testgetFieldSets() {
		Property_Survey__c ps = [select id, RecordTypeId from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.getFloorpanHeaderFields();
		testController.getOfficeFloorpanHeaderFields();
		testController.getSeniorSurveypanHeaderFields();
		testController.getFloorPlanFields();
		testController.getReisContactFields();
		testController.getSeniorSurveyHousingFields();
		testController.getFloorPlanMap();
		testController.getSeniorSurveyMap();
		testController.getAffordableRentnHeaderFields();
		testController.getAffordableVacancyHeaderFields();
		testController.getAffordableUnitSizeHeaderFields();
		testController.getAffordableFloorPlanMap();
		testController.getAffordableUnitHeaderFields();
		testController.getFloorPlanRentFields();
		testController.getFloorPlanVacancyFields();
		testController.getFloorPlanUnitFields();
		testController.getFloorPlanUnitsSizeFields();
	}

	@isTest static void testControllerLoadingApartments() {
		Property_Survey__c ps = [select id, RecordTypeId from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
	}
	@isTest static void testControllerLoadingOffice() {
		Property_Survey__c ps = [select id, RecordTypeId from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Office' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
	}
	@isTest static void testControllerLoadingAffordable() {
		Property_Survey__c ps = [select id, RecordTypeId from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Affordable' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
	}
	@isTest static void testControllerLoadingIndustrial() {
		Property_Survey__c ps = [select id, RecordTypeId from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Industrial' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
	}
	@isTest static void testControllerLoadingSrHousing() {
		Property_Survey__c ps = [select id, RecordTypeId from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Senior Housing' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
	}

	@isTest static void testSaveNoChangeApartments() {
		Property_Survey__c ps = [select id, RecordTypeId from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.saveRecord();
	}

	@isTest static void testSaveSimpleChangeApartments() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Address__c = 'testing change';
		testController.floorPlans[0].sst.Average_Rent_New__c = 1;
		testController.floorPlans[0].sst.Average_Rent_New__c = 2;
		testController.tempRecord.Total_Units_New__c = 1;
		testController.tempRecord.Total_Available_New__c = 1;
		testController.saveRecord();
	}

	@isTest static void testSaveRentChangeApartments() {
		Survey_MSA__c m = new Survey_MSA__c(name = 'test');
		insert m;
		Survey_Sector__c s = new Survey_Sector__c(name = '1');
		insert s;
		Rent_Range__c rr1 = new Rent_Range__c(MSA__c = m.Id, Sector__c = s.id, Minimum_Allowable_Rent__c = 300, Maximum_Allowable_Rent__c = 900, Space_Type__c = 'test Bedroom');
		insert rr1;
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		ps.Survey_Sector__c = s.id;
		ps.Survey_MSA__c = m.id;
		ps.QA_Reason_Notes__c = null;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Address__c = 'testing change';
		testController.floorPlans[0].sst.Average_Rent_New__c = 1;
		testController.saveRecord();
		testController.floorPlans[0].sst.Average_Rent_New__c = 2;
		testController.saveRecord();
		testController.floorPlans[0].sst.Average_Rent_New__c = 500;
		testController.saveRecord();
	}

	@isTest static void testSaveNullSurveySourceApartments() {
		Property_Survey__c ps = [select id, RecordTypeId, Survey_Source__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Survey_Source__c';
		testController.saveRecord();
	}

	@isTest static void testAddNewAmenity() {
		Property_Survey__c ps = [select id, RecordTypeId, Survey_Source__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Survey_Source__c';
		Amenity__c a = [select id from Amenity__c limit 1];
		testController.newAm.Industrial_Amenities__c = a.id;
		testController.addNewAmmenity();
		testController.saveRecord();
	}

	@isTest static void testAddIndustrialAmenity() {
		Property_Survey__c ps = [select id, RecordTypeId, Survey_Source__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Industrial' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Survey_Source__c';
		Amenity__c a = [select id from Amenity__c limit 1];
		testController.newAm.Industrial_Amenities__c = a.id;
		testController.addNewAmmenityIndustry();
		testController.addNewAmmenity();
		testController.saveRecord();
	}

	@isTest static void testAddSpaceDupe() {
		Property_Survey__c ps = [select id, RecordTypeId, Survey_Source__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		Reis_Space_Type__c t = [select id from Reis_Space_Type__c limit 1];
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Survey_Source__c';
		testController.newSST();
		for(PropertySurveyOverrideController.sstWrapper s :testController.floorplans){
			s.sst.Apt_Space_Type__c = t.id;
		}
		testController.saveRecord();
	}

	@isTest static void testAddSpaceNull() {
		Property_Survey__c ps = [select id, RecordTypeId, Survey_Source__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		Reis_Space_Type__c t = [select id from Reis_Space_Type__c limit 1];
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Survey_Source__c';
		testController.newSST();
		for(PropertySurveyOverrideController.sstWrapper s :testController.floorplans){
			s.sst.Apt_Space_Type__c  = null;
		}
		testController.saveRecord();
	}

	@isTest static void testSaveNoChangeOffice() {
		Property_Survey__c ps = [select id, RecordTypeId from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Office' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.saveRecord();
	}

	@isTest static void testSaveSimpleChangeOffice() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Office' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Address__c = 'testing change';
		testController.saveRecord();
	}

	@isTest static void testSaveSimpleChangeSrHousing() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Senior Housing' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Address__c = 'testing change';
		testController.saveRecord();
	}

	@isTest static void testSaveSimpleChangeSeniorHousing() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Senior Housing' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Address__c = 'testing change';
		testController.saveRecord();
	}

	@isTest static void testAddContact() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.AddToConList();
	}

	@isTest static void testAddBlankContactSave() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.AddToConList();
		testController.saveRecord();
	}

	@isTest static void testAddExistingContactSave() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		Reis_Contact__c newC = new Reis_Contact__c(First_Name__c = 'test1', Last_Name__c = 'test2');
		insert newC;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.AddToConList();
		testController.contactList[1].con.Contact__c = newC.id;
		testController.saveRecord();
	}

	@isTest static void testAddSpaceTypeWithExisting() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.newSST();
	}

	@isTest static void testAddSpaceTypeWithOutExisting() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		list<Survey_Space_Type__c> deleteSurveyList = [select id from Survey_Space_Type__c where Property_Survey__c = :ps.id];
		delete deleteSurveyList;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.newSST();
	}

	@isTest static void testSubmitForApprovalNoSurveySource() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Address__c = 'testing change';
		testController.floorPlans[0].sst.Average_Rent_New__c = 1;
		testController.floorPlans[0].sst.Average_Rent_New__c = 2;
		testController.tempRecord.Total_Units_New__c = 1;
		testController.tempRecord.Total_Available_New__c = 1;
		testController.sumbitForApproval();
	}
	@isTest static void testSubmitForApprovalWithSurveySource() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Address__c = 'testing change';
		testController.floorPlans[0].sst.Average_Rent_New__c = 2;
		testController.tempRecord.Total_Units_New__c = 1;
		testController.tempRecord.Total_Available_New__c = 1;
		testController.tempRecord.Survey_Source__c = 'In Progress';
		testController.sumbitForApproval();
	}

	@isTest static void testRemoteLogACall() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Web';
		PropertySurveyOverrideController.logACall('test',ps.id);
	}

	@isTest static void testRemoteSave() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Web';
		PropertySurveyOverrideController.remoteSave(TRUE,ps.id);
	}

	@isTest static void testRemoteComment() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Web';
		PropertySurveyOverrideController.remoteAddComment('TEST',ps.id);
	}

	@isTest static void testRemoteQAComment() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Web';
		PropertySurveyOverrideController.remoteAddQAComment('TEST',ps.id);
	}

	@isTest static void testRemoteQCComment() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Web';
		PropertySurveyOverrideController.remoteAddQCComment('TEST',ps.id);
	}

	@isTest static void testRemoteAddCompany() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		testController.tempRecord.Survey_Source__c = 'Web';
		PropertySurveyOverrideController.remoteCompanyAdd('TEST','test','5555555555');
	}

	@isTest static void testRemoteSiteCheck() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		propertyWebsiteValidation__c a = new propertyWebsiteValidation__c(Name = 'google.com', property_Site__c = TRUE, Survey_Site__c = true);
		insert a;
		PropertySurveyOverrideController.remoteSiteCheck('google.com',true);
		PropertySurveyOverrideController.remoteSiteCheck('google.com',false);
	}

	@isTest static void testRemoteContactGetters() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		String r = [select id from Reis_Contact__c limit 1].id;
		//PropertySurveyOverrideController.remoteContactRole(r);
		PropertySurveyOverrideController.remoteContactFName(r);
		PropertySurveyOverrideController.remoteContactLName(r);
		PropertySurveyOverrideController.remoteContactEmail(r);
		PropertySurveyOverrideController.remoteContactWPhone(r);
		PropertySurveyOverrideController.remoteContactMPhone(r);
		PropertySurveyOverrideController.remoteContactCompany(r);
		PropertySurveyOverrideController.remoteContactJobRole(r);
	}

	@isTest static void testRemoteBeds() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		String r = [select id, name from Reis_Space_Type__c limit 1].name;
		PropertySurveyOverrideController.remoteBeds(r);
	}

	@isTest static void testRemoteSize() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		String r = [select id,name from Reis_Space_Type__c limit 1].name;
		PropertySurveyOverrideController.remoteSize(r);
	}

	@isTest static void testRemoteRentRange() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		Survey_MSA__c m = new Survey_MSA__c(name = 'test');
		insert m;
		Survey_Sector__c s = new Survey_Sector__c(name = '1');
		insert s;
		Rent_Range__c rr1 = new Rent_Range__c(MSA__c = m.Id, Sector__c = s.id, Minimum_Allowable_Rent__c = 300, Maximum_Allowable_Rent__c = 900, Space_Type__c = 'test Bedroom');
		insert rr1;
		PropertySurveyOverrideController.isRentInRange(s.id, m.id,'test Bedroom','500');
		try{
			PropertySurveyOverrideController.isRentInRange(s.id, m.id,'test Bedroom','1000');
			PropertySurveyOverrideController.isRentInRange(s.id, m.id,'test Bedroom','500');
			Rent_Range__c rr2 = new Rent_Range__c(MSA__c = m.id, Sector__c = s.id, Minimum_Allowable_Rent__c = 300, Maximum_Allowable_Rent__c = 900, Space_Type__c = 'test Bedroom');
			insert rr2;
			PropertySurveyOverrideController.isRentInRange(s.id, m.id,'test Bedroom','500');
		}
		catch(Exception e){

		}
	}

	@isTest static void testRemoteAmenityQuanity() {
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		String r = [select id,name from Amenity__c limit 1].id;
		PropertySurveyOverrideController.amenityHasQuantaty(r);
	}

	@isTest static void testRemotCollectBeds(){
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		String r = [select id,name from Reis_Space_Type__c limit 1].id;
		PropertySurveyOverrideController.remoteCollectBeds(New List<String>{r});
	}

	@isTest static void testRemotStorage(){
		Property_Survey__c ps = [select id, RecordTypeId, Address__c from Property_Survey__c limit 1];
		ps.recordTypeId = [select id from recordType where SObjectType = 'Property_Survey__c' AND Name = 'Apartments' limit 1].id;
		update ps;
		ApexPages.currentPage().getParameters().put('id',ps.id);
		ApexPages.StandardController testPage = new ApexPages.StandardController(ps);
		PropertySurveyOverrideController testController = new PropertySurveyOverrideController(testPage);
		String r = [select id,name from Reis_Space_Type__c limit 1].id;
		PropertySurveyOverrideController.remoteStorageFields(New List<String>{r});
	}
}