Public class PopulateSurveyDataClass1{
    //Property_Survey__c propsurvey;
    //Public List<FoundationJSONStructure> fndnJSONStructure;
    //Public PopulateSurveyDataClass1(){

    //Logic to get the JSON and desrialize and store the instance in SurveyDataStructure
    //propsurvey = [Select Id,Outgoing_Survey_Data__c, Buildings_Current__c,/*Buildings_New__c,Floors_Current__c,Floors_New__c,Month_Built_New__c,*/Address__c,Month_Built_Current__c,Incoming_Survey_Data__c from Property_Survey__c where ID= 'a30Q0000000Yfe2' LIMIT 1];
    //String incoSurveyData = propsurvey.Incoming_Survey_Data__c;
    //fndnJSONStructure=(List<FoundationJSONStructure>)JSON.deserialize(incoSurveyData, List<FoundationJSONStructure>.class);
    //system.debug('JSONStructure=>>>>>>>>>>>>>>>>>>>>>'+fndnJSONStructure);
    //}
    /*Public void updateAddress(){
    Logic to write all the data from SurveyDataStructure to the corresponding Survey; This is the Id that identifies the Survey Record to update PropertyId
        if(fndnJSONStructure.size()>0 && fndnJSONStructure !=null){
            for(FoundationJSONStructure fjsn : fndnJSONStructure){
                if(fjsn.addressField!=null && fjsn.addressField.addField!=null){
                    if(fjsn.addressField.addField.streetAddressField!=null){
                        propsurvey.Address__c =  fjsn.addressField.addField.streetAddressField;
                    }
                    if(fjsn.addressField.addField.cityField!=null){
                        propsurvey.City__c = fjsn.addressField.addField.cityField;
                    }
                    if(fjsn.addressField.addField.countyField!=null){
                        propsurvey.County__c = fjsn.addressField.addField.countyField;
                    }
                    if(fjsn.addressField.addField.mSAField!=null){
                        propsurvey.Survey_MSA__c = fjsn.addressField.addField.mSAField;
                    }
                    if(fjsn.addressField.addField.subIdField!=null){
                        propsurvey.Sub_Market_ID__c = String.ValueOf(fjsn.addressField.addField.subIdField);
                    }
                }
            }
        }
    }
    Public void updateSurveyData(){
        if(fndnJSONStructure.size()>0 && fndnJSONStructure !=null){
            for(FoundationJSONStructure fjsn : fndnJSONStructure){
                if(fjsn.buildingField != null && fjsn.buildingField.bldField != null){
                    if(fjsn.buildingField.bldField.monthBuiltField!=null){
                        propsurvey.Month_Built_Current__c = String.ValueOf(fjsn.buildingField.bldField.monthBuiltField);
                    }
                    if(fjsn.buildingField.bldField.yrBuiltField!=null){
                        propsurvey.Year_Built_Current__c = String.ValueOf(fjsn.buildingField.bldField.yrBuiltField);
                    }
                    if( fjsn.buildingField.bldField.aptDetailsField != null){
                        if(fjsn.buildingField.bldField.aptDetailsField.bldgsField!=null){
                            propsurvey.Buildings_Current__c = fjsn.buildingField.bldField.aptDetailsField.bldgsField;
                        }
                        if(fjsn.buildingField.bldField.aptDetailsField.floorsField!=null){
                            propsurvey.Floors_Current__c = fjsn.buildingField.bldField.aptDetailsField.floorsField;
                        }
                    }
                }
            }
        }
    }
    Public void updateContactData(){
        List<Survey_Contact__c> scList = new List<Survey_Contact__c>();
        if(fndnJSONStructure.size()>0 && fndnJSONStructure !=null){
            for(FoundationJSONStructure fjsn : fndnJSONStructure){
                if(fjsn.contactField.size()>0){
                    for(FoundationJSONStructure.PropertyCont propcon : fjsn.contactField){
                        Survey_Contact__c sc = new Survey_Contact__c();
                        if(propcon.firstNameField!=null && propcon.lastNameField!=null){
                            sc.Name__c = propcon.firstNameField + ' '+ propcon.lastNameField;
                            sc.Property_Survey__c = propsurvey.ID;
                            scList.add(sc);
                        }
                    }
                }
            }
        }
        if(scList.size()>0){
            INSERT scList;
        }
    }
    Public void updateSurveyFloorPlans(){
        List<Survey_Space_Type__c> sfPlanList = new List<Survey_Space_Type__c>();
        if(fndnJSONStructure.size()>0 && fndnJSONStructure !=null){
            for(FoundationJSONStructure fjsn : fndnJSONStructure){
                if(fjsn.sectorSurveySpaceDataField.aptSurveySpaceDataField.size()>0){
                    for(FoundationJSONStructure.PropertySectorSurveySpaceDataAptSurveySpaceData pssAPT : fjsn.sectorSurveySpaceDataField.aptSurveySpaceDataField){
                        Survey_Space_Type__c sfp = new Survey_Space_Type__c();
                        if(pssAPT.unitsField!=null){
                            sfp.Of_Units_Current__c = pssAPT.unitsField;
                        }
                        if(pssAPT.sizeField!=null){
                        }
                        if(pssAPT.availField!=null){
                            sfp.Unit_Available_Current__c = pssAPT.availField;
                        }
                        if(pssAPT.averageRentField!=null){
                            sfp.Average_Rent_Current__c = pssAPT.averageRentField;
                        }
                        sfp.Property_Survey__c = propsurvey.ID;
                        sfPlanList.add(sfp);
                    }
                }
            }
        }
        if(sfPlanList.size()>0){
            INSERT sfPlanList;
        }
    }
    Public void updateSurveyAmenity(){
        List<Survey_Amenities__c> samList = new List<Survey_Amenities__c>();
        if(fndnJSONStructure.size()>0 && fndnJSONStructure !=null){
            for(FoundationJSONStructure fjsn : fndnJSONStructure){
                if(fjsn.amenitiesField.size()>0){
                    for(FoundationJSONStructure.PropertyAmen propAmen : fjsn.amenitiesField){
                        Survey_Amenities__c sam = new Survey_Amenities__c();
                        if(propAmen.amenityTypeField!=null){
                            sam.Type__c = propAmen.amenityTypeField;
                            sam.Property_Survey__c = propsurvey.ID;
                            samList.add(sam);
                        }
                    }
                }
            }
        }
        if(samList.size()>0){
            INSERT samList;
        }
    }
    Public Void updateSurvey(){
        PopulateSurveyDataClass1 psd = new PopulateSurveyDataClass1();
        updateAddress();
        updateSurveyData();
        updateContactData();
        updateSurveyAmenity();
        String jsonstring = Json.serialize(fndnJSONStructure);
        propsurvey.Outgoing_Survey_Data__c = jsonstring;
        Update propsurvey;
        System.Debug('<<<<<<>>>>>>>>'+propsurvey.Outgoing_Survey_Data__c);
    }*/
}