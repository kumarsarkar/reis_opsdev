Global class QueueAssignment_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.STATEFUL{
String SurveyorUserId;

global Database.QueryLocator start(Database.BatchableContext BC)
    {
    
    String query = 'Select Id from Surveyor__c';
    return Database.getQueryLocator(query);

}

global void execute(Database.BatchableContext BC, List<Surveyor__c> scope){
system.debug('**scope' + scope);
    for (Surveyor__c surveyor : scope){
    system.debug('**surveyor ' + surveyor );
        List<Queue__c> QueueList=new List<Queue__c>();
        List<Queue_configuration__c> QueueconfigList=new List<Queue_configuration__c>();
        List<Property_Survey__c> PropSurveyList=new List<Property_Survey__c>();
        QueueAssignmentUtils.AssignQueue(surveyor.id,QueueList,QueueconfigList,PropSurveyList);
        try{
            update QueueList;
            update PropSurveyList;
            update QueueconfigList;
        }catch(exception e){
            throw(e);
        }
    }
}

  global void finish(Database.BatchableContext BC){
  }


}