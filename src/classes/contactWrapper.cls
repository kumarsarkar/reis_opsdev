public class contactWrapper{
    public boolean newcon {get;set;}
    public Survey_Contact__c con {get;set;}
    public Reis_Contact__c rCon {get;set;}

    public contactWrapper(){
        this.newCon = false;
        this.con = new Survey_Contact__c();
        this.rCon = new Reis_Contact__c();
    }

    public contactWrapper(Survey_Contact__c c){
        this.newCon = false;
        this.con = c;
        this.rCon = new Reis_Contact__c();
    }

    public contactWrapper(Survey_Contact__c c, Reis_Contact__c r){
        this.newCon = false;
        this.con = c;
        this.rCon = r;
    }

    public contactWrapper(Survey_Contact__c c, Reis_Contact__c r, Boolean b){
        this.newCon = b;
        this.con = c;
        this.rCon = r;
    }

    public static List<contactWrapper> addNewCon(List<contactWrapper> scList){
        scList.add(new contactWrapper(new Survey_Contact__c(), new Reis_Contact__c(), TRUE));
        return scList;
    }
}