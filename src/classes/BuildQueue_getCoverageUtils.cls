public class BuildQueue_getCoverageUtils {
public static void getSectorCoverage(Integer Month,String PoolRecSector,Map<String,Boolean> CoverageResult,Map<String,Integer> PropertiesCountMap,Map<String,Integer> MetricCountMap,Map<String,Integer> PropertiesMarketCountMap,Map<String,Integer> MetricsMarketCountMap,Map<String,Survey_MSA__c> MSAMap,Map<String,Survey_Submarket__c> SubMarketMap,Queue_Configuration__c QueueConfig,Map<String,Queue_Exception__c> QueueExceptionMap/*,Map<String,Integer> PropertiesSubMarketRentCounterMap,Map<String,Integer> PropertiesSubMarketVacancyCounterMap*/){
        //prepare the MSA Quarter Coverage Map
        Decimal TargetMarketPercent,TargetSubMarketPercent;
        
        for(String Key:PropertiesMarketCountMap.keySet()){
            if(MetricsMarketCountMap.containsKey(key)){
                String[] KeyContents=key.split(';');
                system.debug('Key=>'+Key);
                system.debug('KeyContents=>'+KeyContents);
                String Sector=KeyContents[0];
                //    system.debug('KeyContents=>'+KeyContents+' - key=>'+Key+'>> PoolRecSector'+PoolRecSector+'>>Sector'+Sector);
                if(PoolRecSector!=Sector){
                    continue;
                }
                String PropertyType=KeyContents[1];
                String MSAName;
                if(MSAMap.containsKey(KeyContents[2])){
                	MSAName=MSAMap.get(KeyContents[2]).Name;    
                }else{
                    continue;
                }
                
                //String SubMarket=SubMarketMap.get(KeyContents[2]).Name;
                
                system.debug('MSAName=>'+MSAName);
                
                if(Month==1 || Month==4 || Month==7 || Month==10){
                    
                    if(QueueExceptionMap.containsKey(PropertyType+'MSA'+MSAName+'Month 1 Market Coverage %')){
                        TargetMarketPercent=QueueExceptionMap.get(PropertyType+'MSA'+MSAName+'Month 1 Market Coverage %').Rule_Value__c;
                    }else{
                        if(QueueExceptionMap.containsKey(null+'MSA'+MSAName+'Month 1 Market Coverage %')){
                            TargetMarketPercent=QueueExceptionMap.get(null+'MSA'+MSAName+'Month 1 Market Coverage %').Rule_Value__c;
                        }else{
                            if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 1 Market Coverage %')){
                                TargetMarketPercent=QueueExceptionMap.get(PropertyType+'All'+null+'Month 1 Market Coverage %').Rule_Value__c;
                            }else{
                                TargetMarketPercent=QueueConfig.Month_1_Market_Coverage__c;            
                            }
                            
                        }
                        
                    }
                }
                
                if(Month==2 || Month==5 || Month==8 || Month==11){
                    Decimal TempCount;
                    if(QueueExceptionMap.containsKey(PropertyType+'MSA'+MSAName+'Month 2 Market Coverage %')){
                        
                        if(QueueExceptionMap.containsKey(PropertyType+'MSA'+MSAName+'Month 1 Market Coverage %')){
                            TempCount=QueueExceptionMap.get(PropertyType+'MSA'+MSAName+'Month 1 Market Coverage %').Rule_Value__c;
                        }else{
                            if(QueueExceptionMap.containsKey(null+'MSA'+MSAName+'Month 1 Market Coverage %')){
                                TempCount=QueueExceptionMap.get(null+'MSA'+MSAName+'Month 1 Market Coverage %').Rule_Value__c;
                            }else{
                                if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 1 Market Coverage %')){
                                    TempCount=QueueExceptionMap.get(PropertyType+'All'+null+'Month 1 Market Coverage %').Rule_Value__c;
                                }else{
                                    TempCount=QueueConfig.Month_1_Market_Coverage__c;        
                                }
                                
                            }
                        }
                        TargetMarketPercent=QueueExceptionMap.get(PropertyType+'MSA'+MSAName+'Month 2 Market Coverage %').Rule_Value__c+TempCount;
                    }else{
                        if(QueueExceptionMap.containsKey(null+'MSA'+MSAName+'Month 2 Market Coverage %')){
                            if(QueueExceptionMap.containsKey(PropertyType+'MSA'+MSAName+'Month 1 Market Coverage %')){
                                TempCount=QueueExceptionMap.get(PropertyType+'MSA'+MSAName+'Month 1 Market Coverage %').Rule_Value__c;
                            }else{
                                if(QueueExceptionMap.containsKey(null+'MSA'+MSAName+'Month 1 Market Coverage %')){
                                    TempCount=QueueExceptionMap.get(null+'MSA'+MSAName+'Month 1 Market Coverage %').Rule_Value__c;
                                }else{
                                    if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 1 Market Coverage %')){
                                        TempCount=QueueExceptionMap.get(PropertyType+'All'+null+'Month 1 Market Coverage %').Rule_Value__c;
                                    }else{
                                        TempCount=QueueConfig.Month_1_Market_Coverage__c;        
                                    }
                                }
                            }
                            TargetMarketPercent=QueueExceptionMap.get(null+'MSA'+MSAName+'Month 2 Market Coverage %').Rule_Value__c+TempCount;
                        }else{
                            if(QueueExceptionMap.containsKey(PropertyType+'MSA'+MSAName+'Month 1 Market Coverage %')){
                                TempCount=QueueExceptionMap.get(PropertyType+'MSA'+MSAName+'Month 1 Market Coverage %').Rule_Value__c;
                            }else{
                                if(QueueExceptionMap.containsKey(null+'MSA'+MSAName+'Month 1 Market Coverage %')){
                                    TempCount=QueueExceptionMap.get(null+'MSA'+MSAName+'Month 1 Market Coverage %').Rule_Value__c;
                                }else{
                                    if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 1 Market Coverage %')){
                                        TempCount=QueueExceptionMap.get(PropertyType+'All'+null+'Month 1 Market Coverage %').Rule_Value__c;
                                    }else{
                                        TempCount=QueueConfig.Month_1_Market_Coverage__c;        
                                    }   
                                } 
                            }
                            if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 2 Market Coverage %')){
                                TargetMarketPercent=QueueExceptionMap.get(PropertyType+'All'+null+'Month 2 Market Coverage %').rule_value__c+TempCount;
                            }else{
                                TargetMarketPercent=QueueConfig.Month_2_Market_Coverage__c+TempCount;     
                            }
                            
                        }
                        
                    }
                    
                }
                
                if(Month==3 || Month==6 || Month==9 || Month==12){
                    if(QueueExceptionMap.containsKey(PropertyType+'MSA'+MSAName+'Quarter Market Coverage %')){
                        TargetMarketPercent=QueueExceptionMap.get(PropertyType+'MSA'+MSAName+'Quarter Market Coverage %').Rule_Value__c;
                    }else{
                        if(QueueExceptionMap.containsKey(null+'MSA'+MSAName+'Quarter Market Coverage %')){
                            TargetMarketPercent=QueueExceptionMap.get(null+'MSA'+MSAName+'Quarter Market Coverage %').Rule_Value__c;
                        }else{
                            if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Quarter Market Coverage %')){
                                TargetMarketPercent=QueueExceptionMap.get(PropertyType+'All'+null+'Quarter Market Coverage %').Rule_Value__c;    
                            }else{
                                TargetMarketPercent=QueueConfig.Quarter_Market_Coverage__c;    
                            }
                            
                        }
                    }
                }
                
                Integer TotalCount=PropertiesMarketCountMap.get(key);
                Integer MetricCount=MetricsMarketCountMap.get(key);
                TotalCount=TotalCount==null?0:TotalCount;
                MetricCount=MetricCount==null?0:MetricCount;
                Decimal CompletePercent=(Decimal.valueof(MetricCount+1)/TotalCount*100);
                
                system.debug('Sector__c=>'+PoolRecSector+'CompletePercent=>'+CompletePercent+'>> TargetMarketPercent=>'+TargetMarketPercent);
                system.debug('TargetMarketPercent<=CompletePercent=>'+(TargetMarketPercent<=CompletePercent));
                
                if(TargetMarketPercent<=CompletePercent){
                    system.debug('CoverageResult.containsKey(\'Market\')'+(CoverageResult.containsKey('Market')));
                    if(!CoverageResult.containsKey('Market')){
                        CoverageResult.put('Market', true);
                        //        CoverageResult.put('SubMarket', true);
                        system.debug('Inside True ; CoverageResult=>'+CoverageResult);
                    }
                }else{
                    CoverageResult.put('Market', false);
                    //      CoverageResult.put('SubMarket', false);
                    system.debug(' Inside Else ; CoverageResult=>'+CoverageResult);
                }
            }else{
                CoverageResult.put('Market', false);
            }
        }
        
        if(!CoverageResult.containsKey('Market')){
            CoverageResult.put('Market', false);
        }
        // Check the SubMarket Logic.
        for(String Key:PropertiesCountMap.keySet()){
            Decimal TargetSubMarketMonthPercent,SubMarketMinCount;
            if(MetricCountMap.containsKey(key)){
                String[] KeyContents=key.split(';');
                String Sector=KeyContents[0];
                String PropertyType=KeyContents[1];
                //  system.debug('KeyContents=>'+KeyContents+' - key=>'+Key+'>> PoolRecSector'+PoolRecSector+'>>Sector'+Sector);
                if(PoolRecSector!=Sector){
                    continue;
                }
                String MSAName;
                if(MSAMap.containskey(KeyContents[2])){
                	MSAName=MSAMap.get(KeyContents[2]).Name;    
                }else{
                    continue;
                }
                
                System.debug('KeyContents=>'+KeyContents);
                String SubMarket;
                if(SubMarketMap.containskey(KeyContents[3])){
                	SubMarket=SubMarketMap.get(KeyContents[3]).Name;    
                }else{
                    continue;
                }
                
                 //Anantha 21/07/2017
     // 		    Integer SubMarketRentCount=PropertiesSubMarketRentCounterMap.get(key);
     //  		    Integer SubMarketVacancyCount=PropertiesSubMarketVacancyCounterMap.get(key);
                
                
                system.debug('MSAName=>'+MSAName);
                
                if(Month==1 || Month==4 || Month==7 || Month==10){
                    if(QueueExceptionMap.containsKey(PropertyType+'Sub Market'+SubMarket+'Month 1 # Surveys per Submarket')){
                        SubMarketMinCount= QueueExceptionMap.get(PropertyType+'Sub Market'+SubMarket+'Month 1 # Surveys per Submarket').Rule_Value__c;
                    }else{
                        if(QueueExceptionMap.containsKey(null+'Sub Market'+SubMarket+'Month 1 # Surveys per Submarket')){
                            SubMarketMinCount= QueueExceptionMap.get(null+'Sub Market'+SubMarket+'Month 1 # Surveys per Submarket').Rule_Value__c;                        
                        }else{
                            if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 1 # Surveys per Submarket')){
                                SubMarketMinCount= QueueExceptionMap.get(PropertyType+'All'+null+'Month 1 # Surveys per Submarket').Rule_Value__c;
                            }else{
                                SubMarketMinCount=QueueConfig.Month_1_of_Surveys_per_Submarket__c;         
                            }
                        }
                    }
                    
                    if(QueueExceptionMap.containsKey(PropertyType+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %')){
                        TargetSubMarketMonthPercent=QueueExceptionMap.get(PropertyType+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %').Rule_Value__c;
                    }else{
                        if(QueueExceptionMap.containsKey(null+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %')){
                            TargetSubMarketMonthPercent=QueueExceptionMap.get(PropertyType+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %').Rule_Value__c;
                        }else{
                            if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 1 Submarket Coverage %')){
                                TargetSubMarketMonthPercent=QueueExceptionMap.get(PropertyType+'All'+null+'Month 1 Submarket Coverage %').Rule_Value__c;
                            }else{
                                TargetSubMarketMonthPercent=QueueConfig.Month_1_Submarket_Coverage__c;    
                            }
                        }
                        
                    }         
                }
                
                if(Month==2 || Month==5 || Month==8 || Month==11){
                    Decimal TempCount;
                    if(QueueExceptionMap.containsKey(PropertyType+'Sub Market'+SubMarket+'Month 2 # Surveys per Submarket')){
                        SubMarketMinCount=QueueExceptionMap.get(PropertyType+'Sub Market'+SubMarket+'Month 2 # Surveys per Submarket').Rule_Value__c;
                    }else{
                        if(QueueExceptionMap.containsKey(null+'Sub Market'+SubMarket+'Month 2 # Surveys per Submarket')){
                            SubMarketMinCount=QueueExceptionMap.get(null+'Sub Market'+SubMarket+'Month 2 # Surveys per Submarket').Rule_Value__c;
                        }else{
                            if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 2 # Surveys per Submarket')){
                                SubMarketMinCount=QueueExceptionMap.get(PropertyType+'All'+null+'Month 2 # Surveys per Submarket').Rule_Value__c;
                            }else{
                                SubMarketMinCount=QueueConfig.Month_2_of_Surveys_per_Submarket__c;        
                            }
                        }
                    }
                    
                    tempCount=0;            
                    if(QueueExceptionMap.containsKey(PropertyType+'Sub Market'+SubMarket+'Month 2 Submarket Coverage %')){
                        
                        if(QueueExceptionMap.containsKey(PropertyType+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %')){
                            TempCount=QueueExceptionMap.get(PropertyType+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %').Rule_Value__c;
                        }else{
                            if(QueueExceptionMap.containsKey(null+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %')){
                                TempCount=QueueExceptionMap.get(null+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %').Rule_Value__c;
                            }else{
                                if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 1 Submarket Coverage %')){
                                    TempCount=QueueExceptionMap.get(PropertyType+'All'+null+'Month 1 Submarket Coverage %').Rule_Value__c;
                                }else{
                                    TempCount=QueueConfig.Month_1_Submarket_Coverage__c;    
                                }
                            }
                        }
                        TargetSubMarketMonthPercent=QueueExceptionMap.get(PropertyType+'Sub Market'+SubMarket+'Month 2 Submarket Coverage %').Rule_Value__c+TempCount;
                    }else{
                        if(QueueExceptionMap.containsKey(null+'Sub Market'+SubMarket+'Month 2 Submarket Coverage %')){
                            if(QueueExceptionMap.containsKey(PropertyType+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %')){
                                TempCount=QueueExceptionMap.get(PropertyType+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %').Rule_Value__c;
                            }else{
                                if(QueueExceptionMap.containsKey(null+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %')){
                                    TempCount=QueueExceptionMap.get(null+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %').Rule_Value__c;
                                }else{
                                    if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 1 Submarket Coverage %')){
                                        TempCount=QueueExceptionMap.get(PropertyType+'All'+null+'Month 1 Submarket Coverage %').Rule_Value__c;
                                    }else{
                                        TempCount=QueueConfig.Month_1_Submarket_Coverage__c;    
                                    }
                                }
                            }
                            TargetSubMarketMonthPercent=QueueExceptionMap.get(null+'Sub Market'+SubMarket+'Month 2 Submarket Coverage %').Rule_Value__c+TempCount;   
                        }else{
                            if(QueueExceptionMap.containsKey(PropertyType+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %')){
                                TempCount=QueueExceptionMap.get(PropertyType+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %').Rule_Value__c;
                            }else{
                                if(QueueExceptionMap.containsKey(null+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %')){
                                    TempCount=QueueExceptionMap.get(null+'Sub Market'+SubMarket+'Month 1 Submarket Coverage %').Rule_Value__c;
                                }else{
                                    if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 1 Submarket Coverage %')){
                                        TempCount=QueueExceptionMap.get(PropertyType+'All'+null+'Month 1 Submarket Coverage %').Rule_Value__c;
                                    }else{
                                        TempCount=QueueConfig.Month_1_Submarket_Coverage__c;    
                                    }
                                } 
                            }
                            if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Month 2 Submarket Coverage %')){
                                TargetSubMarketMonthPercent=QueueExceptionMap.get(PropertyType+'All'+null+'Month 2 Submarket Coverage %').rule_value__c+TempCount;
                            }else{
                                TargetSubMarketMonthPercent=QueueConfig.Month_2_Submarket_Coverage__c+TempCount;        
                            }
                            
                        }
                        
                    }
                    
                }
                
                if(Month==3 || Month==6 || Month==9 || Month==12){
                    if(QueueExceptionMap.containsKey(PropertyType+'Sub Market'+SubMarket+'Quarter SubMarket Coverage %')){
                        TargetSubMarketMonthPercent=QueueExceptionMap.get(PropertyType+'Sub Market'+SubMarket+'Quarter SubMarket Coverage %').Rule_Value__c;
                    }else{
                        if(QueueExceptionMap.containsKey(null+'Sub Market'+SubMarket+'Quarter SubMarket Coverage %')){
                            TargetSubMarketMonthPercent=QueueExceptionMap.get(null+'Sub Market'+SubMarket+'Quarter SubMarket Coverage %').Rule_Value__c;
                        }else{
                            if(QueueExceptionMap.containsKey(PropertyType+'All'+null+'Quarter SubMarket Coverage %')){
                                TargetSubMarketMonthPercent=QueueExceptionMap.get(PropertyType+'All'+null+'Quarter SubMarket Coverage %').Rule_Value__c;
                            }else{
                                TargetSubMarketMonthPercent=QueueConfig.Quarter_SubMarket_Coverage__c;    
                            }
                        }
                        
                    }
                    
                }
                
                Integer TotalCount=PropertiesCountMap.get(key);
                Integer MetricCount=MetricCountMap.get(key);
                TotalCount=TotalCount==null?0:TotalCount;
                MetricCount=MetricCount==null?0:MetricCount;
                
                //Anantha 24/07
 //               SubMarketRentCount=SubMarketRentCount==null?0:SubMarketRentCount;
 //               SubMarketVacancyCount=SubMarketVacancyCount==null?0:SubMarketVacancyCount;
 //               Decimal SubMarketRentCoverage=Decimal.valueOf(SubMarketRentCount/TotalCount*100);
 //       		Decimal SubMarketVacancyCoverage=Decimal.valueOf(SubMarketVacancyCount/TotalCount*100);
                
                Decimal CompletePercent=(Decimal.valueof(MetricCount+1)/TotalCount*100);
                
                system.debug('Sector__c=>'+PoolRecSector+'CompletePercent=>'+CompletePercent+'>> TargetSubMarketMonthPercent=>'+TargetSubMarketMonthPercent);
                system.debug('TargetSubMarketMonthPercent<=CompletePercent=>'+(TargetMarketPercent<=CompletePercent));
                
                if((TargetSubMarketMonthPercent<=CompletePercent) && (SubMarketMinCount<=MetricCount)){
                    system.debug('CoverageResult.containsKey(\'SubMarket\')'+(CoverageResult.containsKey('SubMarket')));
                    //Anantha 24/07/2017 added this check
             //       if(TargetSubMarketMonthPercent<=SubMarketRentCoverage && TargetSubMarketMonthPercent<=SubMarketVacancyCoverage){
                    	if(!CoverageResult.containsKey('SubMarket')){
                        	CoverageResult.put('SubMarket', true);
                        	system.debug('Inside True ; CoverageResult=>'+CoverageResult);
                    	}    
               //     }else{
               //         CoverageResult.put('SubMarket', false);
               //     }
                    
                }else{
                    CoverageResult.put('SubMarket', false);
	                system.debug(' Inside Else ; CoverageResult=>'+CoverageResult);
                }
                
            }else{
                CoverageResult.put('SubMarket', false);
            }
        }
        
        if(!CoverageResult.containsKey('SubMarket')){
            CoverageResult.put('SubMarket', false);
        }
        // system.debug('CoverageResult=>'+CoverageResult);
        
    }
}