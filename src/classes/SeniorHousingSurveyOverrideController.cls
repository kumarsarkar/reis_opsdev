public class SeniorHousingSurveyOverrideController {
  public id recordId;
  public Property_Survey__c tempRecord {get;set;}
  public string saveResults {get;set;}
  public List<contactWrapper> contactList {get;set;}
  public static id staticRecordId{get;set;}
  public Map<String, List<amenitiesWrapper>> amenitiesList {get;set;}
  public List<Schema.FieldSetMember> secondFloorPlan {get;set;}
  public Survey_Amenities__c newAm {get;set;}
  public Property_Survey__c unChangedRecord {get;set;}


  //custom setter and getter to ensure when we save all the records are properly updated
  public List<sstWrapper> floorplans {
    get{
      return floorplans;
    }
    set{
      floorplans = value;
    }}

  //Methods to get the fields from the fieldsets
  public List<Schema.FieldSetMember> getSurveyContactFields(){
    return SObjectType.Survey_Contact__c.FieldSets.SurveyContactFields.getFields();
  }
  public List<Schema.FieldSetMember> getReisContactFields(){
    return sObjectType.Reis_Contact__c.FieldSets.property_survey_field_set.getFields();
  }

  public List<Schema.FieldSetMember> getLocationFields(){
    return SObjectType.Property_Survey__c.FieldSets.PropertyLocation.getFields();
  }

  public List<Schema.FieldSetMember> getInformationFields(){
    return SObjectType.Property_Survey__c.FieldSets.PropertyInformation.getFields();
  }

  public List<Schema.FieldSetMember> getFloorpanHeaderFields(){
    Return SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurveyHeader.getFields();
  }

  //added by sumedha for senior housing
  public List<Schema.FieldSetMember> getSeniorSurveypanHeaderFields(){
    Return SObjectType.Survey_Space_Type__c.FieldSets.SeniorSurveyHousingHeader.getFields();
  }

  public List<fieldWrapper> getFloorPlanFields(){
    List<fieldWrapper> returnList = new List<fieldWrapper>();
    List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
    tempList = SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurvey.getFields();
    Double counter = 0;
    Double total = tempList.size();
    for(Schema.FieldSetMember fsm :tempList){
      fieldWrapper fw = new fieldWrapper();
      if(counter/total < .5){
        fw.afield = fsm;
        fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
      }
      else{
        break;
      }
      counter++;
      returnList.add(fw);
    }
    return returnList;
  }

  //added by sumedha for senior housing
  public List<fieldWrapper> getSeniorSurveyHousingFields(){
    List<fieldWrapper> returnList = new List<fieldWrapper>();
    List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
    tempList = SObjectType.Survey_Space_Type__c.FieldSets.SeniorSurveyHousingHeaderFields.getFields();
    Double counter = 0;
    Double total = tempList.size();
    for(Schema.FieldSetMember fsm :tempList){
      fieldWrapper fw = new fieldWrapper();
      if(counter/total < .5){
        fw.afield = fsm;
        fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
      }
      else{
        break;
      }
      counter++;
      returnList.add(fw);
    }

    system.debug('returnList*' + returnList);
    return returnList;
  }

  //fieldWrapper is used with field sets for the survey space type records related list, allows us to set editable and readonly fields seperatly
  public class fieldWrapper{
    public Schema.FieldSetMember afield {get;set;}
    public Schema.FieldSetMember bfield {get;set;}

    public fieldWrapper(){
      //this.afield = new Schema.FieldSetMember();
      //this.bfield = new Schema.FieldSetMember();
    }
  }
  //getFields method only returns to us the fields of the property survey record
  public List<Schema.FieldSetMember> getFields(){
    List<Schema.FieldSetMember> returnList = new List<Schema.FieldSetMember>();
    returnList.addAll(getLocationFields());
    returnList.addAll(getInformationFields());
    return returnList;
  }
  //gets us the survey space type records we need to use in the related list, also uses field sets to get data
  //we can expand this to get different fieldsets based on record type
  public List<sstWrapper> getFloorPlanMap(){
    List<sstWrapper> returnMap = new List<sstWrapper>();
    String queryString = 'Select ';
    for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurvey.getFields()){
      QueryString += f.getFieldPath()+', ';
    }

    QueryString += 'Id,Name,Delete_By_Foundation__c,Apt_Space_Type__c,Reis_Space_Type__c,Senior_Housing_Space_Type__r.Name,Senior_Housing_Space_Type__c From Survey_Space_Type__c  Where Property_Survey__c = \''+recordId+'\' ORDER BY Reis_Space_Type__r.Sort_Order__c ASC';
    for(Survey_Space_Type__c sst :Database.query(QueryString)){
        returnMap.add(new sstWrapper(sst));
    }
    return returnMap;
  }
 //added by sumedha for senior housing
  public List<sstWrapper> getSeniorSurveyMap(){
    List<sstWrapper> returnMap = new List<sstWrapper>();
    String queryString = 'Select ';

    for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.SeniorSurveyHousingHeaderFields.getFields()){
      QueryString += f.getFieldPath()+', ';
    }

    QueryString += 'Id,Name,Delete_By_Foundation__c,SR_Housing_Care_Inc_in_Rent__c,Apt_Space_Type__c,Reis_Space_Type__c,Senior_Housing_Space_Type__r.Name,Senior_Housing_Space_Type__c From Survey_Space_Type__c  Where Property_Survey__c = \''+recordId+'\' ORDER BY Reis_Space_Type__r.Sort_Order__c ASC';
    for(Survey_Space_Type__c sst :Database.query(QueryString)){
        returnMap.add(new sstWrapper(sst));
    }

    system.debug('prop survey**' + QueryString);
    system.debug('returnMap sst wrapper**' + returnMap);
    return returnMap;
  }

  //uses custom contact wrapper, we get all survey contacts and reis contacts and put them into the wrapper
  // inoder for us to end the information we need to update the reis contact directly, there are also fields on the survey contact that need to be updated at times as well
  // this helps with that
  public list<contactWrapper> oldCons{get;set;}
  public List<contactWrapper> getContacts(){
    list<contactWrapper> returnList = new list<contactWrapper>();
    oldCons = new list<contactWrapper>();
    map<String,ContactWrapper> emailMap = new map<String,ContactWrapper>();
    List<id> idList = new List<id>();
    String queryString = 'Select ';
    For(Schema.FieldSetMember f: This.getSurveyContactFields()){
      QueryString += f.getFieldPath()+', ';
    }
    QueryString += 'Id, Contact__c,Contact__r.oid__c From Survey_Contact__c Where Property_Survey__c = :recordId ORDER BY Most_Recent_Contact__c DESC';
    for(Survey_Contact__c s :Database.query(QueryString)){
      if(emailMap.get(s.Contact__r.oid__c) == null){
        emailMap.put(s.Contact__r.oid__c, new contactWrapper(s));
        idList.add(s.contact__c);
      }
    }
    String newQueryString = 'Select ';
    for(Schema.FieldSetMember f :sObjectType.Reis_Contact__c.FieldSets.property_survey_field_set.getFields()){
      newQueryString += f.getFieldPath()+', ';
    }
    newQueryString += 'Id, Name, first_name__c,oid__c, last_name__c from Reis_Contact__c where id in :idList';
    for(Reis_Contact__c r:Database.query(newQueryString)){
      if(emailMap.get(r.oid__c) != null){
        emailMap.get(r.oid__c).rCon = r;
      }
    }
    oldCons = emailMap.values();
    Return emailMap.values();
    //Return Database.query(QueryString);
  }

  public Property_Survey__c getTheRecord(){
    //gets us the fields we need to use for the viewing survey contact record, we use a field set, and some fields we think will always be on
    String QueryString = 'Select Id, ';
    For(Schema.FieldSetMember f: This.getFields()){
      QueryString += f.getFieldPath()+', ';
    }
    QueryString += 'Name,Unit_Mix_Estimated__c,Leasing_Incentives__c, Rent_ref__c,Rest_neg__c,Licensed_Unit_Total_Current__c,SR_Housing_Insurance_All_New__c,SR_Housing_Insurance_Medicaid_New__c,Survey_Sector__c ,SR_Housing_Insurance_Medicare_New__c,SR_Housing_Insurance_Private_New__c,SR_Housing_Insurance_Private_Pay_New__c,Vac_Ref__c,Ref_Rest__c,Expected_Increase_Type__c ,DK_rest__c,Entry_Fee_Low_New__c,Entry_Fee_High_New__c,Entry_Fee_Average_New__c,Expected_Increase__c,Expected_Month__c,SR_Housing_MR_Units_New__c, Current_Available__c,Available_2_4_weeks__c,Total_Available_Units__c,Total_NC_Units__c,Facility_Name__c,Select_Code__c , RecordType.Name, Total_Units__c,Insurance_New__c,Comp_Totals_Avail_Hold__c,Comp_Totals_Avail__c,Non_Comp_Totals_by_Unit__c,Comp_Totals__c,Sold__c,Sold_Date__c,Sold_Amount_New__c,Licensed_Unit_Total_New__c From Property_Survey__c Where ID = \''+recordId+'\' LIMIT 1';
    Return Database.query(QueryString);
  }

  public boolean goodToClose {get;set;}
  public void saveRecord(){
    Boolean errorFound = false;
    system.debug(recordCompare(unChangedRecord,tempRecord));
    if(recordCompare(unChangedRecord,tempRecord) && recordCompare(floorplansOrigin,floorplans) && recordCompare(oldCons, contactList)){
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'No Data Has Changed, The Record Has Not Been Updated');
      ApexPages.addMessage(myMsg);
      return;
    }
    try{
      if((tempRecord.Survey_Source__c != null && tempRecord.Survey_Source__c != '') && tempRecord.Survey_Source__c.contains('Web') && (tempRecord.Survey_Website_New__c == null || tempRecord.Survey_Website_New__c == '')){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You need a web address when the Survey Source is related to Web');
        ApexPages.addMessage(myMsg);
        return;
      }
            //errorFound = true;

        errorFound = insertContacts();

      if(errorFound && (tempRecord.recordtype.name == 'Senior Housing' || tempRecord.recordtype.name == 'Affordable' || tempRecord.recordtype.name == 'Apartments')){
        errorFound = apartmentSave();
      }
      if(errorFound){
        update tempRecord;
        loadInformation();
      }
      goodToClose = errorFound;
    }
    catch(Exception e){
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+' at line: '+e.getLineNumber());
      ApexPages.addMessage(myMsg);
    }
    //Boolean errorFound = false;
    for(ApexPages.Message m : apexPAges.getMessages()){
      if(m.getSeverity() == ApexPages.Severity.ERROR){
        errorFound = True;
      }
    }
    if(goodToClose){
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Saved Successfully');
      ApexPages.addMessage(myMsg);
    }
  }

  public void newSST(){
    //adds a new survey space type to our wrapper/related list
    floorplans.add(new sstWrapper( new Survey_Space_Type__c(Property_Survey__c = recordid)));
    system.debug(floorplans.size());
  }

public Boolean apartmentSave(){
  Boolean isError = false;
  //spesific save logic for the apartments record type
  List<Survey_Space_Type__c> insertList = new List<Survey_Space_Type__c>();
  Map<id, Survey_Space_Type__c> dupeCheck = new Map<id, Survey_Space_Type__c>();
  if(floorplans == null){
    return FALSE;
      //Need to distinguish between different kinds of space types - this only works for affordable and apartment
  }
      if(tempRecord.recordtype.name == 'Apartments' || tempRecord.recordtype.name == 'Affordable'){
          for(sstWrapper s :floorplans){
            if(s.sst.Apt_Space_Type__c == null){
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Empty Space Type Name Found');
              ApexPages.addMessage(myMsg);
              isError = true;
            }
              if(dupeCheck.get(s.sst.Apt_Space_Type__c) == null){
                  dupeCheck.put(s.sst.Apt_Space_Type__c, s.sst);
              }
              else{
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate Space Type Found');
                  ApexPages.addMessage(myMsg);
                  isError = true;
              }
    if(s.sst.Apt_Space_Type__c != null){
    s.sst.Reis_Space_Type__c = s.sst.Apt_Space_Type__c;
    }
      insertList.add(s.sst);
    }
      }else if(tempRecord.recordtype.name == 'Senior Housing'){
          for(sstWrapper s :floorplans){
              if(dupeCheck.get(s.sst.Senior_Housing_Space_Type__c) == null){
                  dupeCheck.put(s.sst.Senior_Housing_Space_Type__c, s.sst);
              }
              else{
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate Space Type Found');
                  ApexPages.addMessage(myMsg);
                  isError = true;
              }

              if(s.sst.Senior_Housing_Space_Type__c != null){
                  s.sst.Reis_Space_Type__c = s.sst.Senior_Housing_Space_Type__c;
              }
              system.debug(s.sst.Senior_Housing_Space_Type__r.Name);
              if((s.sst.Senior_Housing_Space_Type__r.Name == 'Memory Care' || s.sst.Senior_Housing_Space_Type__r.Name == 'Assisted Living')&&((s.sst.SR_Housing_Care_Low_New__c > s.sst.Base_Low_New__c) || (s.sst.SR_Housing_Care_High_New__c > s.sst.Base_High_New__c))){
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Care rent cannot be greater than base rent');
                  ApexPages.addMessage(myMsg);
                  isError = true;
              }
        insertList.add(s.sst);
        }
      }
  if(!isError){
    upsert insertList;
  }
  return !isError;
}

  public void AddToConList(){
    //adds a new contact to the contact wrapper / related list
    contactList = contactWrapper.addNewCon(contactList);
  }

  public Boolean insertContacts(){
    //save method for the contacts, there was no indication that we had to check for items to change
    //so we just update all the records with the values we have
    List<Survey_Contact__c> insertList = new List<Survey_Contact__c>();
    List<Survey_Contact__c> insertList2 = new List<Survey_Contact__c>();
    List<Survey_Contact__c> updateSCList = new List<Survey_Contact__c>();
    List<Reis_Contact__c> updateRCList = new List<Reis_Contact__c>();
    List<Reis_Contact__c> newContactList = new List<Reis_Contact__c>();
    for(contactWrapper c :contactList){
      system.debug(c);
      if(c.newcon && c.con.Contact__c == null){
        Reis_Contact__c r = c.rCon;
        r.name = r.first_name__c+' '+r.last_name__c;
        newContactList.add(r);
        insertList.add(c.con);
        c.newCon = false;
      }
      else if(c.newcon && c.con.Contact__c != null){
        c.con.Property_Survey__c = recordId;
        insertList2.add(c.con);
        c.newCon = false;
      }
      else{
        if(c.con.id != null && c.rcon.id != null){
          updateSCList.add(c.con);
          updateRCList.add(c.rcon);
        }
      }
    }

    if(insertList2.size() > 0){
      try{
        insert insertList2;
        system.debug(insertList2);
      }
      catch(Exception e){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+ 'At Line: '+e.getLineNumber());
        ApexPages.addMessage(myMsg);
        return FALSE;
      }
    }

    if(insertList.size() > 0 && newContactList.size() > 0){
      try{
        insert newContactList;
        Integer count = 0;
        for(Survey_Contact__c c :insertList){
            c.Property_Survey__c = recordId;
            c.Contact__c = newContactList[count].id;
            count++;
        }
        insert insertList;

      }
      catch(Exception e){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+ 'At Line: '+e.getLineNumber());
        ApexPages.addMessage(myMsg);
        return FALSE;
      }
    }
    if(updateSCList.size() > 0 && updateRCList.size() > 0){
      try{
        update updateSCList;
        update updateRCList;
      }
      catch(Exception e){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+ 'At Line: '+e.getLineNumber());
        ApexPages.addMessage(myMsg);
      }
    }
    getContacts();
    return TRUE;
  }

  public PageReference sumbitForApproval(){
    //baisc submit for approval
  try{
    Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
    req.setComments('Submitting request for approval'); // You can make comments dynamic
    req.setObjectId(RecordId);
    Approval.ProcessResult result = Approval.process(req);
  }
  catch (Exception ex){
    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
    ApexPages.addMessage(myMsg);
  }
  //return submitPageRef;
  return null;
}

//added by sumedha for senior housing
  public SeniorHousingSurveyOverrideController(ApexPages.StandardController controller) {
    //constructor
    system.debug('new run of the controller');
    recordId = controller.getId();
    staticRecordId = controller.getId();
    tempRecord = getTheRecord();
    contactList = getContacts();
    generateAmenityList();
    goodToClose = false;
    if(tempRecord.recordtype.name == 'Apartments'){
      floorplans = getFloorPlanMap();
    }
    //added by sumedha for senior housing
   if(tempRecord.recordtype.name == 'Senior Housing'){
      floorplans = getSeniorSurveyMap();
    }


  }



  public list<sstWrapper> floorplansOrigin{get;set;}
  public void loadInformation(){
    tempRecord = getTheRecord();
    floorplansOrigin = new list<sstWrapper>();
    unChangedRecord = tempRecord.clone(true, true, true, true);
    system.debug(recordCompare(unChangedRecord,tempRecord));
    contactList = getContacts();
    generateAmenityList();
    if(tempRecord.recordtype.name == 'Apartments'){
      floorplans = getFloorPlanMap();
      floorplansOrigin = getFloorPlanMap();
    }
    if(tempRecord.recordtype.name == 'Senior Housing'){
      floorplans = getSeniorSurveyMap();
      floorplansOrigin = getSeniorSurveyMap();
    }

    system.debug(recordCompare(floorplansOrigin,floorplans));
  }

  public class sstWrapper{
    //survey space tpye wrapper so we can create new survey space type records in our related list
    public Survey_Space_Type__c sst {Get;set;}
    public boolean newSst {get;set;}

    public sstWrapper(){
      this.sst = new Survey_Space_Type__c();
      this.newSst =false;
    }

    public sstWrapper(Survey_Space_Type__c s){
      this.sst = s;
      this.newSst =false;
    }

  }

  @remoteAction
  public static void logACall(String a, String rid){
    //log a call remote action for use on the VF Page
    //depricated
    task t = new task();
    t.whatId = rid;
    t.subject = 'Call';
    t.subject =a;
    t.status = 'Completed';
    t.description = 'Call at: '+system.now();
    t.activityDate = system.today();
    insert t;
    system.debug(t.id);
  }

  @remoteAction
  public static void remoteSave(Boolean b, String rid){
    //used in out reocrd type change validation
    Property_Survey__c a = [select id, needs_qa_review__c from Property_Survey__c where id = :rid limit 1];
    a.needs_qa_review__c = b;
    update a;
  }

  @remoteAction
  public static void remoteAddComment(string newComment, String rid){
    //used on all most all of our validations
    Property_Survey__c a = [select id, needs_qa_review__c, Property_Notes_New__c, QC_Notes_New__c from Property_Survey__c where id = :rid limit 1];
    //a.QC_Notes_New__c += ' ('+system.now()+') '+newComment;
    if(a.Property_Notes_New__c == null){
      a.Property_Notes_New__c = ' ('+system.now()+') '+newComment;
    }
    else{
      a.Property_Notes_New__c += ' ('+system.now()+') '+newComment;
    }

    update a;
  }

  @remoteAction
  public static Boolean remoteSiteCheck(String toCheck, Boolean propertySite){
    //used on site fields to determine if they are on our restricted list, if they are, we blank out on the client side
    system.debug(propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()));
    system.debug(toCheck);
    if(propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()) != null){
      if(propertySite && propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()).property_site__c){
        return true;
      }
      else if(!propertySite && propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()).Survey_Site__c){
        return true;
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }
  }
  @remoteAction
  public static Boolean isRentInRange(String surveySectorId, String msaId, String spaceTypeName, String amount){
    Decimal convertedAmount = Decimal.valueOf(!String.isEmpty(amount)?amount:'0').setScale(2,RoundingMode.HALF_EVEN);
    system.debug(surveySectorId);
    system.debug(msaId);
    system.debug(spaceTypeName);
    system.debug(amount);
    List<Rent_Range__c> rentRangeList = [SELECT id, Minimum_Allowable_Rent__c, Maximum_Allowable_Rent__c FROM Rent_Range__c WHERE MSA__c = :msaId AND Sector__c = :surveySectorId AND Space_Type__c = :spaceTypeName];
    if(rentRangeList.size() > 1){
      THROW New customValidationException('More than one Rent_Range__c record match the criteria');
      return false;
    }
    else if (rentRangeList.size() < 1){
      THROW New customValidationException('No Rent_Range__c records match the criteria');
      return false;
    }
    else{
      if(convertedAmount >= rentRangeList[0].Minimum_Allowable_Rent__c && convertedAmount <= rentRangeList[0].Maximum_Allowable_Rent__c){
        return true;
      }
      else{
        return false;
      }
    }
    return false;
  }

  public void isRentInRange(List<sstWrapper> sstList){
    Date d = date.today();
    String dt = DateTime.newInstance(d.year(),d.month(),d.day()).format('MM-dd-YYYY');
    List<String> Namelist = new List<String>();
    List<String> nameException = new List<String>();
    Set<String> processedNames = new Set<String>();
    Boolean rentRangesFound = false;
    Map<String, String> nameToIdMap = new map<String,String>();
    List<String> idList = new List<String>();

    for(sstWrapper s :sstList){
      idList.add(s.sst.Reis_Space_Type__c);
    }
    map<id,Reis_Space_Type__c> spaceMap = new map<id,Reis_Space_Type__c>([select id, Name from  Reis_Space_Type__c where id in : idList]);
    for(Reis_Space_Type__c r :spaceMap.values()){
      system.debug(r);
      if(r.Name.contains('Studio')){
        Namelist.add('Studio');
      }
      else{
        Namelist.add(r.Name.split(' ')[0]+' Bedroom');
      }
    }
    for(Rent_Range__c rr :[SELECT id,Space_Type__c, Minimum_Allowable_Rent__c, Maximum_Allowable_Rent__c FROM Rent_Range__c WHERE MSA__c = :tempRecord.Survey_MSA__c AND Sector__c = :tempRecord.Survey_Sector__c AND Space_Type__c IN :Namelist]){
      system.debug('In rent range loop');
      for(sstWrapper s :sstList){
        String convertedName;
        if(spaceMap.get(s.sst.Reis_Space_Type__c).Name .contains('Studio')){
          convertedName = 'Studio';
        }
        else{
          convertedName = spaceMap.get(s.sst.Reis_Space_Type__c).Name.split(' ')[0]+' Bedroom';
        }
        system.debug(convertedName);
        system.debug(rr.Space_Type__c);
        if(convertedName == rr.Space_Type__c){
          system.debug('1');
          if((s.sst.Average_Rent_New__c >= rr.Maximum_Allowable_Rent__c) || (s.sst.Average_Rent_New__c <= rr.Minimum_Allowable_Rent__c)){
            system.debug('2');
            if(tempRecord.QA_Reason_Notes__c == null){
              system.debug('3');
              tempRecord.Needs_QA_Review__c = true;
              tempRecord.QA_Reason_Notes__c = '('+dt+') '+s.sst.Reis_Space_Type__r.Name+': Average rent is outside of allowable range.';
              system.debug('Hi1');
              processedNames.add(rr.Space_Type__c);
              system.debug(processedNames);
            }
            else if(!tempRecord.QA_Reason_Notes__c.contains(s.sst.Reis_Space_Type__r.Name+': Average rent is outside of allowable range.')){
              system.debug('4');
              tempRecord.Needs_QA_Review__c = true;
              tempRecord.QA_Reason_Notes__c += '('+dt+') '+s.sst.Reis_Space_Type__r.Name+': Average rent is outside of allowable range.';
              system.debug('Hi2');
              processedNames.add(rr.Space_Type__c);
              system.debug(processedNames);
            }
          }
          else if((s.sst.Average_Rent_New__c <= rr.Maximum_Allowable_Rent__c) && (s.sst.Average_Rent_New__c >= rr.Minimum_Allowable_Rent__c)){
            system.debug('5');
            processedNames.add(rr.Space_Type__c);
          }
        }
      }
      rentRangesFound = true;
    }
    system.debug(processedNames);
      for(Reis_Space_Type__c r :[select id, Name from  Reis_Space_Type__c where id in : idList]){
        for(String n : Namelist){
          String convertedName;
          if(r.Name .contains('Studio')){
            convertedName = 'Studio';
          }
          else{
            convertedName = r.Name.split(' ')[0]+' Bedroom';
          }
          if(convertedName == n){
            system.debug('6');
            if(!processedNames.contains(n)){
              system.debug('7');
                if(tempRecord.QA_Reason_Notes__c == null){
                  system.debug('8');
                tempRecord.Needs_QA_Review__c = true;
                tempRecord.QA_Reason_Notes__c = '('+dt+') '+r.Name+': No rent range was found for this sector, MSA, type combination.';
                }
                else if(!tempRecord.QA_Reason_Notes__c.contains(r.Name+': No rent range was found for this sector, MSA, type combination.')){
                  system.debug('9');
                  tempRecord.Needs_QA_Review__c = true;
                  tempRecord.QA_Reason_Notes__c += '('+dt+') '+r.Name+': No rent range was found for this sector, MSA, type combination.';
                }
            }
          }
        }
      }
  }

  public class customValidationException extends Exception{}

  public class amenitiesWrapper{
    //amenitiesWrapper is used to add new records to our related lists
    // also contains the type field so we can determine which list to populate with which record
    public Survey_Amenities__c sa {get;set;}
    public String type {get;set;}

    public amenitiesWrapper(){
      this.sa = new Survey_Amenities__c();
      this.type = '';
    }

    public amenitiesWrapper(Survey_Amenities__c s, String t){
      this.sa = s;
      this.type = t;
    }
  }

List<amenitiesWrapper> orginalAmenList {get;set;}
  public void generateAmenityList(){
    //populate our amenity wrapper list
    //set our new amenity to a blank amenity so we can create new ones quickly
    newAm = new Survey_Amenities__c();
    amenitiesList = new Map<String,List<amenitiesWrapper>>();
    amenitiesList.put('Complex', new List<amenitiesWrapper>());
    amenitiesList.put('Unit', new List<amenitiesWrapper>());
    amenitiesList.put('Utilities', new List<amenitiesWrapper>());
    for(Survey_Amenities__c s :[select id,Amenity__r.Has_Quantity__c, Delete_By_Foundation__c,Apartment_Amenity__c,Quantity_New__c, Amenity__r.Name, Amenity__r.Type__c from Survey_Amenities__c where Property_Survey__c = :recordId AND Amenity__c != null]){
      if(amenitiesList.get(s.Amenity__r.Type__c) == null){
        amenitiesList.put( s.Amenity__r.Type__c,new List<amenitiesWrapper>());
        amenitiesList.get(s.Amenity__r.Type__c).add(new amenitiesWrapper(s, s.Amenity__r.Type__c));
      }else{
        amenitiesList.get(s.Amenity__r.Type__c).add(new amenitiesWrapper(s, s.Amenity__r.Type__c));
      }
    }
  }

  public void addNewAmmenity(){
    //inserts our new amenity and updates our pages viewing list
    List<amenitiesWrapper> fullList = new list<amenitiesWrapper>();
    For(List<amenitiesWrapper> a :amenitiesList.values()){
      fulllist.addAll(a);
    }

    newAm.Property_Survey__c = recordId;
    newAm.Amenity__c = newAm.Apartment_Amenity__c;
    Amenity__c am = [select name, Type__c from Amenity__c where id = :newAm.Apartment_Amenity__c limit 1];
    for(amenitiesWrapper a :fulllist){
      if(!a.sa.Delete_By_Foundation__c && a.type == am.type__c && am.name.contains('No')){
        return;
      }
      else if(a.type == am.Type__c && a.sa.Amenity__r.Name.contains('No')){
        return;
      }
    }
    insert newAm;
    generateAmenityList();
  }



//depricated
  public String removalId {get;set;}
  public void removeAmenity(){
    system.debug(removalId);
    Survey_Amenities__c ps = [select id from Survey_Amenities__c where id = :removalId limit 1];
    delete ps;
    generateAmenityList();
  }
  public static boolean recordCompare(list<Object> orginial, list<Object> vfRecord){
    system.debug(orginial);
    system.debug(vfRecord);
    if((orginial != null && vfRecord != null) && orginial.size() == vfRecord.size()){
      Integer size = orginial.size();
      for(Integer i = 0; i != size; i++){
        if(!recordCompare(orginial[i],vfRecord[i])){
          return false;
        }
      }
      return true;
    }
    else{
      return false;
    }
  }


  public static boolean recordCompare(Object orginial, Object vfRecord){
    if(orginial != vfRecord){
      if(orginial != null && vfRecord != null){
        try{
          map<string,object> orgMap = (map<string,object>)JSON.deserializeUntyped(JSON.serialize(orginial));
          map<string,object> vfMap = (map<string,object>)JSON.deserializeUntyped(JSON.serialize(vfRecord));
          for(String key :vfMap.keySet()){
            if(orgMap.containsKey(key)?vfMap.get(key)!=orgMap.get(key):vfMap.get(key)!=null){
              if(orgMap.containsKey(key)){
                system.debug('compareSobjects: failed - before/after missmatch for key: '+key);
              }
              else{
                system.debug('compareSobjects: failed - vfMap is not null for key: '+key);
              }
              return false;
            }
          }
        }
        catch(Exception e){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'compareSobjects validation error. Please contact a system administrator. Error: '+e.getMessage()));
          return false;
        }
      }
      else{
        system.debug('compareSobjects: failed - on or more SObjects were null');
        return false;
      }
    }
    system.debug('compare success');
    return true;
  }

}