public class createMetricRecords{
    
    
    public static void createMetric(set<Id> PropertyIdList){
        
        system.debug('***PropertyIdList'+PropertyIdList);
        set<String> PoolDataSet=new Set<String>();
        List<Survey_Metric__c> surveyMetricList = new List<Survey_Metric__c>();
        
        if(PropertyIdList.size() > 0){
            List<Property_Survey__c> listPropertySurvey=[select Anchor_Avail_Current__c,HUD_Id__c,Tax_Credit_Compliance_Years_Remaining__c,Total_Units_Available_New__c,Project_Origin_New__c,Heating_Source_New__c,Minimum_Qualifying_Income__c,Maximum_Qualifying_Income__c,Unit_Designation_New__c,Needs_QA_Review__c,Expected_Month__c,Comp_Totals_Avail__c,SR_Housing_Insurance_All__c,SR_Housing_Insurance_Medicaid__c,SR_Housing_Insurance_Medicare__c,SR_Housing_Insurance_Private__c,SR_Housing_Insurance_Private_Pay__c,Anchor_Avail_New__c,TI_Non_Anchor_New__c,TI_Anchor_New__c,Non_Anchor_Lease_Term_New__c,Lease_Term_New__c,Returned_to_User__c,Sublet_Gross_Net_New__c,Opex_New__c,New_Construction__c,propertyId__c,Leasing_Incentives_New__c,Total_Available_New__c,queue_record__r.Owner.Name,OPSPoolId__c,
                                                         Status__c,Select_Code_New__c,Select_Code__c,Survey_Completed__c,Rent_Captured__c,Vacancy_Captured__c,Name,Id,queue_record__r.ownerId,Survey_Sector__r.Name,Survey_MSA__r.foundation_Id__c,Survey_submarket__r.foundation_id__c,Non_Anchor_Average_Rent_New__c,
                                                         Direct_Available_New__c,Direct_Average_Rent_New__c,Anchor_Lease_Term_New__c,Sublet_Available_New__c,Anchor_Average_Rent_New__c,Total_Anchor_Available_New__c,
                                                         Total_Non_Anchor_Available_New__c,Aged_Property__c,CAM__c,Survey_Data_Can_Be_Purged__c,Survey_Source__c,Pre_Leased_Percentage_New__c,Pre_Leased__c,Real_Estate_Taxes_New__c,
                                                         Rent_By__c,Tax_Credit_Year_Placed_Into_Service__c,TI_Allowance_New_New__c,TI_Allowance_Renew_New__c,Current_Available__c,Wait_List_Length__c,Property_Id__c,Property_Type__c,Survey_Sector__c,
                                                         Contract_Rent_Discount_Anchor_New__c,Survey_Submarket__c,Survey_MSA__c,Commission_New_New__c,Commission_Renew_New__c,Contract_Rent_Discount_New__c,Contract_Rent_Discount_NonAnchor_New__c,
                                                         Entry_Fee_Average_New__c,Free_Rent_New__c,Free_Rent_Anchor_New__c,Free_Rent_Non_Anchor_New__c,Has_Wait_List__c,Insurance__c,Expected_Increase__c,Rent_Utilities_New__c,Students_New__c,Opex__c,
                                                         (Select Id,Amenity__r.Type__c from Survey_Amenities__r),(Select CC_Avg_New__c,NCC_Avg_New__c,Id,MR_Free_Rent_New__c,TC_Free_Rent_New__c,MR_New__c,ami30RentAvg_New__c,ami40RentAvg_New__c,ami50RentAvg_New__c,ami60RentAvg_New__c,unknownAmiRentAvg_New__c,amiNonConventionalRentAvg_New__c,sec8RentAvg_New__c,sec8Vac_New__c,amiNonConventionalVac_New__c,unknownAmiVac_New__c,MR_Vac_New__c,Tax_Credit__c,Type__c,Future_Avg_New__c,Average_Rent_New__c,Lease_Term_New__c,Leasing_Incentives__c,Unit_Available_New__c,
                                                                                                                  High_Rent_New__c,Low_Rent_New__c,Discount_New__c,Total_Available__c,Vacant_Beds_New__c,Current_Available__c from Survey_Floor_Plans1__r),Surveyor__c,queue_record__c,queue_record__r.No_Of_Surveys__c
                                                         from property_Survey__c where Id=:PropertyIdList];
            for (Property_Survey__c propSurveyRec : listPropertySurvey){
                //Anantha 28/09 Removed as per RSI-595
            /*    if(propSurveyRec.Status__c!='In Team Lead Review' && !(propSurveyRec.Select_Code__c=='Z' || propSurveyRec.Select_Code__c=='E')) {
                    continue;
                }*/
                
                System.debug('propSurveyRec=>'+propSurveyRec);
                Survey_Metric__c surveyMetric = new Survey_Metric__c();
                
                if(propSurveyRec.Survey_Sector__r.Name=='Apt')
                    AptFieldMapping(surveyMetric,propSurveyRec);
                if(propSurveyRec.Survey_Sector__r.Name=='Aff')
                    AffFieldMapping(surveyMetric,propSurveyRec);
                if(propSurveyRec.Survey_Sector__r.Name=='Off' || propSurveyRec.Survey_Sector__r.Name=='Ind')
                    OffIndFieldMapping(surveyMetric,propSurveyRec);
                if(propSurveyRec.Survey_Sector__r.Name=='Ret')
                    RetFieldMapping(surveyMetric,propSurveyRec);
                if(propSurveyRec.Survey_Sector__r.Name=='Sth')
                    SthFieldMapping(surveyMetric,propSurveyRec);
                if(propSurveyRec.Survey_Sector__r.Name=='Snr')
                    SnrFieldMapping(surveyMetric,propSurveyRec);
                if(propSurveyRec.Survey_Sector__r.Name=='Sto')
                    StoFieldMapping(surveyMetric,propSurveyRec);
                
                system.debug('propSurveyRec.queue_record__r.ownerId=>'+propSurveyRec.queue_record__r.ownerId);
                if(propSurveyRec.queue_record__r.Owner.Name!='Pool Queue'){
                    surveyMetric.Surveyor__c = propSurveyRec.queue_record__r.ownerId;    
                }
                if(propSurveyRec.Property_Id__c != null)
                    surveyMetric.Property_Id__c = propSurveyRec.Property_Id__c;
                
                if(propSurveyRec.Property_Type__c != null)
                    surveyMetric.Property_Type__c = propSurveyRec.Property_Type__c;
                
                if (propSurveyRec.Survey_Sector__c != null)
                    surveyMetric.Survey_Sector__c = propSurveyRec.Survey_Sector__c;
                
                if(propSurveyRec.Survey_Submarket__c != null)
                    surveyMetric.Survey_Submarket__c = propSurveyRec.Survey_Submarket__c;
                
                if(propSurveyRec.Survey_MSA__c != null)
                    surveyMetric.MSA__c = propSurveyRec.Survey_MSA__c;
                
                surveyMetric.Survey_Number__c=propSurveyRec.name;
                surveyMetric.PropertyId__c=propSurveyRec.PropertyId__c;
                surveyMetric.Aged__c=propSurveyRec.Aged_Property__c;
                surveyMetric.Survey_Completed__c=propSurveyRec.Survey_Completed__c;
                //Anantha RSI-539
                surveyMetric.New_Construction__c=propSurveyRec.New_Construction__c;
                surveyMetric.QA__c=propSurveyRec.Needs_QA_Review__c;
                surveyMetric.Returned_To_User__c=propSurveyRec.Returned_to_User__c;
                if(propSurveyRec.Survey_Source__c!=null && propSurveyRec.Survey_Source__c.containsIgnoreCase('Phone'))
                    surveyMetric.Phone_Survey__c = true;
                else
                    surveyMetric.Phone_Survey__c = false;
                if(propSurveyRec.Survey_Source__c!=null && propSurveyRec.Survey_Source__c.containsIgnoreCase('Web'))
                    surveyMetric.Web_Survey__c = true;
                else
                    surveyMetric.Web_Survey__c = false;
                
  
                surveyMetric.Completion_Date__c=system.now();
                
                //Anantha 28/09 RSI-595
                if(propSurveyRec.Status__c=='Ready for Publish')
                    surveyMetric.Foundation_Accepted_Date__c=system.now();
                
                surveyMetric.Survey_Status__c=propSurveyRec.Status__c;
                
                //Anantha 29/08 Added to update the Next Queue Date
                if((propSurveyRec.Survey_Completed__c || propSurveyRec.Select_Code_New__c=='Z' || propSurveyRec.Select_Code_New__c=='E')){
                    PoolDataSet.add(propSurveyRec.Property_Id__c);
                }
                surveyMetric.Select_Code__c=propSurveyRec.Select_Code_New__c;
                surveyMetricList.add(surveyMetric);
                system.debug('***surveyMetricList'+surveyMetricList);
            }
            List<Ops_Pool_Data__c> PoolDataList=new List<Ops_Pool_Data__c>();
            if(PoolDataSet.size()>0){
                PoolDataList=[Select Id,Next_Queue_Date__c from ops_pool_data__c where Property_Id__c=:PoolDataSet];
            }
            for(ops_pool_data__c PoolData:PoolDataList){
                PoolData.Next_Queue_Date__c=Date.Today().addMonths(3);
            }
            upsert surveyMetricList Survey_Number__c;
            update PoolDataList;
            system.debug('surveyMetricList After Insert=>'+surveyMetricList);
            
        }
        
    }
    
    private static void AptFieldMapping(Survey_Metric__c surveyMetric,Property_Survey__c propSurveyRec){
        
        surveyMetric.Leasing_Incentives__c = false;
        surveyMetric.Vacancy_Not_Zero__c = false;
        surveyMetric.Vacancy__c=false;
        surveyMetric.Average_Rent__c = false;
        surveyMetric.Lease_Term__c = false;
        surveyMetric.Free_Rent__c=false;
        surveyMetric.Free_Rent_Not_Zero__c=false;
        surveyMetric.Total_FP_Vacancy_Adding_to_Total__c=false;
        
        Decimal SumOfAvailable=0;
        if(propSurveyRec.Total_Available_New__c!=null){
            surveyMetric.Vacancy__c=true;
            if(propSurveyRec.Total_Available_New__c>0){
                surveyMetric.Vacancy_Not_Zero__c = true;
            }
        }
        if(propSurveyRec.Leasing_Incentives_New__c!=null && propSurveyRec.Leasing_Incentives_New__c!=''){
            surveyMetric.Leasing_Incentives__c = true;    
        }
        
        for (Survey_Space_Type__c surveyfloor : propSurveyRec.Survey_Floor_Plans1__r){
            if(surveyfloor.Average_Rent_New__c  != null){
                surveyMetric.Average_Rent__c = true;
            }
            if(surveyfloor.Lease_Term_New__c  != null){
                surveyMetric.Lease_Term__c = true;
            }
            surveyMetric.Rent__c=propSurveyRec.Rent_Captured__c;
            if(surveyfloor.Unit_Available_New__c!=null){
                SumOfAvailable+=surveyfloor.Unit_Available_New__c;
            }
            if(surveyFloor.Discount_New__c!=null){
                surveyMetric.Free_Rent__c=true;
                if(Decimal.valueOf(surveyFloor.Discount_New__c)>0){
                    surveyMetric.Free_Rent_Not_Zero__c=true;
                }
            }
        }
        if(SumOfAvailable==propSurveyRec.Total_Available_New__c){
            surveyMetric.Total_FP_Vacancy_Adding_to_Total__c=true;            
        }
        
    }
    
    private static void OffIndFieldMapping(Survey_Metric__c surveyMetric,Property_Survey__c propSurveyRec){
        surveyMetric.Direct_Available__c = false;
        surveyMetric.Direct_Average_Rent__c = false;
        surveyMetric.Sublet_Available__c = false;
        surveyMetric.Sublet_Available_Above_Zero__c = false;
        surveyMetric.Rent_Term__c=false;
        surveyMetric.Opex__c = false;
        surveyMetric.Real_Estate_Tax__c = false;
        surveyMetric.Lease_Term__c = false;
        surveyMetric.TI_New__c = false;
        surveyMetric.Free_Rent__c = false;
        surveyMetric.TI_Renew__c = false;
        surveyMetric.Contract_Rent_Discount__c = false;
        surveyMetric.Contract_Rent_Discount_Above_Zero__c=false;
        surveyMetric.Commission_Renew__c = false;
        surveyMetric.Commission_New__c = false;
        surveyMetric.Free_Rent_Above_Zero__c=false;
        
        if(propSurveyRec.Direct_Available_New__c != null){
            surveyMetric.Direct_Available__c = true;
        }
        
        if(propSurveyRec.Direct_Average_Rent_New__c!= null){
            surveyMetric.Direct_Average_Rent__c = true;
        }
        
        if(propSurveyRec.Sublet_Available_New__c!= null){
            surveyMetric.Sublet_Available__c = true;
            if(propSurveyRec.Sublet_Available_New__c> 0){
                surveyMetric.Sublet_Available_Above_Zero__c = true;
            }
        }
        
        if(propSurveyrec.Sublet_Gross_Net_New__c!=null){
            surveyMetric.Rent_Term__c=true;
        }
        
        if(propSurveyRec.Opex_New__c != null)
            surveyMetric.Opex__c = true;
        if(propSurveyRec.Real_Estate_Taxes_New__c != null)
            surveyMetric.Real_Estate_Tax__c = true;
        if(propSurveyRec.Lease_Term_New__c  != null){
            surveyMetric.Lease_Term__c = true;
        }
        if(propSurveyRec.TI_Allowance_New_New__c != null)
            surveyMetric.TI_New__c = true;
        
        if(propSurveyRec.TI_Allowance_Renew_New__c != null)
            surveyMetric.TI_Renew__c = true;
        
        if(propSurveyRec.Free_Rent_New__c != null){
            surveyMetric.Free_Rent__c = true;
            if(propSurveyRec.Free_Rent_New__c>0)
                surveyMetric.Free_Rent_Above_Zero__c=true;
        }
        if(propSurveyRec.Contract_Rent_Discount_New__c != null){
            surveyMetric.Contract_Rent_Discount__c = true;
            if(propSurveyRec.Contract_Rent_Discount_New__c>0){
                surveyMetric.Contract_Rent_Discount_Above_Zero__c=true;
            }
        }
        if(propSurveyRec.Commission_New_New__c != null)
            surveyMetric.Commission_New__c = true;
        
        if(propSurveyRec.Commission_Renew_New__c != null)
            surveyMetric.Commission_Renew__c = true;
        
    }
    
    private static void RetFieldMapping(Survey_Metric__c surveyMetric,Property_Survey__c propSurveyRec){
        if(propSurveyRec.CAM__c != null)
            surveyMetric.CAM__c = true;
        if(propSurveyRec.Contract_Rent_Discount_Anchor_New__c != null)
            surveyMetric.Anchor_Contact_Rent_Discount__c = true;
        
        if(propSurveyRec.Contract_Rent_Discount_Anchor_New__c > 0)
            surveyMetric.Anchor_Contact_Rent_Discount_Not_Zero__c = true;
        if(propSurveyRec.Contract_Rent_Discount_New__c !=0)
            surveyMetric.Contract_Rent_Discount_Not_Zero__c = true;
        if(propSurveyRec.Contract_Rent_Discount_New__c >0)
            surveyMetric.Contract_Rent_Discount_Above_Zero__c = true;
        
        if(propSurveyRec.Total_Anchor_Available_New__c != null){
            surveyMetric.Anchor_Vacancy__c = true;
        }
        if(propSurveyRec.Total_Non_Anchor_Available_New__c!= null){
            surveyMetric.NonAnchor_Vacancy__c = true;
        }
        
        if(propSurveyRec.Anchor_Average_Rent_New__c!=null){
            surveyMetric.Anchor_Rent__c = true;
        }
        if(propSurveyRec.Non_Anchor_Average_Rent_New__c!= null){
            surveyMetric.NonAnchor_Rent__c = true;
        }
        if(propSurveyRec.Anchor_Lease_Term_New__c  != null){
            surveyMetric.Anchor_LT__c = true;
        }
        if(propSurveyRec.Non_Anchor_Lease_Term_New__c!=null && propSurveyRec.Non_Anchor_Lease_Term_New__c>0){
            surveyMetric.NonAnchor_Lease_Term__c=true;
        }
        if(propSurveyRec.Opex_New__c != null)
            surveyMetric.Opex__c = true;
        if(propSurveyRec.Real_Estate_Taxes_New__c != null)
            surveyMetric.Real_Estate_Tax__c = true;
        
        if(propSurveyRec.TI_Non_Anchor_New__c != null || propSurveyRec.TI_Anchor_New__c!=null)
            surveyMetric.Tenant_Improvement__c = true;
        
        if(propSurveyRec.Anchor_Avail_New__c>0 || propSurveyRec.Anchor_Avail_Current__c>0)
            surveyMetric.Survey_with_Anchor__c=true;
        
        Decimal freeRent;
        if(propSurveyRec.Free_Rent_Anchor_New__c != null || propSurveyRec.Free_Rent_Non_Anchor_New__c != null){
            Decimal FreeRentAnch=propSurveyRec.Free_Rent_Anchor_New__c==null?0:propSurveyRec.Free_Rent_Anchor_New__c;
            Decimal FreeRentNonAnch=propSurveyRec.Free_Rent_Non_Anchor_New__c==null?0:propSurveyRec.Free_Rent_Non_Anchor_New__c;
            freeRent = FreeRentAnch+FreeRentNonAnch;
        }
        if(freeRent !=0){
            surveyMetric.Free_Rent__c=true;
            if(freeRent>0)
                surveyMetric.Free_Rent_Above_Zero__c = true;
            
            if(propSurveyRec.Free_Rent_Anchor_New__c != null || propSurveyRec.Free_Rent_Non_Anchor_New__c != null)
                surveyMetric.Free_Rent_Not_Zero__c = true;
        }
        
    }
    
    private static void SnrFieldMapping(Survey_Metric__c surveyMetric,Property_Survey__c propSurveyRec){
        if(propSurveyRec.Comp_Totals_Avail__c!=null)
            surveyMetric.Vacancy__c=true;
        if(propSurveyRec.Entry_Fee_Average_New__c!=null)
            surveyMetric.Entry_Fee_Average__c=true;
        if(propSurveyRec.Expected_Month__c!=null)
            surveyMetric.Monthly_Income__c=true;
        if(propSurveyRec.Expected_Increase__c!=null){
            surveyMetric.Percent_increase__c=true;
            
            if(propSurveyRec.SR_Housing_Insurance_All__c || propSurveyRec.SR_Housing_Insurance_Medicaid__c || propSurveyRec.SR_Housing_Insurance_Medicare__c || propSurveyRec.SR_Housing_Insurance_Private__c || propSurveyRec.SR_Housing_Insurance_Private_Pay__c){
                surveyMetric.Insurance__c=true;    
            }
        }
    }
    
    private static void SthFieldMapping(Survey_Metric__c surveyMetric,Property_Survey__c propSurveyRec){
        if(propSurveyRec.Rent_Utilities_New__c != null)
            surveyMetric.Percent_Rent_Utilities__c = true;
        if(propSurveyRec.Students_New__c != null)
            surveyMetric.Percent_Students__c = true;
        if(propSurveyRec.Pre_Leased_Percentage_New__c != null)
            surveyMetric.Prelease__c = true;
        if(propSurveyRec.Pre_Leased__c)
            surveyMetric.Prelease_is_Yes__c = true;
        if(propSurveyRec.Rent_By__c != null)
            surveyMetric.Rent_By__c = true;
        if(propSurveyRec.Total_Available_New__c!=null)
            surveyMetric.Vacancy__c=true;
        for (Survey_Space_Type__c surveyfloor : propSurveyRec.Survey_Floor_Plans1__r){
            if(surveyfloor.Future_Avg_New__c  != null || surveyFloor.Average_Rent_New__c!=null){
                surveyMetric.Rent__c = true;
            }
            if(surveyFloor.Discount_New__c!=null){
                surveyMetric.Free_Rent__c=true;
                if(decimal.valueOf(surveyFloor.Discount_New__c)>0)
                    surveyMetric.Free_Rent_Above_Zero__c=true;
                if(decimal.valueOf(surveyFloor.Discount_New__c)!=0)
                    surveyMetric.Free_Rent_Not_Zero__c=true;
            }
        }
        
    }
    
    private static void AffFieldMapping(Survey_Metric__c surveyMetric,Property_Survey__c propSurveyRec){
        //Project_Origin_New__c,Property_Type__c,Heating_Source_New__c,Minimum_Qualifying_Income__c,Maximum_Qualifying_Income__c,Unit_Designation_New__c
        surveyMetric.Floor_Plan_Vacancy__c = false;
        surveyMetric.Tax_Credit_Survey__c=false;
        surveyMetric.Project_Origin__c=false;
        surveyMetric.Minimum_Eligible_Income__c=false;
        surveyMetric.Maximum_Eligible_Income__c=false;
        surveyMetric.Heat_Source__c=false;
        surveyMetric.Unit_Designation__c=false;
        surveyMetric.Tax_Credit_Year_Placed_In_Service__c=false;
        surveyMetric.Tax_Credit_Compliance_Years_Remaining__c=false;
        surveyMetric.Has_Wait_List__c = false;
        surveyMetric.HUD_ID__c = false;
        surveyMetric.Rent__c=false;
        surveyMetric.MR_Free_Rent__c=false;
        surveyMetric.Floor_Plan_Vacancy__c = false;
        surveyMetric.MR_Free_Rent_Above_Zero__c=false;
        surveyMetric.Social_Services_Amenities__c=false;
        if(propSurveyRec.Property_Type__c!=null){
            if(propSurveyRec.Property_Type__c.contains('TC'))
                surveyMetric.Tax_Credit_Survey__c=true;
        }
        if(propSurveyRec.Project_Origin_New__c!=null)
            surveyMetric.Project_Origin__c=true;
        if(propSurveyRec.Minimum_Qualifying_Income__c!=null)
            surveyMetric.Minimum_Eligible_Income__c=true;
        if(propSurveyRec.Maximum_Qualifying_Income__c!=null)
            surveyMetric.Maximum_Eligible_Income__c=true;
        if(propSurveyRec.Heating_Source_New__c!=null)
            surveyMetric.Heat_Source__c=true;
        if(propSurveyRec.Unit_Designation_New__c!=null)
            surveyMetric.Unit_Designation__c=true;
        if(propSurveyRec.Tax_Credit_Year_Placed_Into_Service__c!=null)
            surveyMetric.Tax_Credit_Year_Placed_In_Service__c=true;
        if(propSurveyRec.Tax_Credit_Compliance_Years_Remaining__c!=null)
            surveyMetric.Tax_Credit_Compliance_Years_Remaining__c=true;
        
        if(propSurveyRec.Has_Wait_List__c != null && propSurveyRec.Has_Wait_List__c != '')
            surveyMetric.Has_Wait_List__c = true;
        if(propSurveyRec.Wait_List_Length__c != null)
            surveyMetric.Wait_List_Length__c = true;
        if(propSurveyRec.HUD_Id__c != null)
            surveyMetric.HUD_ID__c = true;
        //sec8Vac_New__c,amiNonConventionalVac_New__c,unknownAmiVac_New__c,MR_Vac_New__c,Tax_Credit__c
        for (Survey_Space_Type__c surveyfloor : propSurveyRec.Survey_Floor_Plans1__r){
            if(surveyfloor.sec8Vac_New__c  != null || surveyFloor.unknownAmiVac_New__c!=null || surveyFloor.amiNonConventionalVac_New__c!=null || surveyFloor.MR_Vac_New__c!=null || surveyFloor.Tax_Credit__c!=null ){
                surveyMetric.Floor_Plan_Vacancy__c = true;
            }
            if(surveyFloor.MR_New__c!=null || surveyFloor.ami30RentAvg_New__c!=Null || surveyFloor.ami40RentAvg_New__c!=null || surveyFloor.ami50RentAvg_New__c!=null || surveyFloor.ami60RentAvg_New__c!=null || surveyFloor.unknownAmiRentAvg_New__c!=null || surveyFloor.amiNonConventionalRentAvg_New__c!=null || surveyFloor.sec8RentAvg_New__c!=null){
                surveyMetric.Rent__c=true;                 
            }
            if(surveyFloor.TC_Free_Rent_New__c!=null)
                surveyMetric.Tax_Credit_Free_Rent__c=true;
            if(surveyFloor.MR_Free_Rent_New__c!=null && propSurveyRec.Property_Type__c.containsignorecase('MI')){
                surveyMetric.MR_Free_Rent__c=true;
                if(surveyFloor.MR_Free_Rent_New__c>0)
                    surveyMetric.MR_Free_Rent_Above_Zero__c=true;
            }
            
        }
        
        for (Survey_Amenities__c surveyAmenity : propSurveyRec.Survey_Amenities__r){
            if(surveyAmenity.Amenity__r.type__c=='Social Services')
                surveyMetric.Social_Services_Amenities__c=true;
        }
    }
    
    private static void StoFieldMapping(Survey_Metric__c surveyMetric,Property_Survey__c propSurveyRec){
        surveyMetric.Total_Available__c=false;
        if(propSurveyRec.Total_Available_New__c!=null)
            surveyMetric.Total_Available__c=true;
        surveyMetric.Rent__c=propSurveyrec.Rent_Captured__c;
    }
}