public Class QueueAssignment{
    String SurveyorUserId = '';
    private ApexPages.StandardController controller;
    
    public QueueAssignment(ApexPages.StandardController controller) {
        
        this.controller = controller;
        
        SurveyorUserId = ApexPages.currentPage().getParameters().get('Id');
        
    }
    
    public PageReference AssignSurvey(){
       PageReference page = new PageReference('/' + controller.getId());
        List<Queue__c> QueueList=new List<Queue__c>();
        List<Queue_configuration__c> QueueconfigList=new List<Queue_configuration__c>();
        List<Property_Survey__c> PropSurveyList=new List<Property_Survey__c>();
        QueueAssignmentUtils.AssignQueue(SurveyorUserId,QueueList,QueueconfigList,PropSurveyList);
        try{
            update QueueList;
            update PropSurveyList;
            update QueueconfigList;
        }catch(exception e){
            throw(e);
        }
        return page;
    }
}