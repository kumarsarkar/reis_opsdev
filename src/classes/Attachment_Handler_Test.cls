/*
	@Name: Attachment_Handler_Test
	@Description: Test class for the Attachment_Handler controller
	

2014-05-05	Ross Field	Initial

*/ 

@isTest
private class Attachment_Handler_Test {
	
	static testMethod void AttachmentTest() {
		
		Account tempAcc = new Account(Name = 'Test Account');
		insert tempAcc;
		
		PageReference pageRef = Page.attachment;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('AccountId',tempAcc.id);
        Attachment_Handler attachHandler = new Attachment_Handler();
        attachHandler.isUnitTest=true;
        attachHandler.action();
        system.assertNotEquals(attachhandler.customattachment.Id, null);
        system.assertNotEquals(attachhandler.customattachment.Name, null);  // Auto Number Field
        attachhandler.upload();
        attachHandler.attachment.Body=Blob.valueOf('This is a sample Text File\nThis is the Second Line');
        attachhandler.upload();
        attachhandler.attachment.Description='Sample File.Txt';
        attachhandler.attachment.Name='Sample File.Txt';

        attachhandler.upload();
        update attachhandler.customattachment;
        
        ApexPages.currentPage().getParameters().put('Id',attachhandler.customattachment.Id);
        attachHandler.isUnitTest=true;
        attachhandler.download();  
        
        attachhandler.cancel();
 	}
  
}