/*
    @Name: Reis_SetTimezoneActivity_Handler
    @Description: On inserting or updating an activity, we will set the Activity Timezone by reading first the Contact's timezone formula, and then the Accounts.
    @Dependancies: 
    @Version: 1.0.0 
    
    ===VERSION HISTORY === 
    | Version Number | Author      | Description
    | 1.0.0          | Barney Holmes |  Initial
*/

public with sharing class Reis_SetTimezoneActivity_Handler {
    
    
    /*** Methods for handling events **/
    public static void processEvents (List<Event> eventList) {
        List<Event> meaningfulEvents = new List<Event>();


        set<Id> contactIds = new  set<Id>();
        set<Id> accountIds = new  set<Id>();
        String contact_prefix = Schema.SObjectType.Contact.getKeyPrefix();
        String account_prefix = Schema.SObjectType.Account.getKeyPrefix();
        

        //Loop through tasks and if they are complete and marked as meaningul add to new list
        for(Event e: eventList){
    
            /* First check if the contact[s] have a timezone, then select the first non null or empty value */
            if(e.whoid != null){
                if(string.valueof(e.whoid).startsWith(contact_prefix)){
                    contactIds.add(e.WhoId);
                    list<Contact> contactsTimeZone = new list<Contact>([Select Id, timezone__c FROM Contact Where Id in : contactIds]);

                    //e.timezone__c = string.valueof(contactsTimeZone.size());
    
                    //This will cycyle through all the contacts
                    for(Contact c: contactsTimeZone){
                        /*Iterate each associated contact to get their timezone*/
                        if (c.timezone__c != null || c.timezone__c != ''){
                            e.timezone__c = c.timezone__c;
                            return;
                        }
                    }
                }
            }
    
            /* If the code reached this point, then lets see if a WhatId / Account has a timezone. */
            if(e.whatid != null){
                if(string.valueof(e.whatid).startsWith(account_prefix)){
                    accountIds.add(e.WhatId);
    
                    list<Account> accountTimeZones = new list<Account>([Select Id, timezone__c FROM Account Where Id in : accountIds]);
                    
                    //e.timezone__c = string.valueof(contactsTimeZone.size());
            
                    //This will cycyle through all the contacts
                    for(Account a: accountTimeZones){
                        /*Iterate each associated contact to get their timezone*/
                        if (a.timezone__c != null || a.timezone__c != ''){
                            e.timezone__c = a.timezone__c;
                            return;
                        }
                    }
                }
            }    
    
        }
    }
   
    
    /*** Methods for handling tasks **/
    public static void processTasks (List<Task> taskList){ 
        List<Task> meaningfulTasks = new List<Task>();
        set<Id> contactIds = new  set<Id>();
        set<Id> accountIds = new  set<Id>();
        String contact_prefix = Schema.SObjectType.Contact.getKeyPrefix();
        String account_prefix = Schema.SObjectType.Account.getKeyPrefix();
        
    
        for(Task t: taskList){
    
            /* First check if the contact[s] have a timezone, then select the first non null or empty value */
            if(t.whoid != null){
                if(string.valueof(t.whoid).startsWith(contact_prefix)){
                    contactIds.add(t.WhoId);
                    list<Contact> contactsTimeZone = new list<Contact>([Select Id, timezone__c FROM Contact Where Id in : contactIds]);
                    //t.timezone__c = string.valueof(contactsTimeZone.size());
    
                    //This will cycyle through all the contacts
                    for(Contact c: contactsTimeZone){
                        /*Iterate each associated contact to get their timezone*/
                        if (c.timezone__c != null || c.timezone__c != ''){
                            t.timezone__c = c.timezone__c;
                            return;
                        }
                    }
                }
            }
    
            /* If the code reached this point, then lets see if a WhatId / Account has a timezone. */
            if(t.whatid != null){
                if(string.valueof(t.whatid).startsWith(account_prefix)){
                    accountIds.add(t.WhatId);
    
                    list<Account> accountTimeZones = new list<Account>([Select Id, timezone__c FROM Account Where Id in : accountIds]);
                    
                    //t.timezone__c = string.valueof(contactsTimeZone.size());
            
                    //This will cycyle through all the contacts
                    for(Account a: accountTimeZones){
                        /*Iterate each associated contact to get their timezone*/
                        if (a.timezone__c != null || a.timezone__c != ''){
                            t.timezone__c = a.timezone__c;
                            return;
                        }
                    }
                }
            }    
    
        }
    }
    

}