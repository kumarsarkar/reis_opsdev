public class STHController {
	public id recordId;
	public Property_Survey__c tempRecord {get;set;}
	public string saveResults {get;set;}
	public List<contactWrapper> contactList {get;set;}
	public static id staticRecordId{get;set;}
	public Map<String, List<amenitiesWrapper>> amenitiesList {get;set;}
	public List<Schema.FieldSetMember> secondFloorPlan {get;set;}
	public Survey_Amenities__c newAm {get;set;}

	//custom setter and getter to ensure when we save all the records are properly updated
	public List<sstWrapper> floorplans {
		get{
			return floorplans;
		}
		set{
			floorplans = value;
		}}

	//Methods to get the fields from the fieldsets
	public List<Schema.FieldSetMember> getSurveyContactFields(){
		return SObjectType.Survey_Contact__c.FieldSets.SurveyContactFields.getFields();
	}
	public List<Schema.FieldSetMember> getReisContactFields(){
		return sObjectType.Reis_Contact__c.FieldSets.property_survey_field_set.getFields();
	}

	public List<Schema.FieldSetMember> getLocationFields(){
		return SObjectType.Property_Survey__c.FieldSets.PropertyLocation.getFields();
	}

	public List<Schema.FieldSetMember> getInformationFields(){
		return SObjectType.Property_Survey__c.FieldSets.PropertyInformation.getFields();
	}

	public List<Schema.FieldSetMember> getFloorpanHeaderFields(){
		Return SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurveyHeader.getFields();
	}

    //**** Anantha : Added for Student Housing
    public List<Schema.FieldSetMember> getSTHFloorpanHeaderFields(){
		Return SObjectType.Survey_Space_Type__c.FieldSets.StudentHousingSurveyHeader.getFields();
	}


    public List<sstWrapper> getSTHFloorPlanMap(){
		List<sstWrapper> returnMap = new List<sstWrapper>();
		String queryString = 'Select ';
		for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.StudentHousingSurveyFloorPlanFields.getFields()){
			QueryString += f.getFieldPath()+', ';
		}
        for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.Student_Housing_Free_Rent.getFields()){
			QueryString += f.getFieldPath()+', ';
		}

        //Anantha : Added the 	Student_Housing_Space_Type__c to the query string for STH Sector.
		QueryString += 'Id,Name,Delete_By_Foundation__c,Current_Year__c,Future_Year__c,Current_Month__c,Future_Month__c,Apt_Space_Type__c, Student_Housing_Space_Type__c,Reis_Space_Type__c,Reis_Space_Type__r.Bedrooms__c From Survey_Space_Type__c  Where Property_Survey__c = \''+recordId+'\' ORDER BY Reis_Space_Type__r.Sort_Order__c ASC';
		for(Survey_Space_Type__c sst :Database.query(QueryString)){
				returnMap.add(new sstWrapper(sst));
		}
		return returnMap;
	}

    public List<fieldWrapper> getSTHFreeRentPlanFields(){
		List<fieldWrapper> returnList = new List<fieldWrapper>();
		List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
		tempList = SObjectType.Survey_Space_Type__c.FieldSets.Student_Housing_Free_Rent.getFields();
		for(Schema.FieldSetMember fsm :tempList){
			fieldWrapper fw = new fieldWrapper();
			fw.afield = fsm;
			returnList.add(fw);
        }
		return returnList;
	}

    public List<fieldWrapper> getSTHFloorPlanFields(){
		List<fieldWrapper> returnList = new List<fieldWrapper>();
		List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
		tempList = SObjectType.Survey_Space_Type__c.FieldSets.StudentHousingSurveyFloorPlanFields.getFields();
		Double counter = 0;
		Double total = tempList.size();
		for(Schema.FieldSetMember fsm :tempList){
			fieldWrapper fw = new fieldWrapper();
			if(counter/total < .5){
				fw.afield = fsm;
				fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
			}
			else{
				break;
			}
			counter++;
			returnList.add(fw);
		}
		return returnList;
	}

    public void STHSave(){
		//specific save logic for the apartments record type
		List<Survey_Space_Type__c> insertList = new List<Survey_Space_Type__c>();
		for(sstWrapper s :floorplans){
			system.debug(s);
			s.sst.Reis_Space_Type__c = s.sst.Student_Housing_Space_Type__c;
			//for(Survey_Space_Type__c sst :floorplans.get(s)){
				//system.debug(sst);
				insertList.add(s.sst);
			//}
		}
			upsert insertList;
	}

    public void addNewSTHAmmenity(){
		//inserts our new amenity and updates our pages viewing list
		List<amenitiesWrapper> fullList = new list<amenitiesWrapper>();
		For(List<amenitiesWrapper> a :amenitiesList.values()){
			fulllist.addAll(a);
		}

		newAm.Property_Survey__c = recordId;
		newAm.Amenity__c = newAm.Student_Housing_Amenity__c;
		Amenity__c am = [select name, Type__c from Amenity__c where id = :newAm.Student_Housing_Amenity__c limit 1];
		for(amenitiesWrapper a :fulllist){
			if(!a.sa.Delete_By_Foundation__c && a.type == am.type__c && am.name.contains('No')){
				return;
			}
			else if(a.type == am.Type__c && a.sa.Amenity__r.Name.contains('No')){
				return;
			}
		}
		insert newAm;
		generateAmenityList();
	}

// **** End of Addition
	public List<fieldWrapper> getFloorPlanFields(){
		List<fieldWrapper> returnList = new List<fieldWrapper>();
		List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
		tempList = SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurvey.getFields();
		Double counter = 0;
		Double total = tempList.size();
		for(Schema.FieldSetMember fsm :tempList){
			fieldWrapper fw = new fieldWrapper();
			if(counter/total < .5){
				fw.afield = fsm;
				fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
			}
			else{
				break;
			}
			counter++;
			returnList.add(fw);
		}
		return returnList;
	}

	//fieldWrapper is used with field sets for the survey space type records related list, allows us to set editable and readonly fields seperatly
	public class fieldWrapper{
		public Schema.FieldSetMember afield {get;set;}
		public Schema.FieldSetMember bfield {get;set;}

		public fieldWrapper(){
			//this.afield = new Schema.FieldSetMember();
			//this.bfield = new Schema.FieldSetMember();
		}
	}
	//getFields method only returns to us the fields of the property survey record
	public List<Schema.FieldSetMember> getFields(){
		List<Schema.FieldSetMember> returnList = new List<Schema.FieldSetMember>();
		returnList.addAll(getLocationFields());
		returnList.addAll(getInformationFields());
		return returnList;
	}
	//gets us the survey space type records we need to use in the related list, also uses field sets to get data
	//we can expand this to get different fieldsets based on record type
	public List<sstWrapper> getFloorPlanMap(){
        tempRecord = getTheRecord();
		List<sstWrapper> returnMap = new List<sstWrapper>();
		String queryString = 'Select ';
		for(Schema.FieldSetMember f: SObjectType.Survey_Space_Type__c.FieldSets.FloorPlanSurvey.getFields()){
			QueryString += f.getFieldPath()+', ';
		}
		QueryString += 'Id, Name,Delete_By_Foundation__c,Apt_Space_Type__c, Reis_Space_Type__c, Reis_Space_Type__r.Bedrooms__c From Survey_Space_Type__c Where Property_Survey__c = \''+recordId+'\' ORDER BY Reis_Space_Type__r.Sort_Order__c ASC';
        for(Survey_Space_Type__c sst :Database.query(QueryString)){
				returnMap.add(new sstWrapper(sst));
		}
		return returnMap;
	}

    //uses custom contact wrapper, we get all survey contacts and reis contacts and put them into the wrapper
	// inoder for us to end the information we need to update the reis contact directly, there are also fields on the survey contact that need to be updated at times as well
	// this helps with that.

   public list<contactWrapper> oldCons{get;set;}
	public List<contactWrapper> getContacts(){
		list<contactWrapper> returnList = new list<contactWrapper>();
		oldCons = new list<contactWrapper>();
		map<String,ContactWrapper> emailMap = new map<String,ContactWrapper>();
		List<id> idList = new List<id>();
		String queryString = 'Select ';
		For(Schema.FieldSetMember f: This.getSurveyContactFields()){
			QueryString += f.getFieldPath()+', ';
		}
		QueryString += 'Id, Contact__c,Contact__r.oid__c From Survey_Contact__c Where Property_Survey__c = :recordId ORDER BY Most_Recent_Contact__c DESC';
		for(Survey_Contact__c s :Database.query(QueryString)){
			if(emailMap.get(s.Contact__r.oid__c) == null){
				emailMap.put(s.Contact__r.oid__c, new contactWrapper(s));
				idList.add(s.contact__c);
			}
		}
		String newQueryString = 'Select ';
		for(Schema.FieldSetMember f :sObjectType.Reis_Contact__c.FieldSets.property_survey_field_set.getFields()){
			newQueryString += f.getFieldPath()+', ';
		}
		newQueryString += 'Id, Name, first_name__c,oid__c, last_name__c from Reis_Contact__c where id in :idList';
		for(Reis_Contact__c r:Database.query(newQueryString)){
			if(emailMap.get(r.oid__c) != null){
				emailMap.get(r.oid__c).rCon = r;
			}
		}
		oldCons = emailMap.values();
		Return emailMap.values();
		//Return Database.query(QueryString);
	}

	public Property_Survey__c getTheRecord(){
		//gets us the fields we need to use for the viewing survey contact record, we use a field set, and some fields we think will always be on
		String QueryString = 'Select Id, ';
		For(Schema.FieldSetMember f: This.getFields()){
			QueryString += f.getFieldPath()+', ';
		}
        for(Schema.FieldSetMember f: SObjectType.Property_Survey__c.FieldSets.StudentHousingFields.getFields()){
			QueryString += f.getFieldPath()+', ';
		}
        //Anantha Updated the below fields for STH
        QueryString+='Reduced_Free_Rent_Pct__c,Months_Free_Rent__c,Free_Rent_Lease_Term__c,Free_Rent_Pct__c,Base_Rent__c,Reduced_Lease_Term__c,Discount__c,Pre_Leased_New__c,Rent_Type__c,Total_Available_Beds_Current__c,Total_Available_New__c,Comp_Totals_by_Bed__c,Comp_Totals_by_Unit__c,Pre_Leased__c,Contact_Multiple__c,Non_Comp_Totals_by_Bed__c,Sth_MR_Beds_New__c,Sth_MR_Beds_Current__c,SR_Housing_MR_Units_New__c,SR_Housing_MR_Units_Current__c,Total_Available_Beds_New__c,Non_Comp_Totals_by_Unit__c,Free_Rent_BR_Type_s__c,Survey_Sector__c,';
		QueryString += 'Name,Unit_Mix_Estimated__c,Select_Code_New__c,Rent_ref__c,Last_Surveyed__c,Call_Status__c,Status__c,Rest_neg__c,Vac_Ref__c,Ref_Rest__c,DK_rest__c, Current_Available__c,Free_Rent_Text__c,Available_2_4_weeks__c,Total_Available_Units__c, Total_NC_Units__c, Facility_Name__c,Select_Code__c , RecordType.Name, Total_Units__c From Property_Survey__c Where ID = \''+recordId+'\' LIMIT 1';
        Return Database.query(QueryString);
	}

	public void saveRecord(){
		try{
			updateAmenities();
			insertContacts();

			if(tempRecord.RecordType.name == 'Apartments'){
				apartmentSave();
			}
            //Anantha : Added for Student Housing
            if(tempRecord.RecordType.name == 'Student Housing'){
				STHSave();
			}
			update tempRecord;
		}
		catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
      ApexPages.addMessage(myMsg);
		}
	}

    public List<fieldWrapper> getSTHPropSurveyFields(){
		List<fieldWrapper> returnList = new List<fieldWrapper>();
		List<Schema.FieldSetMember> tempList = new List<Schema.FieldSetMember>();
		tempList = SObjectType.Property_Survey__c.FieldSets.StudentHousingFields.getFields();
		Double counter = 0;
		Double total = tempList.size();
		for(Schema.FieldSetMember fsm :tempList){
			fieldWrapper fw = new fieldWrapper();
			if(counter/total < .5){
				fw.afield = fsm;
				fw.bfield = tempList[Integer.valueOf(counter + (total/2))];
			}
			else{
				break;
			}
			counter++;
			returnList.add(fw);
		}
		return returnList;
	}

	public void newSST(){
        try{
					system.debug('in the new sst');
		//adds a new survey space type to our wrapper/related list
		if(floorplans == null){
			floorplans = new List<sstWrapper>();
		}
		floorplans.add(new sstWrapper( new Survey_Space_Type__c(Property_Survey__c = recordid)));
		system.debug(floorplans.size());
        }
        catch(exception e){
					system.debug('doing a thing');
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
      ApexPages.addMessage(myMsg);
		}
	}

	public void apartmentSave(){
		//spesific save logic for the apartments record type
		List<Survey_Space_Type__c> insertList = new List<Survey_Space_Type__c>();
		for(sstWrapper s :floorplans){
			system.debug(s);
			s.sst.Reis_Space_Type__c = s.sst.Apt_Space_Type__c;
			//for(Survey_Space_Type__c sst :floorplans.get(s)){
				//system.debug(sst);
				insertList.add(s.sst);
			//}
		}
			upsert insertList;
	}


		public void AddToConList(){
		//adds a new contact to the contact wrapper / related list
		contactList = contactWrapper.addNewCon(contactList);
	}

	public Boolean insertContacts(){
		//save method for the contacts, there was no indication that we had to check for items to change
		//so we just update all the records with the values we have
		List<Survey_Contact__c> insertList = new List<Survey_Contact__c>();
		List<Survey_Contact__c> insertList2 = new List<Survey_Contact__c>();
		List<Survey_Contact__c> updateSCList = new List<Survey_Contact__c>();
		List<Reis_Contact__c> updateRCList = new List<Reis_Contact__c>();
		List<Reis_Contact__c> newContactList = new List<Reis_Contact__c>();
		for(contactWrapper c :contactList){
			system.debug(c);
			if(c.newcon && c.con.Contact__c == null){
				Reis_Contact__c r = c.rCon;
				r.name = r.first_name__c+' '+r.last_name__c;
				newContactList.add(r);
				insertList.add(c.con);
				c.newCon = false;
			}
			else if(c.newcon && c.con.Contact__c != null){
				c.con.Property_Survey__c = recordId;
				insertList2.add(c.con);
				c.newCon = false;
			}
			else{
				if(c.con.id != null && c.rcon.id != null){
					updateSCList.add(c.con);
					updateRCList.add(c.rcon);
				}
			}
		}

		if(insertList2.size() > 0){
			try{
				insert insertList2;
				system.debug(insertList2);
			}
			catch(Exception e){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+ 'At Line: '+e.getLineNumber());
				ApexPages.addMessage(myMsg);
				return FALSE;
			}
		}

		if(insertList.size() > 0 && newContactList.size() > 0){
			try{
				insert newContactList;
				Integer count = 0;
				for(Survey_Contact__c c :insertList){
						c.Property_Survey__c = recordId;
						c.Contact__c = newContactList[count].id;
						count++;
				}
				insert insertList;

			}
			catch(Exception e){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+ 'At Line: '+e.getLineNumber());
	      ApexPages.addMessage(myMsg);
				return FALSE;
			}
		}
		if(updateSCList.size() > 0 && updateRCList.size() > 0){
			try{
				update updateSCList;
				update updateRCList;
			}
			catch(Exception e){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+ 'At Line: '+e.getLineNumber());
	      ApexPages.addMessage(myMsg);
			}
		}
		getContacts();
		return TRUE;
	}

	public PageReference sumbitForApproval(){
		//baisc submit for approval
	try{
		Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
		req.setComments('Submitting request for approval'); // You can make comments dynamic
		req.setObjectId(RecordId);
		Approval.ProcessResult result = Approval.process(req);
	}
	catch (Exception ex){
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
		ApexPages.addMessage(myMsg);
	}
	//return submitPageRef;
	return null;
}

	public STHController(ApexPages.StandardController controller) {
		//constructor
		system.debug('new run of the controller');
		recordId = controller.getId();
		staticRecordId = controller.getId();
		tempRecord = getTheRecord();
		contactList = getContacts();
		generateAmenityList();
		if(tempRecord.recordtype.name == 'Apartments'){
			floorplans = getFloorPlanMap();
		}
        //Anantha: Added for Student Housing
        if(tempRecord.recordtype.name == 'Student Housing'){
			floorplans = getSTHFloorPlanMap();
		}
	}

	public class sstWrapper{
		//survey space tpye wrapper so we can create new survey space type records in our related list
		public Survey_Space_Type__c sst {Get;set;}
		public boolean newSst {get;set;}

		public sstWrapper(){
			this.sst = new Survey_Space_Type__c();
			this.newSst =false;
		}

		public sstWrapper(Survey_Space_Type__c s){
			this.sst = s;
			this.newSst =false;
		}
	}

	@remoteAction
	public static void logACall(String a, String rid){
		//log a call remote action for use on the VF Page
		//deprecated
		task t = new task();
		t.whatId = rid;
		t.subject = 'Call';
		t.subject =a;
		t.status = 'Completed';
		t.description = 'Call at: '+system.now();
		t.activityDate = system.today();
		insert t;
		system.debug(t.id);
	}

	@remoteAction
	public static void remoteSave(Boolean b, String rid){
		//used in out reocrd type change validation
		Property_Survey__c a = [select id, needs_qa_review__c from Property_Survey__c where id = :rid limit 1];
		a.needs_qa_review__c = b;
		update a;
	}

	@remoteAction
	public static void remoteAddComment(string newComment, String rid){
		//used on all most all of our validations
		Property_Survey__c a = [select id, needs_qa_review__c, Property_Notes_New__c, QC_Notes_New__c from Property_Survey__c where id = :rid limit 1];
		//a.QC_Notes_New__c += ' ('+system.now()+') '+newComment;
		if(a.Property_Notes_New__c == null){
			a.Property_Notes_New__c = ' ('+system.now()+') '+newComment;
		}
		else{
			a.Property_Notes_New__c += ' ('+system.now()+') '+newComment;
		}

		update a;
	}

	@remoteAction
	public static Boolean remoteSiteCheck(String toCheck, Boolean propertySite){
		//used on site fields to determine if they are on our restricted list, if they are, we blank out on the client side
		system.debug(propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()));
		system.debug(toCheck);
		if(propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()) != null){
			if(propertySite && propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()).property_site__c){
				return true;
			}
			else if(!propertySite && propertyWebsiteValidation__c.getInstance(toCheck.toLowerCase()).Survey_Site__c){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	public class amenitiesWrapper{
		//amenitiesWrapper is used to add new records to our related lists
		// also contains the type field so we can determine which list to populate with which record
		public Survey_Amenities__c sa {get;set;}
		public String type {get;set;}

		public amenitiesWrapper(){
			this.sa = new Survey_Amenities__c();
			this.type = '';
		}

		public amenitiesWrapper(Survey_Amenities__c s, String t){
			this.sa = s;
			this.type = t;
		}
	}


	public void generateAmenityList(){
		//populate our amenity wrapper list
		//set our new amenity to a blank amenity so we can create new ones quickly
		newAm = new Survey_Amenities__c();
		amenitiesList = new Map<String,List<amenitiesWrapper>>();
		amenitiesList.put('Complex', new List<amenitiesWrapper>());
		amenitiesList.put('Unit', new List<amenitiesWrapper>());
		amenitiesList.put('Utilities', new List<amenitiesWrapper>());
		for(Survey_Amenities__c s :[select id,Amenity__r.Has_Quantity__c, Delete_By_Foundation__c,Apartment_Amenity__c,Quantity_New__c, Amenity__r.Name, Amenity__r.Type__c,Student_Housing_Amenity__c from Survey_Amenities__c where Property_Survey__c = :recordId AND Amenity__c != null]){
			if(amenitiesList.get(s.Amenity__r.Type__c) == null){
				amenitiesList.put( s.Amenity__r.Type__c,new List<amenitiesWrapper>());
				amenitiesList.get(s.Amenity__r.Type__c).add(new amenitiesWrapper(s, s.Amenity__r.Type__c));
			}else{
				amenitiesList.get(s.Amenity__r.Type__c).add(new amenitiesWrapper(s, s.Amenity__r.Type__c));
			}
		}
	}



	public void addNewAmmenity(){
		//inserts our new amenity and updates our pages viewing list
		List<amenitiesWrapper> fullList = new list<amenitiesWrapper>();
		For(List<amenitiesWrapper> a :amenitiesList.values()){
			fulllist.addAll(a);
		}

		newAm.Property_Survey__c = recordId;
		newAm.Amenity__c = newAm.Apartment_Amenity__c;
		Amenity__c am = [select name, Type__c from Amenity__c where id = :newAm.Apartment_Amenity__c limit 1];
		for(amenitiesWrapper a :fulllist){
			if(!a.sa.Delete_By_Foundation__c && a.type == am.type__c && am.name.contains('No')){
				return;
			}
			else if(a.type == am.Type__c && a.sa.Amenity__r.Name.contains('No')){
				return;
			}
		}
		insert newAm;
		generateAmenityList();
	}

	public void updateAmenities(){
		//updates any changes we made to existing amenities
		List<Survey_Amenities__c> updateList = new List<Survey_Amenities__c>();
		List<amenitiesWrapper> fullList = new list<amenitiesWrapper>();
		For(List<amenitiesWrapper> a :amenitiesList.values()){
			fulllist.addAll(a);
		}
		for(amenitiesWrapper aw :fullList){
            //Anantha : Added for Student Housing.
            if(tempRecord.RecordType.name == 'Student Housing'){
				aw.sa.Amenity__c = aw.sa.Student_Housing_Amenity__c;
            }else{
            	aw.sa.Amenity__c = aw.sa.Apartment_Amenity__c;
            }

			updateList.add(aw.sa);
		}

		try{
			update updateList;
		}
		catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
      ApexPages.addMessage(myMsg);
		}
	}

//depricated
	public String removalId {get;set;}
	public void removeAmenity(){
		system.debug(removalId);
		Survey_Amenities__c ps = [select id from Survey_Amenities__c where id = :removalId limit 1];
		delete ps;
		generateAmenityList();
	}
}