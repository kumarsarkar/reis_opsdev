/*
	@Name: Shift_NEFEmail_Test
	@Description: Unit test for the NEf Email component and preview page
	@Dependancies: 
	@Version: 1.0.0

	===VERSION HISTORY ===
	| Version Number | Author      | Description
	| 1.0.0          | Ryan Morden |  Initial
*/

@isTest
public class Shift_NEFEmail_Test {
	
	@isTest static void testGetPrimaryCon() {
		Account tempAcc = new Account(Name='Test');
		Insert tempAcc;

		Opportunity tempOpp = new Opportunity(
			AccountId=TempAcc.Id,
			Name='Test Opp',
			StageName='Closed',
			CloseDate=Date.Today()
		);
		insert tempOpp;

		Contact tempCon = new Contact(
			AccountId=tempAcc.Id,
			LastName='testLNAME'
		);
		insert tempCon;

		OpportunityContactRole ocr = new OpportunityContactRole(
			OpportunityId = tempOpp.Id,
			ContactId = tempCon.Id,
			isPrimary = true
		);
		insert ocr;

		Shift_NEFEmailTemplate_Ext controller = new Shift_NEFEmailTemplate_Ext();
		Controller.theOppId = tempOpp.Id;
		Contact tempContact = controller.getPrimaryContact();
		system.assertEquals(tempContact.Id, tempCon.Id);


		Shift_NEFEmailPreview_Ext controller2 = new Shift_NEFEmailPreview_Ext(new ApexPages.StandardController(tempOpp));
		controller2.triggerEmailAlert();
		controller2.theCancel();


	}
}