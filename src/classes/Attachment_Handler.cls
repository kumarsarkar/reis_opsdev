/*
    @Name: Attachment_Handler_Test
    
2014-05-05  Ross Field  Initial
2014-05-29  Ross Field  Added filename=EncodingUtil.urlEncode(filename, 'UTF-8');
2014-08-28  Raul - Comment Out File Post routine.
*/



global with sharing class Attachment_Handler {

    public String API_Partner_Server_URL_80 { get; set; }

    public String API_Session_ID { get; set; }

  public Attachment attachment {get;set;}
  public Boolean errorflag   {get;set;}
  public String httpResult {get;set;}
  public String endPoint {get;set;}
  public Attachment__c customAttachment {get;set;}  // only public for Test Class
  public Boolean isUnitTest {get;set;}
        
//Constructor
  public Attachment_Handler() 
  {
    attachment = new Attachment();
    //DML Cannot be performed in a Constructor - see action();
    return;
  }

    @RemoteAction
    global static string getToken() {
        Blob key = Blob.valueOf('6179B71481E79186');
        Blob Iv = Blob.valueOf('AF7727F6381D48B4');
        
        Datetime dt = Datetime.now();
        Long l = dt.getTime();
        String strDate = l.format().remove(',');
        strDate = strDate.substring(0, strDate.length() -3);
        Blob ep = Blob.valueOf(strDate);
       
	    Blob cipherText = Crypto.encrypt('AES128', key, Iv, ep);
	   	String encodedCipherText = EncodingUtil.base64Encode(cipherText);

		//return dt.format() + '-' + l.format() + '-' + encodedCipherText;
        return encodedCipherText;
    }       
     
  
  public PageReference action() {
        if (isUnitTest==null) 
            isUnitTest=false;
        
        if (ApexPages.currentPage().getParameters().get('Id')!=null) 
            return download();
        
        customAttachment = new Attachment__c();
        insert customAttachment;
        customAttachment = [SELECT Id,Name from Attachment__c where Id=:customAttachment.Id];
        return null;        
  }
   
  public PageReference cancel() {
    try
    {
        delete customAttachment;
    }
    catch (exception E)
    {
    }
    return returnpage();
  }
    
  private Pagereference returnpage(){
    
        String Id;
            
        Id = ApexPages.currentPage().getParameters().get('OpportunityId');
        if (Id !='')
            return new PageReference('/'+Id);
        
        Id = ApexPages.currentPage().getParameters().get('ContactId');
        if (Id !='')
            return new PageReference('/'+Id);
        
        Id=ApexPages.currentPage().getParameters().get('AccountId');
            return new PageReference('/'+Id);
        
  }
    
    
  
  public PageReference upload() {
  
//    if (attachment.Body==null || attachment.Name=='')
//    {
//        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please choose file to Upload'));
//        return null;
//    }
    
//    if (customAttachment.Name==null || customAttachment.Name=='')
//        {
//            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Unique Field is null'));
//            return null ;
//      }
  
    try 
    {
        customAttachment.File_Name__c=attachment.Name;
        if (attachment.Description!=null && attachment.Description!='')
            customAttachment.Description__c=attachment.Description;
        else
            customAttachment.Description__c=attachment.Name;
            
        
        if (ApexPages.currentPage().getParameters().get('AccountId')!='')
            customattachment.Account__c=ApexPages.currentPage().getParameters().get('AccountId');
        
        if (ApexPages.currentPage().getParameters().get('OpportunityId')!='')
            customattachment.Opportunity__c=ApexPages.currentPage().getParameters().get('OpportunityId');
        
        if (ApexPages.currentPage().getParameters().get('ContactId')!='')
            customattachment.Contact__c=ApexPages.currentPage().getParameters().get('ContactId');
        

//        httpResult='';
//        HTTPResponse  res = uploadFile(attachment);
//        if (res.getStatusCode()!=200 || !res.getBody().contains('success'))
//        {
//                // Very Important to clear the attachment due to SFDC viewstate size restrictions!
//                attachment.Name=null;
//                attachment.Body=null;
//                httpResult=res.getBody();
//                errorflag=true;
//                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to Upload File'));
//                return null;
//        }

        update customAttachment;
        
        
        // Very Important to clear the attachment due to SFDC viewstate size restrictions!
        attachment.Name=null;
        attachment.Body=null;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        
        return returnPage();
    }
    catch (exception E) 
    {
            attachment.Name=null;
            attachment.Body=null;
            errorflag=true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to Upload File:' +e.getMessage() +'/' + e.getLineNumber()));
            return null;
    }
  }
  
  
    public HTTPResponse uploadFile(Attachment file){
      String boundary = '__boundary__xxx';
      String header = '--'+boundary+'\n';
      String Prefix=customAttachment.Name;
      header += 'Content-Disposition: form-data; name="theFile"; filename="'+Prefix+file.name
        +'"\r\nContent-Type: application/octet-stream\r\n';
     
      String footer = '\r\n--'+boundary+'--';
       
      // no trailing padding on header by adding ' ' before the last "\n\n" characters
      String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
      //this ensures no trailing "=" padding
      while(headerEncoded.endsWith('='))
      {
       header+=' ';
       headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
      }
      //base64 encoded body
      String bodyEncoded = EncodingUtil.base64Encode(file.body);
      //base64 encoded footer
      String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
       
      Blob bodyBlob = null;
      //last encoded body bytes
      String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
      //if the last 4 bytes encoded base64 ends with the padding character (= or ==) then re-encode those bytes with the footer
      //to ensure the padding is added only at the end of the body
      if(last4Bytes.endsWith('='))
      {
       Blob decoded4Bytes = EncodingUtil.base64Decode(last4Bytes);
       HttpRequest tmp = new HttpRequest();
       tmp.setBodyAsBlob(decoded4Bytes);
       String last4BytesFooter = tmp.getBody()+footer;   
       bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded.substring(0,bodyEncoded.length()-4)+EncodingUtil.base64Encode(Blob.valueOf(last4BytesFooter)));
      }
      else
      {
       bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
      }
      HttpRequest req = new HttpRequest();
      req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
      req.setMethod('POST');
      endpoint= 'http://207.99.64.123/SimplisticFTP/upload';
      req.setEndpoint(Endpoint);
         
      req.setBodyAsBlob(bodyBlob);
      req.setTimeout(60000);
      req.setHeader('Content-Length',String.valueof(req.getBodyAsBlob().size()));
      Http http = new Http();
      HTTPResponse res;
      
      if (!isUnitTest) 
        res = http.send(req);
      
      return res;   
    }
    


    


  public PageReference download() {
    
   
    Attachment__c customAttachment = [ SELECT 
                                            Id,
                                            Name,
                                            File_Name__c
                                            FROM Attachment__c 
                                            WHERE Id=:ApexPages.currentPage().getParameters().get('Id') LIMIT 1];
    
    
    string fileName;
    
    if (customAttachment.Name.left(1)=='Q') // SLX File
        fileName=customAttachment.File_Name__c;
    else
        filename=customAttachment.File_Name__c;
        
        
        //filename=customAttachment.Name+customAttachment.File_Name__c;
    
    filename=EncodingUtil.urlEncode(filename, 'UTF-8');
    HttpRequest r = new HttpRequest();
    r.setMethod('GET'); 
    Endpoint='http://207.99.64.123/SimplisticFTP/download?fileName='+fileName;
    r.setEndpoint(EndPoint);
    
    
    Http http = new Http();
    HttpResponse res = new HttpResponse();
    
    if (!isUnitTest)
        res = http.send(r);
    else
        res.setStatusCode(200); 
    
    errorflag=false;
    httpResult='';
    
    if (res.getStatusCode()!=200)
    {
        httpResult=res.getBody();
        errorflag=true;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to  Download File'));
        return null;
    }
    blob image = res.getBodyAsBlob();
    
    Document d;
    try 
    {
        d = [SELECT Id 
                FROM Document 
                WHERE folderId = :UserInfo.getUserId() AND
                DeveloperName= :'TemporaryWorkingDownloadFile2'+UserInfo.getUserId() 
                LIMIT 1
            ];
        }
    catch (exception E)
    {
        d=null; 
    }
    if (d==null)    
    {
        d = new Document();
        d.folderId = UserInfo.getUserId();
        d.DeveloperName= 'TemporaryWorkingDownloadFile2'+UserInfo.getUserId();
    }   
    
    d.name = customAttachment.File_Name__c;
    d.body = image;
    d.Name = customAttachment.File_Name__c;
    d.ContentType='';
    d.type='';
    
    if (!isUnitTest)
        upsert d;
        
    return new Pagereference('/servlet/servlet.FileDownload?file='+d.Id);
      

  }
  

}