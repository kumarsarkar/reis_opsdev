/*
    @Name: Shift_NEFEmailTemplate_Ext
    @Description: Extension for NEF Email component
    @Dependancies: 
    @Version: 1.0.0

    ===VERSION HISTORY ===
    | Version Number | Author      | Description
    | 1.0.0          | Ryan Morden |  Initial
*/


public class Shift_NEFEmailTemplate_Ext {

    //public Contact primaryContact {get; set;}
    public String theOppId {get;set;}
    public List<OpportunityContactRole> oppContactRole {get;set;}
    public String OpportunityIdStr {get;set;}

    public Shift_NEFEmailTemplate_Ext() {}

    public Opportunity getActiveOpportunity() {
 
        List<Opportunity> tempOppList = new List<Opportunity>([SELECT Id, Name, AccountId, OwnerId, Target_Price__c, Owner.Name, Contract_Number__c, Contract_Start_Date__c, Contract_End_Date__c, CloseDate, Account.SLXID__c,
                                            Account.BillingStreet, Account.Reis_Accounting_ID__c, Account.BillingCity, Account.BillingState, Account.BillingCountry, Account.BillingPostalCode, Account.Industry, Amount, 
                                            Sales_Comments__c, Client_Knows_Prop_Level_Criteria__c, Most_Critical_Reports__c, Reports_Not_Shown__c, Client_KNows_SPV_Limitation__c, Account.Account_ID__c, Account.Reis_Website_ID__c,
                                            Competition__c, Competitors_Beaten__c, Client_Uses__c, Account.Account_Type__c FROM Opportunity WHERE Id = :theOppId LIMIT 1]);

        Opportunity tempOpp = tempOppList[0];       
        return tempOpp;
    }

    public List<OpportunityLineItem> getOpportunityLineItems() {
        List<OpportunityLineItem> tempOLIList = new List<OpportunityLineItem>([SELECT Id, Product_Type__c, Product_Family__c, Capped_Amount__c, MSA__c, Reports__c, Sector__c, Region__c,
                                                                    Frequency__c, UnitPrice, ServiceDate, Site_End_Date__c, Notes__c, Analytic__c FROM OpportunityLineItem
                                                                    WHERE OpportunityId = :theOppId] );

        for(OpportunityLineItem oli : tempOLIList ){
           oli.Reports__c = FormatLists(oli.Reports__c );
           oli.MSA__c = FormatLists(oli.MSA__c);
           oli.Sector__c = FormatLists(oli.Sector__c);            
        }


        return tempOLIList;

    }

    public Contact getPrimaryContact() {
        //Find the primary contact if it exist
        oppContactRole = [SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId=:theOppId AND isPrimary=TRUE LIMIT 1];

        //If a primary contact is found, then return the contact details
        contact tempCon = new contact();
        if (!oppContactRole.isEmpty()) {
            tempCon = [SELECT Name, Phone, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode FROM Contact WHERE Id=:oppContactRole[0].ContactId];
        }

        return tempCon;
    }
    
    private string FormatLists(string inText)
    {
       if (inText == null){
           return '';
        }
       else {
           String[]  mylist = inText.split(';');         
           //inText = String.join(', ',mylist);
           mylist.sort();
           String result = String.join(mylist, ', ');
           return result.replaceAll(';',', ');
           }
    }
    
}