<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Leads_New_CS_lead_assignment_notification</fullName>
        <description>Leads: New CS lead assignment notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>matthew.burlenski@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mila.feldbaum@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>natasha.singh@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thomas.burkland@reis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsNewCSleadassignmentnotification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Lead_Generator_Id</fullName>
        <description>Capture Lead Generator Id</description>
        <field>Lead_Generator_Id__c</field>
        <formula>CASESAFEID(OwnerId)</formula>
        <name>Update Lead Generator Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Lead Generator Id</fullName>
        <actions>
            <name>Update_Lead_Generator_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow that will capture the Lead Generator Id when a lead is created</description>
        <formula>(ISNEW()  || ISCHANGED(OwnerId)) &amp;&amp; Owner:User.Profile.Name=&quot;Lead Generation&quot;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
