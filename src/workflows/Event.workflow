<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_Activity_Touch_Checkbox</fullName>
        <field>Meaningful_Activity__c</field>
        <literalValue>1</literalValue>
        <name>Check Meaningful Activity Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Event_Touched_Date</fullName>
        <field>Touched_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Event Touched Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Event Touched Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Event.Subtype__c</field>
            <operation>equals</operation>
            <value>Conversation,Received Call,Received Email</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Activity_Touch_Checkbox</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Event_Touched_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Event.EndDateTime</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
