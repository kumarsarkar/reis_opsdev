<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Case_Owner_to_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Case Owner to Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Share_Case</fullName>
        <field>Share_Case__c</field>
        <literalValue>1</literalValue>
        <name>Set Share Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case send to Queue</fullName>
        <actions>
            <name>Change_Case_Owner_to_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>Client Services</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Make Case Public</fullName>
        <actions>
            <name>Set_Share_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>When a case is set to closed, check the share case field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
