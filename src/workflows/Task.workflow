<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Meaningful_Touch_Checkbox</fullName>
        <field>Meaningful_Activity__c</field>
        <literalValue>1</literalValue>
        <name>Update Meaningful Touch Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Task_Touched_Date</fullName>
        <field>Touched_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Task Touched Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Task Touched Date</fullName>
        <actions>
            <name>Update_Meaningful_Touch_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Task_Touched_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Subtype__c</field>
            <operation>equals</operation>
            <value>Conversation,Received Call,Received Email</value>
        </criteriaItems>
        <description>When a task is completed and Meaningful task is checked, capture the Touched Date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
