<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Surveyor_Update_Surveyor_Name</fullName>
        <field>Name</field>
        <formula>User__r.FirstName +&apos; &apos;+User__r.LastName</formula>
        <name>Surveyor-Update Surveyor Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Surveyor-Update Surveyor Name</fullName>
        <actions>
            <name>Surveyor_Update_Surveyor_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is used to update the Name field as and when the User is assigned to the Surveyor.</description>
        <formula>NOT(ISBLANK( User__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
