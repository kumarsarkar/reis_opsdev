<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Closed_Lost_Oppty_15K</fullName>
        <description>Closed Lost Oppty $15K</description>
        <protected>false</protected>
        <recipients>
            <recipient>adi.chugh@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>david.webber@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>michael.richardson@reis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Lost_Oppty_15K</template>
    </alerts>
    <alerts>
        <fullName>Closed_Lost_Oppty_Mgr</fullName>
        <description>Closed Lost Oppty Mgr</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>adi.chugh@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>david.webber@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>randy.schwingen@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rich.hollister@reis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Closed_Lost_Oppty_Mgr</template>
    </alerts>
    <alerts>
        <fullName>NEF_Processing_Complete</fullName>
        <ccEmails>fanni.nova@reis.com</ccEmails>
        <ccEmails>david.webber@reis.com</ccEmails>
        <ccEmails>adi.chugh@reis.com</ccEmails>
        <description>NEF Processing Complete</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/NEF_Processing_Complete</template>
    </alerts>
    <alerts>
        <fullName>Notify_AM_team_of_New_Biz_Closed_Opp</fullName>
        <description>Notify AM team of New Biz Closed Opp</description>
        <protected>false</protected>
        <recipients>
            <recipient>mark.merenda@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>paul.grier@reis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Business_Closed_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Send_NEF_Email</fullName>
        <ccEmails>NEF@reis.com</ccEmails>
        <description>Send NEF Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>david.webber@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fanni.novak@reis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Shift_NEF_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Blank_Contract_Number_Field</fullName>
        <field>Contract_Number__c</field>
        <name>Blank Contract Number Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Multi_Year</fullName>
        <field>Multi_Year_Deal__c</field>
        <literalValue>1</literalValue>
        <name>Check Multi-Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flag_Greater_than_40</fullName>
        <field>Flag_Greater_than_40__c</field>
        <literalValue>1</literalValue>
        <name>Flag Greater than 40</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Manager_Forecast_Status_Renewals</fullName>
        <field>Forecast_Category_Mgr__c</field>
        <literalValue>Commit</literalValue>
        <name>Manager Forecast Status - Renewals</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_Lead_Generation_Id</fullName>
        <field>Lead_GeneratorId__c</field>
        <name>Remove Lead Generation Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Flag_Date</fullName>
        <field>Flag_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Flag Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Original_Deal_Value</fullName>
        <field>Original_Deal_Value__c</field>
        <formula>Amount</formula>
        <name>Set Original Deal Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Original_End_Date</fullName>
        <field>Original_Contract_End_Date__c</field>
        <formula>Contract_End_Date__c</formula>
        <name>Set Original End Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Original_Start_Date</fullName>
        <field>Original_Contract_Start_Date__c</field>
        <formula>Contract_Start_Date__c</formula>
        <name>Set Original Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_go_for_other_month</fullName>
        <field>To_go_for_other_months__c</field>
        <literalValue>1</literalValue>
        <name>Set to go for other month</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trial_Checkbox_TRUE</fullName>
        <field>Trial__c</field>
        <literalValue>1</literalValue>
        <name>Trial Checkbox TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Contract_Override</fullName>
        <field>Contract_Number_Override__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Contract Override</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Multi_Year</fullName>
        <field>Multi_Year_Deal__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Multi-Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contract_Number_to_SF</fullName>
        <field>Contract_Number__c</field>
        <formula>SF_Contract_Number__c</formula>
        <name>Update Contract Number to SF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Mobiuss_Probability_to_10</fullName>
        <description>Updates the Mobiuss Probability of an Opportunity to 10%</description>
        <field>Probability</field>
        <formula>0.10</formula>
        <name>Update Mobiuss Probability to 10%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Status_to_Lost</fullName>
        <field>Opportunity_Status__c</field>
        <literalValue>Lost</literalValue>
        <name>Update Opp Status to Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Renewal_Amount</fullName>
        <field>Renewal_Amount__c</field>
        <formula>Total_Renewal_Revenue__c</formula>
        <name>Update Renewal Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Renewal_Increase</fullName>
        <field>Renewal_Increase__c</field>
        <formula>Total_Increase_Revenue__c</formula>
        <name>Update Renewal Increase</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Renewal_New_Business</fullName>
        <field>Renewal_New_Business__c</field>
        <formula>Total_New_Business_Revenue__c</formula>
        <name>Update Renewal New Business</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Blank Contract Number</fullName>
        <actions>
            <name>Blank_Contract_Number_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.SF_Contract_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Contract_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Lost Oppty %2415K</fullName>
        <actions>
            <name>Closed_Lost_Oppty_15K</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 4) AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>15000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Record_Type__c</field>
            <operation>notContain</operation>
            <value>Compliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Previous_Renewal_Amount__c</field>
            <operation>greaterOrEqual</operation>
            <value>15000</value>
        </criteriaItems>
        <description>Closed Lost Oppty Over $15K</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Lost Oppty Mgr</fullName>
        <actions>
            <name>Closed_Lost_Oppty_Mgr</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 4) AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>5000</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Record_Type__c</field>
            <operation>notContain</operation>
            <value>Compliance</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Previous_Renewal_Amount__c</field>
            <operation>greaterOrEqual</operation>
            <value>5000</value>
        </criteriaItems>
        <description>Closed Lost Oppty Mgr</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Flag an Opportunity as Above 40</fullName>
        <actions>
            <name>Flag_Greater_than_40</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Flag_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Probability</field>
            <operation>greaterOrEqual</operation>
            <value>40</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manager Forecast Status - Renewals</fullName>
        <actions>
            <name>Manager_Forecast_Status_Renewals</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Record_Type__c</field>
            <operation>contains</operation>
            <value>Renewal</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Mobiuss Initial Demo Probability</fullName>
        <actions>
            <name>Update_Mobiuss_Probability_to_10</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mobiuss</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Initial Demo Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Multi-Year Deal - FALSE</fullName>
        <actions>
            <name>Uncheck_Multi_Year</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Calculates if an opportunity is a multi-year deal.  If not, uncheck Multi-Year Deal</description>
        <formula>AND( NOT (ISPICKVAL( StageName , &quot;Closed Won&quot;)), Contract_End_Date__c - Contract_Start_Date__c &lt; 547)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Multi-Year Deal - TRUE</fullName>
        <actions>
            <name>Check_Multi_Year</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Calculates if an opportunity is a multi-year deal</description>
        <formula>AND( NOT (ISPICKVAL( StageName , &quot;Closed Won&quot;)), Contract_End_Date__c - Contract_Start_Date__c &gt; 547)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NEF Process Complete</fullName>
        <actions>
            <name>NEF_Processing_Complete</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.NEF_Processed__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Account Management Team</fullName>
        <actions>
            <name>Notify_AM_team_of_New_Biz_Closed_Opp</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When a New Business Opportunity is set to Closed won, notify the Account Management team that an Account needs to be assigned to an AM</description>
        <formula>ISPICKVAL(StageName, &quot;Closed Won&quot;) &amp;&amp; $Profile.Id=&quot;00e11000000DhBR&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Remove Lead Generation Id</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Lead_GeneratorId__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When an Opp is created and Lead Gen has been added to Opportunity team, remove lead gen id</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Remove_Lead_Generation_Id</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send NEF Email</fullName>
        <actions>
            <name>Send_NEF_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Send_NEF_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Original Deal Value</fullName>
        <actions>
            <name>Set_Original_Deal_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Original_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Original_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Multi_Year_Deal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When an Opportunity is set to Closed Won, the Original deal amount is captured</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set SF Contract Number</fullName>
        <actions>
            <name>Update_Contract_Number_to_SF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Contract_Number_Override__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.SF_Contract_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Trial Checkbox to TRUE</fullName>
        <actions>
            <name>Trial_Checkbox_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Trial_Amount__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>To go for other months</fullName>
        <actions>
            <name>Set_to_go_for_other_month</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>if close month of opportunity is not equal to contract renewal month then check.  Used in reporting</description>
        <formula>MONTH(CloseDate) &lt;&gt; MONTH( Account.Contract_Renewal_Date__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Opp Status to Lost</fullName>
        <actions>
            <name>Update_Opp_Status_to_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>When an opportunity is set to Closed Lost, Opportunity Status is updated to lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Renewal Amount</fullName>
        <actions>
            <name>Update_Renewal_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Tracks changes to the Renewal Amount Field</description>
        <formula>ISCHANGED( Total_Renewal_Revenue__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Renewal Increase</fullName>
        <actions>
            <name>Update_Renewal_Increase</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Tracks changes to the Renewal Increase Field</description>
        <formula>ISCHANGED( Total_Increase_Revenue__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Renewal New Business</fullName>
        <actions>
            <name>Update_Renewal_New_Business</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Tracks changes to the Renewal New Business Field</description>
        <formula>ISCHANGED( Total_New_Business_Revenue__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
