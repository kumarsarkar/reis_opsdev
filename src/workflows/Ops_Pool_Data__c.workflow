<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PoolData_UpdateMSA</fullName>
        <description>Updates the MSA with the value from Affordable MSA</description>
        <field>MSA__c</field>
        <formula>Affordable_MSA__c</formula>
        <name>PoolData-UpdateMSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PoolData_UpdateSectorToAff</fullName>
        <description>Updates the sector to Aff</description>
        <field>Sector__c</field>
        <formula>&quot;Aff&quot;</formula>
        <name>PoolData-UpdateSectorToAff</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PoolData_UpdateSubId</fullName>
        <description>Updates the SubmarketID with Affordable Submarket ID</description>
        <field>Sub_Market_ID__c</field>
        <formula>Affordable_Submarket_Id__c</formula>
        <name>PoolData-UpdateSubId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PoolData - Affordable Review</fullName>
        <actions>
            <name>PoolData_UpdateMSA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PoolData_UpdateSectorToAff</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PoolData_UpdateSubId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Ops_Pool_Data__c.Sector__c</field>
            <operation>equals</operation>
            <value>Apt</value>
        </criteriaItems>
        <criteriaItems>
            <field>Ops_Pool_Data__c.Has_Affordable_Units__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Ops_Pool_Data__c.Property_Type__c</field>
            <operation>equals</operation>
            <value>Tax Credit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Ops_Pool_Data__c.Property_Type__c</field>
            <operation>equals</operation>
            <value>Mixed Income</value>
        </criteriaItems>
        <description>If the property meets the affordable criteria, update Sector, MSA, and SubID fields.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
