<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Quota_Credit_Amount</fullName>
        <field>Quota_Credit_Amount__c</field>
        <formula>Quota_Credit_Renewal__c +   Quota_Credit_Increase__c + Quota_Credit_NewBiz__c</formula>
        <name>Quota Credit Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Quota Credit Amount</fullName>
        <actions>
            <name>Quota_Credit_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunitySplit.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Quota Credit Amount = Quota Credit Renewal + Quota Credit NewBiz</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
