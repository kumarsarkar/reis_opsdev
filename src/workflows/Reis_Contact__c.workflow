<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SF_Created_Update</fullName>
        <field>Salesforce_Created__c</field>
        <literalValue>1</literalValue>
        <name>SF Created Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>REIS Contact Update Check</fullName>
        <actions>
            <name>SF_Created_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISCHANGED( First_Name__c ),ISCHANGED(Last_Name__c ),ISCHANGED(  Mobile_Phone__c ),ISCHANGED(  Email__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
