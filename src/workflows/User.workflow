<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>X60_Days_Add_User_to_Actvity_Funnel_Report</fullName>
        <description>60 Days: Add User to Actvity Funnel Report</description>
        <protected>false</protected>
        <recipients>
            <recipient>mark.merenda@reis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_User_Created_60_Days_Activity_Funnel</template>
    </alerts>
    <alerts>
        <fullName>X90_Days_Add_User_to_Pipeline_Report</fullName>
        <description>90 Days: Add User to Pipeline Report</description>
        <protected>false</protected>
        <recipients>
            <recipient>adi.chugh@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mark.merenda@reis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_User_Created_90_Days_Pipeline_Report</template>
    </alerts>
    <rules>
        <fullName>Sales User Add Activity Funnel %26 Pipeline Reports</fullName>
        <active>false</active>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
            <value>Account Manager,Chris Account Managers,Jared Account Managers,Compliance Manager,Compliance Rep,Custom Sales,New Business,SDA</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X60_Days_Add_User_to_Actvity_Funnel_Report</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>User.CreatedDate</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>X90_Days_Add_User_to_Pipeline_Report</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>User.CreatedDate</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
