<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Status_changed_to_pending_assignment</fullName>
        <field>Status__c</field>
        <literalValue>Pending Assignment</literalValue>
        <name>Status changed to pending assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Call_Status_Change_Time</fullName>
        <field>Call_Status_Updated_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Call Status Change Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Queue_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Pool_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Queue Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ReAssign Queue to Pool Queue</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Queue__c.Status__c</field>
            <operation>equals</operation>
            <value>In Progress,On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Queue__c.Call_Status_Updated_Time_Difference__c</field>
            <operation>greaterOrEqual</operation>
            <value>48</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Status_changed_to_pending_assignment</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Queue_Owner</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Queue__c.Call_Status_Updated_Time__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Call Status Change Time</fullName>
        <actions>
            <name>Update_Call_Status_Change_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(  Call_Status__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
