<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SFDC_Project_Update</fullName>
        <description>SFDC Project Update</description>
        <protected>false</protected>
        <recipients>
            <recipient>adi.chugh@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mark.merenda@reis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SFDC_Project_Update</template>
    </alerts>
    <alerts>
        <fullName>SFDC_User_Request_Completed</fullName>
        <description>SFDC User Request Completed</description>
        <protected>false</protected>
        <recipients>
            <recipient>adi.chugh@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>fanni.novak@reis.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mark.merenda@reis.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SFDC_Project_Update</template>
    </alerts>
    <rules>
        <fullName>SFDC Project Completed</fullName>
        <actions>
            <name>SFDC_Project_Update</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SFDC_Project__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>SFDC_Project__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Enhancement,Project</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFDC User Request Completed</fullName>
        <actions>
            <name>SFDC_User_Request_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SFDC_Project__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>SFDC_Project__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New User,Term User,Transfer User</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
