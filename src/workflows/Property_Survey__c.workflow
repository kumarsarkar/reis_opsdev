<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Flag_Property_Survey_For_QA_Review</fullName>
        <field>Needs_QA_Review__c</field>
        <literalValue>1</literalValue>
        <name>Flag Property Survey For QA Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Flag_Property_Survey_For_Review</fullName>
        <description>Sets the QA field on Property Survey to true, indicates that the approval process needs to go to QA for review</description>
        <field>QA__c</field>
        <literalValue>1</literalValue>
        <name>Flag Property Survey For Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PropSurvey_QA_Check_Accepts_Section_8</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &quot;Does not accept Section 8 Vouchers&quot;</formula>
        <name>PropSurvey - QA Check Accepts Section 8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PropSurvey_Set_QC_Note_Renovation</fullName>
        <field>QC_Notes_New__c</field>
        <formula>QC_Notes_New__c + BR() +&quot;Renovation Date:&quot;+ TEXT(Month( Renovation_Date_New__c ))+&quot;/&quot;+ TEXT(DAY(Renovation_Date_New__c)) +&quot;/&quot;+TEXT(Year(Renovation_Date_New__c))</formula>
        <name>PropSurvey: Set QC Note for Renovation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PropSurvey_Update_Purge_Flag</fullName>
        <field>Survey_Data_Can_Be_Purged__c</field>
        <literalValue>1</literalValue>
        <name>PropSurvey-Update Purge Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PropSurvey_Update_TC_Comp_Years_Remain</fullName>
        <field>Tax_Credit_Compliance_Years_Remaining__c</field>
        <formula>TEXT(
ABS(YEAR(TODAY()) - VALUE(Tax_Credit_Year_Placed_Into_Service__c))
)</formula>
        <name>PropSurvey - Update TC Comp Years Remain</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PropSurvey_Update_status</fullName>
        <description>Update Status</description>
        <field>Status__c</field>
        <literalValue>Pending Creation</literalValue>
        <name>PropSurvey-Update status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QA_Update_Add_Date</fullName>
        <description>Add a date to the QA Reason Notes field before adding the remainder of the notes. This will insure that all dates are formatted correctly</description>
        <field>QA_Reason_Notes__c</field>
        <formula>(QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;)</formula>
        <name>QA Update - Add Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QA_Update_Property_Type_Change</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>(QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+&quot; Total Property Size changed to &quot; +   TEXT(Total_Property_Size_New__c)  )</formula>
        <name>QA Update Property Type Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QA_Update_Proposed_Type</fullName>
        <description>Updates the QA field to reflect the proposed type change</description>
        <field>QA_Reason_Notes__c</field>
        <formula>(QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+&quot; Call Status Proposed Type Change&quot;)</formula>
        <name>QA Update Proposed Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QA_Update_Retail_Anchor_Size</fullName>
        <description>Update QA Notes to reflect the change in anchor size</description>
        <field>QA_Reason_Notes__c</field>
        <formula>(QA_Reason_Notes__c +  &quot;Anchor size changed to &quot; + TEXT( Anchor_Size_New__c ))</formula>
        <name>QA Update - Retail - Anchor Size</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QA_Update_Retail_Food_Court_Size</fullName>
        <description>Write note to reflect changes in space type</description>
        <field>QA_Reason_Notes__c</field>
        <formula>(QA_Reason_Notes__c + &apos;Food Court size changed to &apos; + TEXT( Food_Court_Size_New__c ))</formula>
        <name>QA Update - Retail - Food Court Size</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QA_Update_Retail_Ground_Lease_Size</fullName>
        <description>Update QA Notes to reflect the change in ground lease size</description>
        <field>QA_Reason_Notes__c</field>
        <formula>(QA_Reason_Notes__c + &quot;Ground Lease size changed to &quot; + TEXT(Ground_Lease_Size_New__c))</formula>
        <name>QA Update - Retail - Ground Lease Size</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QA_Update_Retail_NonAnchor_Size</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>(QA_Reason_Notes__c + &quot;Non-Anchor size changed to &quot; + TEXT(  NonAnchor_Size_New__c ))</formula>
        <name>QA Update - Retail - NonAnchor Size</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QA_Update_Retail_Outbuilding_Size</fullName>
        <description>update QA Reason Notes to reflect changes in the size of outbuildings</description>
        <field>QA_Reason_Notes__c</field>
        <formula>(QA_Reason_Notes__c + &quot;Outbuilding size changed to &quot; + TEXT(  Outparcel_Building_Size_New__c  ))</formula>
        <name>QA Update - Retail - Outbuilding Size</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Select_IAG_Type_Change</fullName>
        <description>Set the select code to IAG type change</description>
        <field>Select_Code_New__c</field>
        <literalValue>IAT</literalValue>
        <name>Select IAG Type Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Select_Type_Change</fullName>
        <description>Selects Type change from the select code picklist</description>
        <field>Select_Code_New__c</field>
        <literalValue>T</literalValue>
        <name>Select Type Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QC_Notes</fullName>
        <field>QC_Notes_New__c</field>
        <formula>QC_Notes_New__c + &apos; Does not accept Section 8 vouchers &apos;</formula>
        <name>Set QC Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_QC_Notes_S8</fullName>
        <field>QC_Notes_New__c</field>
        <formula>QC_Notes_New__c + BR()+&apos;(&apos; + TEXT(MONTH(Today()))+&quot;/&quot; +TEXT(DAY(Today()))+&quot;/&quot; +TEXT(YEAR(Today())) +&apos;)&apos; + &apos;Does not accept section 8 vouchers&apos;</formula>
        <name>Set QC Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SurveyDataCanBePurged_Changes_To_False</fullName>
        <field>Survey_Data_Can_Be_Purged__c</field>
        <literalValue>0</literalValue>
        <name>SurveyDataCanBePurged Changes To False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TT_Update_Ready_to_be_Purged_Flag</fullName>
        <field>Survey_Data_Can_Be_Purged__c</field>
        <literalValue>1</literalValue>
        <name>TT Update Ready to be Purged Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TT_Update_Status_to_Pending_Creation</fullName>
        <field>Status__c</field>
        <literalValue>Pending Creation</literalValue>
        <name>TT Update Status to Pending Creation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TT_Update_to_Pool_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Pool_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>TimeTrigger: Update to Pool Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_On_Hold_Time</fullName>
        <field>Call_Status_On_Hold_Date__c</field>
        <formula>NOW()</formula>
        <name>Update On Hold Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Note_Unit_Mix</fullName>
        <description>Updates QA Reason notes with the details of the unit mix change</description>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( Unit_Mix_Estimated__c == True,  QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+&quot;Unit Mix Estimated = True&quot;, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+&quot;Unit Mix Estimated = False&quot;)</formula>
        <name>Update QA Notes for Unit Mix</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Address_Change</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Address&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot;Address Changed&quot;)</formula>
        <name>Update QA Notes for Address Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Buildings</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;# of Building Change&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot;# of Building Change&quot;)</formula>
        <name>Update QA Notes for Buildings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Floors_Change</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Floors Change&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot;Floors Changed&quot;)</formula>
        <name>Update QA Notes for Floors Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Of_Buildings</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;# Of Buildings&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &quot; # Of Buildings Changed&quot;)</formula>
        <name>Update QA Notes for # Of Buildings</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Of_Floors</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;# Of Floors&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &quot; # Of Floors Changed&quot;)</formula>
        <name>Update QA Notes for # Of Floors</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Property_Name</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Property Name&quot;), QA_Reason_Notes__c,  QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot;Property Name Changed&quot;)</formula>
        <name>Update QA Notes for Property Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Property_Size</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Property Size&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &quot;Property Size changed by more than 20,000 sqft&quot;)</formula>
        <name>Update QA Notes for Property Size</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Property_Status</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Property Status&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot; Property Status Changed&quot;)</formula>
        <name>Update QA Notes for Property Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Property_Type</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Property Type&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot;Property Type&quot;)</formula>
        <name>Update QA Notes for Property Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Property_Type_New</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Property Type&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &quot; Property Type Changed&quot;)</formula>
        <name>Update QA Notes for Property Type New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Property_Website</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Property Website&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot; Property Website Changed&quot;)</formula>
        <name>Update QA Notes for Property Website</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Select_Code</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Select Code&quot;), QA_Reason_Notes__c,  QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot;Select Code changed to Non Competitive&quot;)</formula>
        <name>Update QA Notes for Select Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Total_Units</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Total Units&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot;Total Units Changed by more than 10&quot;)</formula>
        <name>Update QA Notes for Total Units</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QA_Notes_for_Year_Built</fullName>
        <field>QA_Reason_Notes__c</field>
        <formula>IF( CONTAINS(QA_Reason_Notes__c, &quot;Year Built&quot;), QA_Reason_Notes__c, QA_Reason_Notes__c + SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + &apos;(&apos; + TEXT(MONTH(TODAY()))+&apos;-&apos;+TEXT(DAY(TODAY())) + &apos;-&apos; + TEXT(YEAR(TODAY()))+&apos;)&apos;+ &quot;Year Built changed more than 10&quot;)</formula>
        <name>Update QA Notes for Year Built</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QC_Notes_with_DK_Rest</fullName>
        <field>QC_Notes_New__c</field>
        <formula>IF( CONTAINS(QC_Notes_New__c, &quot;DK Rest&quot;), QC_Notes_New__c, QC_Notes_New__c+ SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + TEXT(TODAY()) +&quot; - DK Rest&quot;)</formula>
        <name>Update QC Notes with DK Rest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QC_Notes_with_Refused_Rest</fullName>
        <field>QC_Notes_New__c</field>
        <formula>IF( CONTAINS(QC_Notes_New__c, &quot;Refused Rest&quot;), QC_Notes_New__c, QC_Notes_New__c +  SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + Text(TODAY()) +&quot; - Refused Rest&quot;)</formula>
        <name>Update QC Notes with Refused Rest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_QC_Notes_with_Rest_Neg</fullName>
        <field>QC_Notes_New__c</field>
        <formula>IF( CONTAINS(QC_Notes_New__c, &quot;Rest Neg&quot;), QC_Notes_New__c, QC_Notes_New__c+ SUBSTITUTE($Label.NewLine,&quot;-&quot;,&quot;&quot;) + Text(TODAY()) + &quot; - Rest Neg&quot;)</formula>
        <name>Update QC Notes with Rest Neg</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Returned_to_User_Flag</fullName>
        <field>Returned_to_User__c</field>
        <literalValue>1</literalValue>
        <name>Update Returned to User Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_In_Progress</fullName>
        <field>Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Update Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_In_QA_Review</fullName>
        <field>Status__c</field>
        <literalValue>In QA Review</literalValue>
        <name>Update Status to In QA Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_In_Team_Lead_Review</fullName>
        <field>Status__c</field>
        <literalValue>In Team Lead Review</literalValue>
        <name>Update Status to In Team Lead Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Ready_for_Publish</fullName>
        <field>Status__c</field>
        <literalValue>Ready for Publish</literalValue>
        <name>Update Status to Ready for Publish</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Returned_to_User</fullName>
        <field>Status__c</field>
        <literalValue>Returned to User</literalValue>
        <name>Update Status to Returned to User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Pool_Queue</fullName>
        <description>Update the Owner to Pool Queue and Nullify Queue Record</description>
        <field>OwnerId</field>
        <lookupValue>Pool_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update to Pool Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Prop Survey %3A Renovation Date Change</fullName>
        <actions>
            <name>PropSurvey_Set_QC_Note_Renovation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the QC Note</description>
        <formula>AND(ISCHANGED(Renovation_Date_New__c) , NOT(ISNULL(Renovation_Date_New__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - ApprovalProces QA Status Check</fullName>
        <actions>
            <name>Update_Status_to_Ready_for_Publish</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Property_Survey__c.Needs_QA_Review__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Property_Survey__c.Status__c</field>
            <operation>equals</operation>
            <value>In QA Review</value>
        </criteriaItems>
        <description>Checks the survey status to see if it went to QA step</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check  Buildings</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Of_Buildings</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Reviews the Buildings field for a change</description>
        <formula>IF(ISCHANGED( Buildings_New__c ) &amp;&amp;  Buildings_Current__c !=  Buildings_New__c , TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Buildings</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Buildings</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Buildings field for a change to QA</description>
        <formula>( NOT(ISBLANK(Buildings_New__c)) &amp;&amp; NOT(ISBLANK(Buildings_Current__c)) &amp;&amp; (ABS(Buildings_Current__c - Buildings_New__c) &gt;3) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Floors</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Floors_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Floors field for a change to QA</description>
        <formula>( ( NOT( RecordType.DeveloperName = &quot;Industrial&quot;) &amp;&amp; NOT( RecordType.DeveloperName = &quot;Retail&quot;) )&amp;&amp; NOT(ISBLANK(Floors_New__c)) &amp;&amp; NOT(ISBLANK(Floors_Current__c)) &amp;&amp; (ABS(Floors_Current__c - Floors_New__c) &gt;3) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Floors %28Retail and Industrial%29</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Floors_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Floors field for a change to QA for the retail / industrial sectors</description>
        <formula>( (  RecordType.DeveloperName = &quot;Industrial&quot;|| RecordType.DeveloperName = &quot;Retail&quot; )&amp;&amp; NOT(ISBLANK(Floors_New__c)) &amp;&amp; NOT(ISBLANK(Floors_Current__c)) &amp;&amp; (ABS(Floors_Current__c - Floors_New__c) &gt;1) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Property Name</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Property_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Property Name field for a change to QA</description>
        <formula>(   
ISCHANGED(Property_Name__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Retail - Anchor Size</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Add_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Retail_Anchor_Size</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check to see if the individual spaces in the retail survey have changed</description>
        <formula>(Anchor_Size_New__c  != Anchor_Size_Current__c &amp;&amp; !ISNULL(Anchor_Size_New__c)) &amp;&amp;  ISCHANGED(Anchor_Size_New__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Retail - Food Court Size</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Add_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Retail_Food_Court_Size</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check to see if the individual spaces in the retail survey have changed</description>
        <formula>Food_Court_Size_New__c != Food_Court_Size_Current__c &amp;&amp;  !ISNULL(Food_Court_Size_New__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Retail - Ground Lease</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Add_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Retail_Ground_Lease_Size</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check to see if the individual spaces in the retail survey have changed</description>
        <formula>Ground_Lease_Size_New__c  !=  Ground_Lease_Size_Current__c  &amp;&amp;  !ISNULL(Ground_Lease_Size_New__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Retail - Nonanchor Size</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Add_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Retail_NonAnchor_Size</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check to see if the individual spaces in the retail survey have changed</description>
        <formula>(NonAnchor_Size_New__c != NonAnchor_Size_Current__c &amp;&amp; !ISNULL(NonAnchor_Size_New__c)) &amp;&amp;  ISCHANGED(NonAnchor_Size_New__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Retail - Outbuilding Size</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Add_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Retail_Outbuilding_Size</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check to see if the individual spaces in the retail survey have changed</description>
        <formula>Outparcel_Building_Size_Current__c !=  Outparcel_Building_Size_New__c &amp;&amp; !ISNULL(Outparcel_Building_Size_New__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check - Select Code</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Select_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks to see if the select code is non-comp</description>
        <formula>ISPICKVAL(Select_Code_New__c, &apos;X&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Address</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Address_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Address field for a change to QA</description>
        <formula>( ISCHANGED(Address_New__c) &amp;&amp;
NOT(ISBLANK(Address_New__c)) &amp;&amp; 
NOT(ISBLANK(Address__c)) &amp;&amp; (Address__c &lt;&gt; Address_New__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Property Floors</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Of_Floors</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Reviews the Floors field for a change</description>
        <formula>If(ISCHANGED(Floors_New__c) &amp;&amp; Floors_New__c != Floors_Current__c, TRUE,FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Property Status</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Property_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Property Status field for a change to QA</description>
        <formula>( ISCHANGED(Property_Status_New__c) &amp;&amp;
NOT(ISBLANK(Property_Status_New__c)) &amp;&amp; 
NOT(ISBLANK(Property_Status_Current__c)) &amp;&amp; (Property_Status_Current__c &lt;&gt; Property_Status_New__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Property Type</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Property_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Property Type field for a change to QA</description>
        <formula>( ISCHANGED(Property_Type_New__c) &amp;&amp;
NOT(ISBLANK(Text(Property_Type_New__c))) &amp;&amp; 
NOT(ISBLANK(Property_Type__c)) &amp;&amp; (Property_Type__c &lt;&gt; Text(Property_Type_New__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Residential Type</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Property_Type_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Reviews the Residential Type field for a change</description>
        <formula>IF(TEXT(PRIORVALUE(Residential_Type_New__c )) !=  TEXT(Residential_Type_New__c) &amp;&amp; TEXT(Residential_Type_New__c) !=  TEXT(Residential_Type_Current__c), TRUE, FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Sec8 Year End</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_QC_Notes_S8</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>( ISCHANGED(Accepts_Section_8_Vouchers__c) &amp;&amp; 
!Accepts_Section_8_Vouchers__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Total Units</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Total_Units</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Total Units field for a change to QA</description>
        <formula>( ISCHANGED(Total_Units__c) &amp;&amp; (ABS(Total_Units__c - PRIORVALUE(Total_Units__c)) &gt; 10) &amp;&amp; ( RecordType.DeveloperName &lt;&gt; &quot;Industrial&quot; || RecordType.DeveloperName &lt;&gt; &quot;Office&quot; || RecordType.DeveloperName &lt;&gt; &quot;Retail&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Unit Mix</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Note_Unit_Mix</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Unit Mix Estimated field for a change</description>
        <formula>ISCHANGED(Unit_Mix_Estimated__c) &amp;&amp; !ISPICKVAL(Status__c, &apos;Pending Assignment&apos;) &amp;&amp; !ISPICKVAL(Status__c, &apos;Pending Creation&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Website</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Property_Website</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Reviews the Property Website field for a change to QA</description>
        <formula>( ISCHANGED(Property_Website_New__c) &amp;&amp;
NOT(ISBLANK(Property_Website_New__c)) &amp;&amp; 
NOT(ISBLANK(Property_Website_Current__c)) &amp;&amp; (Property_Website_New__c &lt;&gt; Address_New__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - QA Check Year Built</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_QA_Notes_for_Year_Built</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Reviews the Year Built field for a change to QA</description>
        <formula>( ISCHANGED(Year_Built_New__c) &amp;&amp; NOT(ISBLANK(Year_Built_New__c)) &amp;&amp; (ABS(VALUE(Year_Built_New__c) - VALUE(PRIORVALUE(Year_Built_Current__c))) &gt; 10) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - Set On Hold Time</fullName>
        <actions>
            <name>Update_On_Hold_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the Time when the Survey was first Assigned.</description>
        <formula>AND(ISCHANGED( Status__c ),OR(ISPICKVAL(Status__c, &apos;In Progress&apos;),ISPICKVAL(Status__c, &apos;On Hold&apos;)), ISNULL( Call_Status_On_Hold_Date__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey - TC Compliance Years Remaining</fullName>
        <actions>
            <name>PropSurvey_Update_TC_Comp_Years_Remain</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Property_Survey__c.Tax_Credit_Year_Placed_Into_Service__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Reviews the TC Compliance Years Remaining field for a change</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey-Check if On Hold</fullName>
        <active>true</active>
        <description>This checks if the Prop Survey is on hold even after 2 days. If Yes, then, assigns it back to pool queue.</description>
        <formula>AND(OR(ISPICKVAL( Status__c , &apos;On Hold&apos;),ISPICKVAL( Status__c , &apos;In Progress&apos;)), NOT(Returned_to_User__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>TT_Update_Ready_to_be_Purged_Flag</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Property_Survey__c.Call_Status_On_Hold_Date__c</offsetFromField>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>PropSurvey-Check if Proposed Type Change and IAG</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Proposed_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Select_IAG_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks if the call status has been changed to proposed type change.</description>
        <formula>ISCHANGED(Call_Status__c)&amp;&amp;(ISPICKVAL( Call_Status__c , &apos;Proposed Type Change&apos;))&amp;&amp;((ISPICKVAL(Select_Code_New__c , &apos;IAG&apos;)) ||
(ISPICKVAL(Select_Code_New__c , &apos;IAZ&apos;)) ||
(ISPICKVAL(Select_Code_New__c , &apos;IAI&apos;)) ||
(ISPICKVAL(Select_Code_New__c , &apos;IAA&apos;)) ||
(ISPICKVAL(Select_Code_New__c , &apos;IAR&apos;)) ||
(ISPICKVAL(Select_Code_New__c , &apos;IAK&apos;)) ||
(ISPICKVAL(Select_Code_New__c , &apos;IAT&apos;)) ||
(ISPICKVAL(Select_Code_New__c , &apos;IAU&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey-Check if Proposed Type Change and Not IAG</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Flag_Property_Survey_For_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Proposed_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Select_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This checks if the call status has been changed to proposed type change.</description>
        <formula>ISCHANGED(Call_Status__c)&amp;&amp;(ISPICKVAL(Call_Status__c , &apos;Proposed Type Change&apos;))&amp;&amp; !((ISPICKVAL(Select_Code_New__c , &apos;IAG&apos;)) || 
(ISPICKVAL(Select_Code_New__c , &apos;IAZ&apos;)) || 
(ISPICKVAL(Select_Code_New__c , &apos;IAI&apos;)) || 
(ISPICKVAL(Select_Code_New__c , &apos;IAA&apos;)) || 
(ISPICKVAL(Select_Code_New__c , &apos;IAR&apos;)) || 
(ISPICKVAL(Select_Code_New__c , &apos;IAK&apos;)) || 
(ISPICKVAL(Select_Code_New__c , &apos;IAT&apos;)) || 
(ISPICKVAL(Select_Code_New__c , &apos;IAU&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>PropSurvey-Check if Status - change in team lead review</fullName>
        <actions>
            <name>SurveyDataCanBePurged_Changes_To_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (ISCHANGED(Status__c),  ISPICKVAL(Status__c, &apos;In Team Lead Review&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Property Survey - QA Check - Check for Size Change</fullName>
        <actions>
            <name>Flag_Property_Survey_For_QA_Review</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QA_Update_Property_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Check if total property size has been changed &gt;=20000 and Trigger a QA review</description>
        <formula>Total_Property_Size_New__c !=  Total_Property_Size_Current__c &amp;&amp; !ISNULL(Total_Property_Size_New__c) &amp;&amp; (ABS(Total_Property_Size_New__c-Total_Property_Size_Current__c)&gt;=20000 )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Sec 8 Voucher note</fullName>
        <actions>
            <name>Set_QC_Notes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Property_Survey__c.Accepts_Section_8_Vouchers__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Property_Survey__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Affordable</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Survey - DK Rest Check</fullName>
        <actions>
            <name>Update_QC_Notes_with_DK_Rest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Property_Survey__c.DK_rest__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Survey - Refused Rest Check</fullName>
        <actions>
            <name>Update_QC_Notes_with_Refused_Rest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Property_Survey__c.Ref_Rest__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Survey - Rest Neg Check</fullName>
        <actions>
            <name>Update_QC_Notes_with_Rest_Neg</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Property_Survey__c.Rest_neg__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TestWF</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Property_Survey__c.Students__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
