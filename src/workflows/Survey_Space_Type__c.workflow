<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AptAverageRentCalc</fullName>
        <field>Average_Rent_New__c</field>
        <formula>(High_Rent_New__c + Low_Rent_New__c) / 2</formula>
        <name>AptAverageRentCalc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Apt - Calc Avg Rent</fullName>
        <actions>
            <name>AptAverageRentCalc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Survey_Space_Type__c.Low_Rent_New__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Survey_Space_Type__c.High_Rent_New__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>QA Check - New Survey Space Type</fullName>
        <active>false</active>
        <description>Write a QA review note on the associated property survey when a survey space type is added</description>
        <formula>True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
